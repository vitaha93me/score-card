package com.introlabgroup.scorecard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;

@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
public class ScoreCardApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScoreCardApplication.class, args);
    }
}
