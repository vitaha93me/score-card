package com.introlabgroup.scorecard.configuration;

import com.introlabgroup.scorecard.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {


    private final UserRepository userRepository;

    @Autowired
    public SecurityConfiguration(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Bean(name = "customDetailService")
    public UserDetailsService userDetailsServiceBean() {
        return new CustomUserDetailsService(userRepository);
    }

    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .antMatcher("/api/**")
                    .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                    .addFilterBefore(new JWTAuthenticationFilter(),
                            UsernamePasswordAuthenticationFilter.class)
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        }

    }


    @Configuration
    public static class App1ConfigurationAdapter extends WebSecurityConfigurerAdapter {

        private final BCryptPasswordEncoder bCryptPasswordEncoder;

        private final SimpleAuthenticationSuccessHandler successHandler;

        @Autowired
        public App1ConfigurationAdapter(SimpleAuthenticationSuccessHandler successHandler, BCryptPasswordEncoder bCryptPasswordEncoder, @Qualifier("customDetailService") UserDetailsService userDetailsService) {
            this.successHandler = successHandler;
            this.bCryptPasswordEncoder = bCryptPasswordEncoder;
            this.userDetailsService = userDetailsService;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            //@formatter:off
            http.
                    authorizeRequests()
                    .antMatchers("/web/mylogin").permitAll()
                    .antMatchers("/web/test/qualities/match").permitAll()
                    .antMatchers("/web/jwt/login").permitAll()
                    .antMatchers("/web/payment/handle").permitAll()
                    .antMatchers("/web/company/registration").permitAll()
                    .antMatchers("/manager/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN", "ROLE_SUPER_ADMIN")
                    .antMatchers("/admin/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_SUPER_ADMIN", "ROLE_USER")
                    .and()
                    .formLogin().successHandler(successHandler)
                    .loginPage("/login")
                    .loginProcessingUrl("/web/mylogin")
                    .failureUrl("/login?error=true")
                    .usernameParameter("email")
                    .passwordParameter("password")
                    .and()
                    .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/")
                    .and().csrf().disable().exceptionHandling().accessDeniedPage("/denied")
                    .authenticationEntryPoint(authenticationEntryPoint());
        }

        //@formatter:on

        @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }

        private final UserDetailsService userDetailsService;

        @Override
        protected void configure(AuthenticationManagerBuilder auth)
                throws Exception {
            auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
        }

        @Bean
        public AuthenticationEntryPoint authenticationEntryPoint() {
            return new LoginUrlAuthenticationEntryPoint("/web/mylogin");
        }

        @Override
        public void configure(WebSecurity web) {
            web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/fonts/**", "/font-awesome/**");
            web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
        }
    }
}