package com.introlabgroup.scorecard.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.introlabgroup.scorecard.configuration.TokenAuthenticationService;
import com.introlabgroup.scorecard.error.IncorrectLoginOrPasswordException;
import com.introlabgroup.scorecard.model.*;
import com.introlabgroup.scorecard.model.dto.DataJsonObject;
import com.introlabgroup.scorecard.model.dto.UserDTO;
import com.introlabgroup.scorecard.model.onet.MeasuringType;
import com.introlabgroup.scorecard.model.onet.MeasuringTypeEnumConverter;
import com.introlabgroup.scorecard.model.onet.PriorityRanking;
import com.introlabgroup.scorecard.model.onet.PriorityRankingEnumConverter;
import com.introlabgroup.scorecard.model.pi.TimeOnTheWay;
import com.introlabgroup.scorecard.repository.onet.OnetItemRepository;
import com.introlabgroup.scorecard.service.*;
import com.introlabgroup.scorecard.utils.CandidateUtils;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

import static com.introlabgroup.scorecard.utils.UserUtils.hasAccessToUser;
import static com.introlabgroup.scorecard.utils.UserUtils.userToDTO;

/**
 * @author vitalii.
 */
@RestController
public class ApiController {

    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    private final GoogleMapsService googleMapsService;

    private final DepartmentService departmentService;

    private final UserService userService;

    private final CompanyService companyService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final MailSendingService mailSendingService;

    private final StripeService stripeService;

    @Autowired
    public ApiController(GoogleMapsService googleMapsService, DepartmentService departmentService, UserService userService, CompanyService companyService, BCryptPasswordEncoder bCryptPasswordEncoder, MailSendingService mailSendingService, StripeService stripeService, OnetItemRepository onetItemRepository) {
        this.googleMapsService = googleMapsService;
        this.departmentService = departmentService;
        this.userService = userService;
        this.companyService = companyService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.mailSendingService = mailSendingService;
        this.stripeService = stripeService;
        this.onetItemRepository = onetItemRepository;
    }

    @RequestMapping(value = "/api/admin", method = RequestMethod.GET)
    @ResponseBody
    public String represent() {
        return "";
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(PriorityRanking.class, new PriorityRankingEnumConverter());
        dataBinder.registerCustomEditor(MeasuringType.class, new MeasuringTypeEnumConverter());
    }

    @RequestMapping(value = "/web/jwt/login", method = RequestMethod.POST)
    public TreeMap<String, Object> login(HttpServletRequest request, HttpServletResponse httpServletResponse) throws IOException {

        User creds = new ObjectMapper().readValue(request.getInputStream(), User.class);
        User user;
        try {
            user = userService.findUserByEmailAndPassword(creds.getEmail(), creds.getPassword());
        } catch (NullPointerException e) {
            throw new IncorrectLoginOrPasswordException(e);
        }

        if (user == null) {
            throw new IncorrectLoginOrPasswordException();
        }

        TreeMap<String, Object> treeMap = new TreeMap<>();

        Company company = companyService.findById(user.getCompany().getId());

        checkTrial(company);
        if (company.isPaid() && !company.isTrial()) {
            checkPaymentPeriod(company);
        }

        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        UserDTO userDTO = userToDTO(user);

        if (!user.getCompany().isTrial() && user.getCompany().isPaid()) {
            try {
                userDTO.setSubscription(stripeService.getStripeDao().getStripeSubscriptionByCompanyId(user.getCompany().getId()));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        treeMap.put("token", TokenAuthenticationService.addAuthentication(httpServletResponse, userDTO));
        return treeMap;

    }

    private void checkPaymentPeriod(Company company) {
        try {
            if (!company.isFree()) {
                boolean periodFinished = companyService.isPeriodFinished(company.getId());
                if (periodFinished) {
                    company.setAllowUsage(false);
                    company.setReason("Subscription canceled or payment failed");
                    companyService.saveCompany(company);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void checkTrial(Company company) {
        if (company.isTrial()) {
            boolean trial = companyService.isTrial(company.getId());
            if (!trial) {
                company.setTrial(false);
                company.setAllowScorecards(false);
                company.setAllowUsage(false);
                company.setReason("Trial period ended");
                companyService.saveCompany(company);
            }
        }

    }

    @RequestMapping(value = "/api/userinfo", method = RequestMethod.GET)
    public UserDTO userInfo(@RequestParam Long userId) {
        User user = userService.findUserById(userId);
        UserDTO userDTO = userToDTO(user);
        Company company = user.getCompany();

        checkTrial(company);

        if (company.isPaid() && !company.isTrial()) {
            checkPaymentPeriod(company);
        }
        if (!company.isTrial() && company.isPaid()) {
            try {
                userDTO.setSubscription(stripeService.getStripeDao().getStripeSubscriptionByCompanyId(company.getId()));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return userDTO;
    }

    @RequestMapping(value = "/api/companyinfo", method = RequestMethod.GET)
    public UserDTO companyInfo(@RequestParam Long companyId) {

        UserDTO userDTO = new UserDTO();
        Company company = companyService.findById(companyId);

        checkTrial(company);

        if (company.isPaid() && !company.isTrial()) {
            checkPaymentPeriod(company);
        }

        company.setDepartments(null);
        company.setUsers(null);

        if (!company.isTrial() && company.isPaid()) {
            try {
                userDTO.setSubscription(stripeService.getStripeDao().getStripeSubscriptionByCompanyId(company.getId()));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        userDTO.setCompany(company);
        return userDTO;
    }

    @RequestMapping(value = "/web/jwt/adminlogin", method = RequestMethod.POST)
    public TreeMap<String, Object> loginAsUser(HttpServletRequest request, HttpServletResponse httpServletResponse) throws IOException {

        User creds = new ObjectMapper()
                .readValue(request.getInputStream(), User.class);
        User user = userService.findUserByEmail(creds.getEmail());
        TreeMap<String, Object> treeMap = new TreeMap<>();

        if (user != null) {
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
            treeMap.put("token", TokenAuthenticationService.addAuthentication(httpServletResponse, userToDTO(user)));
            return treeMap;
        }
        httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        treeMap.put("Token", "Failed");
        return treeMap;
    }


    @RequestMapping(value = "web/company/registration", method = RequestMethod.POST, consumes = {MediaType.ALL_VALUE})
    @ResponseBody
    public String createAdminUser(@RequestBody CompanyRegistrationDTO user, BindingResult bindingResult, HttpServletResponse response) throws IOException {
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult.rejectValue("email", "error.user", "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "There is already a user registered with the email provided");
            return "Failed";
        }
        if (companyService.findByName(user.getCompanyName()) != null) {
            bindingResult
                    .rejectValue("Name", "error.company",
                            "There is already a company registered with the name provided");
        }
        if (bindingResult.hasErrors()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "There is already a company registered with the name provided");
            return "Failed";
        }
        Company company = new Company();
        company.setName(user.getCompanyName());
        company.setAllowUsage(true);
        company.setTrial(true);
        company.setAllowScorecards(true);
        company.setEndOfPeriod(DateUtils.addMonths(new Date(), 1));

        companyService.saveCompany(company);
        response.setStatus(HttpServletResponse.SC_OK);
        company = companyService.saveCompany(company);

        final User newUser = new User();
        newUser.setCompany(company);
        newUser.setPassword(user.getPassword());
        newUser.setName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setEmail(user.getEmail());

        mailSendingService.sendAdminNotification(newUser, "Scorecard Team");
        userService.createUser(newUser, "ROLE_ADMIN");

        response.setStatus(HttpServletResponse.SC_OK);
        return "OK";
    }

    @RequestMapping("/api/manager/registration")
    @ResponseBody
    public String createNewUser(@Valid User user, BindingResult bindingResult, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult.rejectValue("email", "error.user", "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {
            user.setCompany(loggedUser.getCompany());
            mailSendingService.sendManagerNotification(user, loggedUser.getFullName());
            userService.createUser(user, "ROLE_USER");
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }


    @RequestMapping("/api/manager/update")
    @ResponseBody
    public String updateUser(@Valid User user, @RequestParam String depIds, BindingResult bindingResult, HttpServletResponse response) {
        User userExists = userService.findUserById(user.getId());

        List<User> usersByEmail = userService.findUsersByEmail(user.getEmail());
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else if (!usersByEmail.isEmpty() && usersByEmail.get(0).getId().longValue() != userExists.getId().longValue()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "This email used by another user";
        } else {

            if (!user.getPassword().equals(userExists.getPassword())) {
                userExists.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            }


            //TODO: discuss with Dmitrii
            Collection<CompanyDepartment> departments = new ArrayList<>();
            if (!depIds.isEmpty()) {
                for (String s : depIds.split(",")) {
                    departments.add(departmentService.findById((long) Integer.parseInt(s), false));
                }
            }
            userExists.setDepartments(new HashSet<>(departments));

            userExists.setAdmin(user.isAdmin());
            userExists.setName(user.getName());
            userExists.setLastName(user.getLastName());
            userExists.setEmail(user.getEmail());
            userExists.setJobTitle(user.getJobTitle());
            userService.saveUser(userExists);
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/api/admin/create/{companyId}/{departmentId}", method = RequestMethod.POST)
    @ResponseBody
    public String createManagerUser(@Valid User user, @PathVariable long companyId, @PathVariable long departmentId, BindingResult bindingResult, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult.rejectValue("email", "error.user", "This email used by another user");
        }
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "This email used by another user";
        } else {
            user.setCompany(companyService.findById(companyId));

            if (user.isAdmin()) {
                mailSendingService.sendAdminNotification(user, loggedUser.getFullName());
                userService.createUser(user, "ROLE_ADMIN");
            } else {
                if (user.getDepartments() != null) {
                    user.getDepartments().add(departmentService.findById(departmentId, true));
                } else {
                    user.setDepartments(Collections.singleton(departmentService.findById(departmentId, true)));
                }
                mailSendingService.sendManagerNotification(user, loggedUser.getFullName());
                userService.createUser(user, "ROLE_USER");
            }
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/api/manager/create/{companyId}", method = RequestMethod.POST)
    @ResponseBody
    public String createManagerUser(@Valid User user, @PathVariable long companyId, BindingResult bindingResult, HttpServletResponse response) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult.rejectValue("email", "error.user", "This email used by another user");
        }
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "This email used by another user";
        } else {
            user.setCompany(companyService.findById(companyId));

            if (user.isAdmin()) {
                mailSendingService.sendAdminNotification(user, loggedUser.getFullName());
                userService.createUser(user, "ROLE_ADMIN");
            } else {
                if (user.getDepartments() == null) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No departments defined.");
                    return "Failed";
                }
                mailSendingService.sendManagerNotification(user, loggedUser.getFullName());
                userService.createUser(user, "ROLE_USER");
            }
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/api/admin/create/{companyId}", method = RequestMethod.POST)
    @ResponseBody
    public String createAdminUser(@Valid User user, @PathVariable long companyId, BindingResult bindingResult, HttpServletResponse response) {
        User userExists = userService.findUserByEmail(user.getEmail());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        if (userExists != null) {
            bindingResult.rejectValue("email", "error.user", "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {
            user.setCompany(companyService.findById(companyId));

            if (user.isAdmin()) {
                mailSendingService.sendAdminNotification(user, loggedUser.getFullName());
                userService.createUser(user, "ROLE_ADMIN");

            } else {
                mailSendingService.sendManagerNotification(user, loggedUser.getFullName());
                userService.createUser(user, "ROLE_USER");
            }
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/api/admin/create/master", method = RequestMethod.POST)
    @ResponseBody
    public String createMasterAdminUser(@Valid User user, BindingResult bindingResult, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult.rejectValue("email", "error.user", "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {
            user.setCompany(companyService.findById(1L));
            mailSendingService.sendSuperAdminNotification(user, loggedUser.getFullName());
            userService.createUser(user, "ROLE_SUPER_ADMIN");
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/api/manager/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteUser(@RequestParam Long userId, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        User userExists = userService.findUserById(userId);
        if (userExists == null || !hasAccessToUser(loggedUser, userExists)) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return "Failed";
        } else {
            userService.remove(userExists);
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/api/managers/all", method = RequestMethod.GET)
    @ResponseBody
    public Collection<User> getManagers() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        List<User> managers = userService.findManagersByCompany(loggedUser.getCompany());
        for (User manager : managers) {
            manager.getDepartments().forEach(department -> department.setOpenPositions(new ArrayList<>()));
        }

        return managers;
    }

    @RequestMapping(value = "/api/managers/all/paged", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject getManagersPaged(
            @RequestParam(defaultValue = "0", name = "page") Integer page,
            @RequestParam(defaultValue = "20", name = "size") Integer size,
            @ApiParam(value = "Sort column name like 'name'") @RequestParam(defaultValue = "name", name = "sort") String column,
            @ApiParam(value = "Search keyword") @RequestParam(defaultValue = "", name = "search") String keyword,
            @ApiParam(value = "Sort direction: asc/desc") @RequestParam(defaultValue = "asc", name = "order") String direction) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Page<User> managers = userService.findManagersAndAdminsByCompany(loggedUser.getCompany(), keyword, page, size, direction, column);
        for (User manager : managers) {
            manager.getDepartments().forEach(department -> department.setOpenPositions(new ArrayList<>()));
        }

        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(managers.getTotalElements());
        personJsonObject.setRecordsFiltered(managers.getTotalElements());
        List<User> content = managers.getContent();
        personJsonObject.setData(content);
        return personJsonObject;
    }

    @RequestMapping(value = "/api/admins/all", method = RequestMethod.GET)
    @ResponseBody
    public Collection<User> getAdmins() {
        return userService.findAllAdmins();
    }

    @RequestMapping(value = "/api/admins/masters", method = RequestMethod.GET)
    @ResponseBody
    public Collection<User> getMasterAdmins() {
        return userService.findAllMasterAdmins();
    }

    @RequestMapping(value = "/api/admins/{companyId}", method = RequestMethod.GET)
    @ResponseBody
    public Collection<User> getAdmins(@PathVariable long companyId) {
        return userService.findAdminsByCompany(companyService.findById(companyId));
    }

    @RequestMapping(value = "/api/users/{companyId}", method = RequestMethod.GET)
    @ResponseBody
    public Collection<User> getUsers(@PathVariable long companyId) {
        List<User> users = userService.findAllByCompany(companyService.findById(companyId));
        for (User manager : users) {
            manager.getDepartments().forEach(department -> department.setOpenPositions(new ArrayList<>()));
        }
        return users;
    }

    @RequestMapping(value = "/api/address/distance", method = RequestMethod.POST)
    @ResponseBody
    public Collection<TimeOnTheWay> calculateDistance(@RequestParam String originAddress,
                                                      @RequestParam String destinationAddress,
                                                      @RequestParam String time) {
        return googleMapsService.calculateTripData(originAddress, destinationAddress, CandidateUtils.getInstance().findNextMondayAtTime(time), "job");
    }

    @RequestMapping(value = "/api/address/states", method = RequestMethod.GET)
    @ResponseBody
    public Collection<String> stateList() {
        return States.list;
    }

    @RequestMapping(value = "/api/address/statesasmap", method = RequestMethod.GET)
    @ResponseBody
    public Object stateMap() {

        @Getter
        @Setter
        class USStatesDictionaryItem {
            private String key;
            private String name;

            private USStatesDictionaryItem(String key, String name) {
                this.key = key;
                this.name = name;
            }
        }

        List<USStatesDictionaryItem> states = new ArrayList<>();

        for (Map.Entry<String, String> entry : States.map.entrySet()) {
            states.add(new USStatesDictionaryItem(entry.getValue(), entry.getKey()));
        }


        return states;
    }


    /**
     * Test endpoint
     */
    private final OnetItemRepository onetItemRepository;

    @RequestMapping(value = "/web/test/qualities/match", method = RequestMethod.GET)
    @ResponseBody
    public Object testQualityMatch(@RequestParam String quality) {
        return onetItemRepository.findByElementNameIgnoreCase(quality);
    }

}
