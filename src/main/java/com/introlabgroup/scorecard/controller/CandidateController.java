package com.introlabgroup.scorecard.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.introlabgroup.scorecard.model.*;
import com.introlabgroup.scorecard.model.dto.*;
import com.introlabgroup.scorecard.model.onet.Occupation;
import com.introlabgroup.scorecard.model.pi.PowerInterview;
import com.introlabgroup.scorecard.service.CandidateService;
import com.introlabgroup.scorecard.service.CompanyService;
import com.introlabgroup.scorecard.service.PowerInterviewService;
import com.introlabgroup.scorecard.service.UserService;
import com.introlabgroup.scorecard.service.sovren.ResumeParser;
import com.introlabgroup.scorecard.utils.CandidateUtils;
import com.introlabgroup.scorecard.utils.FileUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.introlabgroup.scorecard.utils.UserUtils.isManager;

/**
 * @author vitalii.
 */
@RestController
@RequestMapping("/api/candidate")
public class CandidateController {

    private static final Logger logger = LoggerFactory.getLogger(CandidateController.class);


    private final CandidateService candidateService;
    private final CompanyService companyService;
    private final PowerInterviewService powerInterviewService;
    private final ResumeParser resumeParser;
    private final UserService userService;
    @Value("${attachments.resume.directory}")
    private String resumeDirectory;

    @Autowired
    public CandidateController(CandidateService candidateService, UserService userService, CompanyService companyService, ResumeParser resumeParser, PowerInterviewService powerInterviewService) {
        this.candidateService = candidateService;
        this.userService = userService;
        this.companyService = companyService;
        this.resumeParser = resumeParser;
        this.powerInterviewService = powerInterviewService;
    }

    @RequestMapping(value = "/references/add/{candidateId}", method = RequestMethod.POST)
    public CandidateReference addReferences(@Valid CandidateReference candidateReference, @PathVariable long candidateId) {
        candidateReference.setCandidate(candidateService.findById(candidateId));
        return candidateService.saveCandidateReference(candidateReference);
    }

    @ApiOperation(value = "Get candidate.")
    @RequestMapping(value = "/fs/{candidateId}", method = RequestMethod.GET)
    public CandidateFinalScoreCardDTO candidateDTO(@PathVariable Long candidateId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());

        Candidate candidate = candidateService.findById(candidateId);
        CandidateUtils.getInstance().calculateScores(candidate);
        return CandidateToDto(loggedUser, candidate);
    }

    private CandidateFinalScoreCardDTO CandidateToDto(User loggedUser, Candidate candidate) {
        CandidateFinalScoreCardDTO candidateDTO = new CandidateFinalScoreCardDTO();
        candidateDTO.setName(candidate.getName());
        candidateDTO.setEmail(candidate.getEmail());
        candidateDTO.setPhone(candidate.getPhone());
        candidateDTO.setId(candidate.getId());
        candidateDTO.setLastName(candidate.getLastName());
        candidateDTO.setCity(candidate.getCity());
        candidateDTO.setBuildingNumber(candidate.getBuildingNumber());
        candidateDTO.setStreet(candidate.getStreet());
        candidateDTO.setState(candidate.getState());
        candidateDTO.setZip(candidate.getZip());
        candidateDTO.setAssessments(new ArrayList<>());
        candidateDTO.setSupervisor(candidate.getSupervisor());
        candidateDTO.setCandidateReferences(candidate.getCandidateReferences());
        candidateDTO.setDrivingRecords(candidate.isDrivingRecords());
        candidateDTO.setDrugTest(candidate.isDrugTest());
        candidateDTO.setBackgroundCheck(candidate.isBackgroundCheck());
        candidateDTO.setCreditCheck(candidate.isCreditCheck());
        candidateDTO.setPhysical(candidate.isPhysical());
        candidateDTO.setResume(candidate.getResumeFileName());
        candidateDTO.setResume(candidate.getQualificationSummary());
        candidateDTO.setResume(candidate.getSovrenDescription());
        try {
            mapCandidateAssessmentToFSDTO(candidate, candidateDTO, loggedUser);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return candidateDTO;
    }

    private void mapCandidateAssessmentToFSDTO(Candidate candidate, CandidateFinalScoreCardDTO candidateDTO, User loggedUser) throws IOException {
        for (Assessment assessment : candidate.getAssessments()) {
            Long id = assessment.getJobProfile().getDepartment().getId();
            if (!isUserHasDepartment(loggedUser, id)) {
                continue;
            }

            ObjectMapper objectMapper = new ObjectMapper();
            FilledSurveyDTO assessmentDTO = new FilledSurveyDTO();
            assessmentDTO.setInterviewer(assessment.getInterviewerName());
            assessmentDTO.setId(assessment.getId());
            assessmentDTO.setJobProfileId(assessment.getJobProfile().getId());
            assessmentDTO.setName(assessment.getName());
            assessmentDTO.setAnswers(assessment.getBody());
            assessmentDTO.setOverallScore(assessment.getOverallScore());
            assessmentDTO.setOccupation(objectMapper.readValue(assessment.getOccupationJson(), Occupation.class));

            boolean flag = true;

            for (InnerCandidateAssessment candidateAssessment : candidateDTO.getAssessments()) {
                if (candidateAssessment.getJobProfileId().equals(assessment.getJobProfile().getId())) {
                    candidateAssessment.getAssessments().add(assessmentDTO);
                    flag = false;
                    break;
                }
            }

            if (flag) {

                ArrayList<FilledSurveyDTO> list = new ArrayList<>();
                list.add(assessmentDTO);
                InnerCandidateAssessment innerResult = new InnerCandidateAssessment();
                innerResult.setAssessments(list);
                innerResult.setJobProfileId(assessmentDTO.getJobProfileId());
                candidateDTO.getAssessments().add(innerResult);
            }
        }
    }

    private boolean isUserHasDepartment(User loggedUser, Long departmentId) {
        if (!isManager(loggedUser)) {
            return true;
        }
        boolean hasAccess = false;
        for (CompanyDepartment companyDepartment : loggedUser.getDepartments()) {
            if (companyDepartment.getId().longValue() == departmentId.longValue())
                hasAccess = true;
        }
        return hasAccess;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject candidates(@RequestParam(required = false) Long companyId,
                                     @RequestParam(defaultValue = "0", name = "page") Integer page,
                                     @RequestParam(defaultValue = "20", name = "size") Integer size,
                                     @ApiParam(value = "Sort column name like 'name'") @RequestParam(defaultValue = "name", name = "sort") String column,
                                     @ApiParam(value = "Search keyword") @RequestParam(defaultValue = "", name = "search") String keyword,
                                     @ApiParam(value = "Sort direction: asc/desc") @RequestParam(defaultValue = "asc", name = "order") String direction) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Company company;
        if (companyId != null) {
            company = companyService.findById(companyId);
        } else {
            company = loggedUser.getCompany();
        }

        Page<Candidate> candidates = candidateService.findAll(company, keyword, keyword, page, size, direction, column);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(candidates.getTotalElements());
        personJsonObject.setRecordsFiltered(candidates.getTotalElements());
        List<Candidate> content = candidates.getContent();

        for (Candidate candidate : content) {
            candidate.setCompany(null);
        }
        personJsonObject.setData(content);
        return personJsonObject;
    }

    @ApiOperation(value = "Get candidates. Deprecated!",
            notes = "Deprecated")
    @RequestMapping(value = "/list/old", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject candidates(HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Integer start = Integer.valueOf(request.getParameter("start"));
        Integer length = Integer.valueOf(request.getParameter("length"));
        String searchKeyword = request.getParameter("search[value]");
        String orderColumnNumber = request.getParameter("order[0][column]");
        String orderColumnName = request.getParameter("columns[" + orderColumnNumber + "][data]");
        String orderDirection = request.getParameter("order[0][dir]");
        System.out.println(searchKeyword);
        Page<Candidate> candidates = candidateService.findAll(loggedUser.getCompany(), searchKeyword, searchKeyword, start / length, length, orderDirection, orderColumnName);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(candidates.getTotalElements());
        personJsonObject.setRecordsFiltered(candidates.getTotalElements());
        List<Candidate> content = candidates.getContent();
        candidates.forEach(candidate -> candidate.setCompany(null));
        personJsonObject.setData(content);
        return personJsonObject;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = {MediaType.ALL_VALUE})
    @ResponseBody
    public String createNewCandidate(@RequestBody Candidate candidate, BindingResult bindingResult, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {

            candidate.setCompany(loggedUser.getCompany());
            candidateService.save(candidate);
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/resume/save", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseBody
    public String createNewCandidateFromFile(@RequestBody Candidate candidate, @Valid UploadedFile file) {

        candidate.setResumeFile(file.getFile());
        candidate.setResumeFileName(file.getFile().getOriginalFilename());
        final Candidate savedCandidate = candidateService.save(candidate);

        String directory = resumeDirectory + savedCandidate.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, file.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }
        return "OK";
    }

    @RequestMapping(value = "/create/resume", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseBody
    public String createNewCandidateFromFile(@Valid UploadedFile file) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());

        Candidate candidate = new Candidate();

        candidate.setName("");
        candidate.setLastName("");
        candidate.setCompany(loggedUser.getCompany());

        final Candidate savedCandidate = candidateService.save(candidate);


        String directory = resumeDirectory + savedCandidate.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, file.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }

        candidate.setResumeFile(file.getFile());
        candidate.setResumeFileName(file.getFile().getOriginalFilename());


        try {
            List<PowerInterview> byCandidateId = powerInterviewService.findByCandidateId(savedCandidate.getId(), loggedUser);
            resumeParser.parseResume(candidate, directory + candidate.getResumeFile().getOriginalFilename(), byCandidateId);
            for (PowerInterview powerInterview : byCandidateId) {
                powerInterviewService.save(powerInterview);
            }

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        candidateService.save(candidate);
        return "OK";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void deleteCandidate(@RequestParam Long candidateId) {
        candidateService.delete(candidateId);
    }

    @RequestMapping(value = "/careerhistory/add/{candidateId}", method = RequestMethod.POST)
    public CareerHistoryDTO leadingQuestions(@Valid CareerHistoryDTO historyDto, @PathVariable long candidateId) throws ParseException {

        CareerHistory careerHistory = new CareerHistory();
        careerHistory.setCompany(historyDto.getCompany());
        careerHistory.setJobTitle(historyDto.getJobTitle());
        careerHistory.setCandidate(candidateService.findById(candidateId));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        careerHistory.setStartDate(dateFormat.parse(historyDto.getStartDate()));
        careerHistory.setEndDate(dateFormat.parse(historyDto.getEndDate()));
        return CandidateUtils.getInstance().careerHistoryToDto(candidateService.addCareerHistory(careerHistory));
    }

    @RequestMapping(value = "/resume/parse", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Candidate parseResume(@Valid UploadedFile file) throws IOException {

        String directory = resumeDirectory + "temp/";
        try {
            FileUtils.saveAttachment(directory, file.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }

        Candidate candidate = new Candidate();

        candidate.setResumeFile(file.getFile());
        candidate.setResumeFileName(file.getFile().getOriginalFilename());

        resumeParser.parseResume(candidate, directory + file.getFile().getOriginalFilename(), Collections.emptyList());
        Path filePath = Paths.get(directory + file.getFile().getOriginalFilename());
        Files.deleteIfExists(filePath);
        new File(directory + file.getFile().getOriginalFilename()).deleteOnExit();
        candidate.setResumeFile(null);
        return candidate;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.ALL_VALUE})
    @ResponseBody
    public String update(@Valid Candidate candidate, BindingResult bindingResult, HttpServletResponse response, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {
            Candidate existedCandidate = candidateService.findById(candidate.getId());
            existedCandidate.setLastName(candidate.getLastName());
            existedCandidate.setName(candidate.getName());
            existedCandidate.setBuildingNumber(candidate.getBuildingNumber());
            existedCandidate.setCity(candidate.getCity());
            existedCandidate.setStreet(candidate.getStreet());
            existedCandidate.setZip(candidate.getZip());
            existedCandidate.setState(candidate.getState());
            existedCandidate.setSupervisor(candidate.getSupervisor());


            MultipartFile file = null;
            try {
                file = ((StandardMultipartHttpServletRequest) request).getFile("file");
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            System.out.println(file);
            if (file != null) {
                //parse resume
                String directory = resumeDirectory + existedCandidate.getId() + "/";
                try {
                    FileUtils.saveAttachment(directory, file);
                } catch (IOException ie) {
                    logger.error(ie.getMessage(), ie);
                }

                existedCandidate.setResumeFileName(file.getOriginalFilename());
                existedCandidate.setResumeFile(file);

                try {
                    resumeParser.parseResume(existedCandidate, directory + candidate.getResumeFile().getOriginalFilename(), Collections.emptyList());
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            candidateService.save(existedCandidate);
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/factors/backgroundcheck", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String updateBackgroundCheck(@RequestParam Long candidateId) {
        Candidate candidate = candidateService.findById(candidateId);
        candidate.setBackgroundCheck(!candidate.isBackgroundCheck());
        candidateService.save(candidate);
        candidateService.save(candidate);
        return "OK";
    }

    @RequestMapping(value = "/factors/creditcheck", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String updateCreditCheck(@RequestParam Long candidateId) {
        Candidate candidate = candidateService.findById(candidateId);
        candidate.setCreditCheck(!candidate.isCreditCheck());
        candidateService.save(candidate);
        return "OK";
    }

    @RequestMapping(value = "/factors/drivingrecords", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String updateDrivingRecords(@RequestParam Long candidateId) {
        Candidate candidate = candidateService.findById(candidateId);
        candidate.setDrivingRecords(!candidate.isDrivingRecords());
        candidateService.save(candidate);
        return "OK";
    }

    @RequestMapping(value = "/factors/drugtest", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String updateDrugTest(@RequestParam Long candidateId) {
        Candidate candidate = candidateService.findById(candidateId);
        candidate.setDrugTest(!candidate.isDrugTest());
        candidateService.save(candidate);
        return "OK";
    }

    @RequestMapping(value = "/factors/physicalcheck", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String updatePhysicalCheck(@RequestParam Long candidateId) {
        Candidate candidate = candidateService.findById(candidateId);
        candidate.setPhysical(!candidate.isPhysical());
        candidateService.save(candidate);
        return "OK";
    }

    @RequestMapping(value = "/references/update", method = RequestMethod.POST)
    public CandidateReference updateReferences(@Valid CandidateReference candidateReference, @RequestParam long candidateId) {
        candidateReference.setCandidate(candidateService.findById(candidateId));
        return candidateService.saveCandidateReference(candidateReference);
    }

    @RequestMapping(value = "/resume/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public CandidateFinalScoreCardDTO uploadWorkSample(@Valid UploadedFile file, @RequestParam Long candidateId) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());

        String directory = resumeDirectory + candidateId + "/";
        try {
            FileUtils.saveAttachment(directory, file.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }

        Candidate candidate = candidateService.findById(candidateId);

        candidate.setResumeFile(file.getFile());
        candidate.setResumeFileName(file.getFile().getOriginalFilename());


        try {
            List<PowerInterview> byCandidateId = powerInterviewService.findByCandidateId(candidateId, loggedUser);
            resumeParser.parseResume(candidate, directory + candidate.getResumeFile().getOriginalFilename(), byCandidateId);
            for (PowerInterview powerInterview : byCandidateId) {
                powerInterviewService.save(powerInterview);
            }

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        candidateService.save(candidate);

        CandidateUtils.getInstance().calculateScores(candidate);

        return CandidateToDto(loggedUser, candidate);
    }
}
