package com.introlabgroup.scorecard.controller;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.service.CompanyService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

/**
 * @author vitalii.
 */
@RestController
@RequestMapping("/api/company")
public class CompanyController {

    private final CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public String createNewCompany(@Valid Company company, BindingResult bindingResult, HttpServletResponse response) {
        if (companyService.findByName(company.getName()) != null) {
            bindingResult
                    .rejectValue("Name", "error.company",
                            "There is already a company registered with the name provided");
        }
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {
            company.setAllowUsage(true);
            company.setTrial(true);
            company.setAllowScorecards(true);
            company.setCreated(new Date());
            company.setEndOfPeriod(DateUtils.addMonths(new Date(), 1));
            companyService.saveCompany(company);
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteCompany(@RequestParam Long companyId, HttpServletResponse response) {

        companyService.delete(companyId);
        response.setStatus(HttpServletResponse.SC_OK);
        return "OK";
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Company> getCompanies() {
        return companyService.findAll();
    }
}
