package com.introlabgroup.scorecard.controller;

import com.introlabgroup.scorecard.model.*;
import com.introlabgroup.scorecard.model.dto.DataJsonObject;
import com.introlabgroup.scorecard.service.*;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

/**
 * @author vitalii.
 */
@RestController
@RequestMapping("/api/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    private final UserService userService;

    private final CompanyService companyService;

    private final SurveyService surveyService;

    private final JobProfileService jobProfileService;

    @Autowired
    public DepartmentController(DepartmentService departmentService, UserService userService, CompanyService companyService, SurveyService surveyService, JobProfileService jobProfileService) {
        this.departmentService = departmentService;
        this.userService = userService;
        this.companyService = companyService;
        this.surveyService = surveyService;
        this.jobProfileService = jobProfileService;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public CompanyDepartment findDepartment(@RequestParam long id) {
        return departmentService.findById(id, false);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid CompanyDepartment department,
                         @RequestParam(value = "companyId", required = false) Long companyId,
                         BindingResult bindingResult, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Company company;
        if (companyId != null) {
            company = companyService.findById(companyId);
        } else {
            company = loggedUser.getCompany();
        }
        if (departmentService.findByNameAndCompany(department.getName(), company) != null) {
            bindingResult.rejectValue("Name", "error.department", "There is already a department registered with the name provided");
        }
        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {
            department.setCompany(company);
            departmentService.saveDepartment(department);
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid CompanyDepartment department,
                         BindingResult bindingResult, HttpServletResponse response) {

        if (bindingResult.hasErrors()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "Failed";
        } else {
            CompanyDepartment existedDepartment = departmentService.findById(department.getId(), false);
            existedDepartment.setName(department.getName());
            existedDepartment.setAddress(department.getAddress());
            existedDepartment.setCity(department.getCity());
            existedDepartment.setDescription(department.getDescription());
            existedDepartment.setState(department.getState());
            existedDepartment.setEmail(department.getEmail());
            existedDepartment.setZip(department.getZip());
            existedDepartment.setPhone(department.getPhone());
            departmentService.saveDepartment(existedDepartment);
            response.setStatus(HttpServletResponse.SC_OK);
            return "OK";
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteDepartment(@RequestParam Long departmentId, HttpServletResponse response) {
        CompanyDepartment department = departmentService.findById(departmentId, false);
        deleteDepartmentData(department);

        departmentService.delete(departmentId);
        response.setStatus(HttpServletResponse.SC_OK);
        return "OK";
    }

    private void deleteDepartmentData(CompanyDepartment department) {
        for (JobProfile profile : department.getOpenPositions()) {
            surveyService.deleteAssessmentsByJobProfileId(profile);
            jobProfileService.delete(profile);
        }
    }

    @RequestMapping(value = "/allinone", method = RequestMethod.GET)
    @ResponseBody
    public Collection<CompanyDepartment> getDepartments(@RequestParam long companyId) {
        return departmentService.findAllByCompany(companyService.findById(companyId), true);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject getDepartments(
            @RequestParam(defaultValue = "0", name = "page") Integer page,
            @RequestParam(defaultValue = "20", name = "size") Integer size,
            @ApiParam(value = "Sort column name like 'name'") @RequestParam(defaultValue = "name", name = "sort") String column,
            @ApiParam(value = "Search keyword") @RequestParam(defaultValue = "", name = "search") String keyword,
            @ApiParam(value = "Sort direction: asc/desc") @RequestParam(defaultValue = "asc", name = "order") String direction) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Page<CompanyDepartment> departments = departmentService.findAllByCompany(loggedUser.getCompany(),  keyword, page, size, direction, column, true);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(departments.getTotalElements());
        personJsonObject.setRecordsFiltered(departments.getTotalElements());
        List<CompanyDepartment> content = departments.getContent();
        personJsonObject.setData(content);
        return personJsonObject;
    }

    @RequestMapping(value = "/forcompany", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject getDepartments(
            @RequestParam long companyId,
            @RequestParam(defaultValue = "0", name = "page") Integer page,
            @RequestParam(defaultValue = "20", name = "size") Integer size,
            @ApiParam(value = "Sort column name like 'name'") @RequestParam(defaultValue = "name", name = "sort") String column,
            @ApiParam(value = "Search keyword") @RequestParam(defaultValue = "", name = "search") String keyword,
            @ApiParam(value = "Sort direction: asc/desc") @RequestParam(defaultValue = "asc", name = "order") String direction) {
        Page<CompanyDepartment> departments = departmentService.findAllByCompany(companyService.findById(companyId), keyword, page, size, direction, column, true);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(departments.getTotalElements());
        personJsonObject.setRecordsFiltered(departments.getTotalElements());
        List<CompanyDepartment> content = departments.getContent();
        personJsonObject.setData(content);
        return personJsonObject;
    }
}
