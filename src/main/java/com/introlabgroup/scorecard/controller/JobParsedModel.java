package com.introlabgroup.scorecard.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author vitalii.
 */
@Getter
@Setter
@AllArgsConstructor
public class JobParsedModel {

    private String title;
    private List<String> skills;

}
