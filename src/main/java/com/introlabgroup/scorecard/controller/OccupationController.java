package com.introlabgroup.scorecard.controller;

import com.introlabgroup.scorecard.model.onet.CustomQuality;
import com.introlabgroup.scorecard.model.onet.Occupation;
import com.introlabgroup.scorecard.model.onet.OnetItem;
import com.introlabgroup.scorecard.repository.onet.CustomQualityRepository;
import com.introlabgroup.scorecard.repository.onet.OccupationRepository;
import com.introlabgroup.scorecard.repository.onet.OnetItemRepository;
import com.introlabgroup.scorecard.service.SurveyService;
import com.introlabgroup.scorecard.utils.CandidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author vitalii.
 */
@RestController
@RequestMapping("/api/occupations")
public class OccupationController {

    private final SurveyService surveyService;

    private final OnetItemRepository onetItemRepository;

    private final CustomQualityRepository qualityRepository;

    private final OccupationRepository occupationRepository;

    @Autowired
    public OccupationController(SurveyService surveyService, OnetItemRepository onetItemRepository, CustomQualityRepository qualityRepository, OccupationRepository occupationRepository) {
        this.surveyService = surveyService;
        this.onetItemRepository = onetItemRepository;
        this.qualityRepository = qualityRepository;
        this.occupationRepository = occupationRepository;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public List<Occupation> findOccupations(@RequestParam String keyword) {
        return surveyService.searchOccupation(keyword);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Occupation findOccupation(@RequestParam String keyword) {
        return surveyService.findOneOccupation(keyword);
    }

    @RequestMapping(value = "/addcustom", method = RequestMethod.GET)
    public List<CustomQuality> addCustomQuality(@RequestParam String keyword, @RequestParam String qualityId) {
        OnetItem one = onetItemRepository.findOne(qualityId);
        CustomQuality customQuality = new CustomQuality();
        customQuality.setOnetItem(one);
        customQuality.setOccupation(keyword);
        customQuality.setWeight(4D);
        qualityRepository.save(customQuality);
        return occupationRepository.findOne(keyword).getCustomQualities();
    }

    @RequestMapping(value = "/qualities/addown", method = RequestMethod.GET)
    public List<CustomQuality> addMyOwnQuality(@RequestParam String keyword, @RequestParam String name, @RequestParam(required = false) String description) {
        OnetItem onetItem = new OnetItem();
        onetItem.setCustom(true);
        onetItem.setDescription(description);
        onetItem.setElementName(name);
        onetItem.setElementId(CandidateUtils.getInstance().generateCustomId(name));
        OnetItem one = onetItemRepository.save(onetItem);
        CustomQuality customQuality = new CustomQuality();
        customQuality.setOnetItem(one);
        customQuality.setOccupation(keyword);
        customQuality.setWeight(4D);
        qualityRepository.save(customQuality);
        return occupationRepository.findOne(keyword).getCustomQualities();
    }

    @RequestMapping(value = "/custom-qualities/search", method = RequestMethod.POST)
    public List<OnetItem> findCustomQualities(@RequestParam String keyword) {
        return onetItemRepository.findAllByElementNameContainsAndCustom(keyword, true);
    }
}
