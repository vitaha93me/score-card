package com.introlabgroup.scorecard.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.JobProfile;
import com.introlabgroup.scorecard.model.User;
import com.introlabgroup.scorecard.model.dto.JobProfileStatusObject;
import com.introlabgroup.scorecard.model.onet.PaymentPeriod;
import com.introlabgroup.scorecard.model.onet.PaymentPeriodEnumConverter;
import com.introlabgroup.scorecard.model.payment.StripeData;
import com.introlabgroup.scorecard.model.payment.StripeEvent;
import com.introlabgroup.scorecard.model.payment.StripeSubscription;
import com.introlabgroup.scorecard.model.payment.SubscriptionDTO;
import com.introlabgroup.scorecard.service.CompanyService;
import com.introlabgroup.scorecard.service.JobProfileService;
import com.introlabgroup.scorecard.service.StripeService;
import com.introlabgroup.scorecard.service.UserService;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.Plan;
import com.stripe.model.Subscription;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

import static com.introlabgroup.scorecard.utils.CommonUtils.extractPostRequestBody;

@RestController
@RequestMapping
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(SurveyController.class);

    private final UserService userService;

    private final CompanyService companyService;

    private final StripeService stripeService;

    private final JobProfileService jobProfileService;

    @Value("${stripe.webhook.secret}")
    private String webHookSecret;

    @Autowired
    public PaymentController(UserService userService, CompanyService companyService, StripeService stripeService, JobProfileService jobProfileService) {
        this.userService = userService;
        this.companyService = companyService;
        this.stripeService = stripeService;
        this.jobProfileService = jobProfileService;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(PaymentPeriod.class, new PaymentPeriodEnumConverter());
    }

    @RequestMapping(value = "/api/payment/setup", method = RequestMethod.POST)
    public void setup(@RequestBody SubscriptionDTO subscriptionDto, HttpServletResponse response) throws Exception {
        Company company = companyService.findById(subscriptionDto.getCompanyId());
        List<StripeSubscription> stripeSubscriptions = stripeService.getStripeDao().getStripeActiveSubscriptionsByCompanyId(company.getId());
        if (!stripeSubscriptions.isEmpty()) {
            throw new Exception("Customer already subscribed");
        }

        stripeService.calculatePrice(subscriptionDto);

        Plan plan = stripeService.createTariffPlan(subscriptionDto);

        Customer customer = stripeService.createCustomer(subscriptionDto);

        StripeData stripeData = new StripeData(subscriptionDto.getCompanyId(), customer.getId());
        stripeService.getStripeDao().insertStripeData(stripeData);

        Subscription subscription = stripeService.subscribeCustomer(plan, customer);


        StripeSubscription stripeSubscription = new StripeSubscription();
        stripeSubscription.setCompanyId(subscriptionDto.getCompanyId());
        stripeSubscription.setCurrentStatus(true);
        stripeSubscription.setSubscriptionId(subscription.getId());
        stripeSubscription.setEmail(subscriptionDto.getEmail());
        stripeSubscription.setPrice(subscriptionDto.getPrice());
        stripeSubscription.setScoreCardCount(subscriptionDto.getScorecardCount());
        stripeSubscription.setScorecardsLeft(subscriptionDto.getScorecardCount());
        stripeSubscription.setType(subscriptionDto.getPaymentPeriod().getValue());

        List<StripeSubscription> deactivated = stripeService.getStripeDao().getStripeDeActivatedSubscriptionsByCompanyId(company.getId());
        if (!deactivated.isEmpty()) {
            stripeService.getStripeDao().updateStripeStripeSubscription(stripeSubscription);
        } else {
            stripeService.getStripeDao().insertStripeStripeSubscription(stripeSubscription);
        }

        company.setTrial(false);
        company.setPaid(true);
        company.setAllowUsage(true);
        company.setAllowScorecards(true);
        company.setFree(false);

        setEndOfPeriod(subscriptionDto, company);

        companyService.saveCompany(company);

        for (JobProfileStatusObject scorecardStatus : subscriptionDto.getScorecardStatuses()) {
            JobProfile jobProfile = jobProfileService.findById(scorecardStatus.getScorecardId());
            jobProfile.setStatus(scorecardStatus.isStatusActive());
            jobProfileService.saveJobProfile(jobProfile);
        }

        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = "/api/payment/setupfree", method = RequestMethod.POST)
    public void setupFree(@RequestBody SubscriptionDTO subscriptionDto, HttpServletResponse response) throws Exception {
        Company company = companyService.findById(subscriptionDto.getCompanyId());
        List<StripeSubscription> stripeSubscriptions = stripeService.getStripeDao().getStripeActiveSubscriptionsByCompanyId(company.getId());
        if (!stripeSubscriptions.isEmpty()) {
            throw new Exception("Customer already subscribed");
        }

        stripeService.calculatePrice(subscriptionDto);

        StripeSubscription stripeSubscription = new StripeSubscription();
        stripeSubscription.setCompanyId(subscriptionDto.getCompanyId());
        stripeSubscription.setSubscriptionId("");
        stripeSubscription.setCurrentStatus(true);
        stripeSubscription.setEmail(subscriptionDto.getEmail());
        stripeSubscription.setPrice(subscriptionDto.getPrice());
        stripeSubscription.setScoreCardCount(subscriptionDto.getScorecardCount());
        stripeSubscription.setScorecardsLeft(subscriptionDto.getScorecardCount());
        stripeSubscription.setType(subscriptionDto.getPaymentPeriod().getValue());

        List<StripeSubscription> deactivated = stripeService.getStripeDao().getStripeDeActivatedSubscriptionsByCompanyId(company.getId());
        if (!deactivated.isEmpty()) {
            stripeService.getStripeDao().updateStripeStripeSubscription(stripeSubscription);
        } else {
            stripeService.getStripeDao().insertStripeStripeSubscription(stripeSubscription);
        }

        company.setTrial(false);
        company.setPaid(true);
        company.setAllowUsage(true);
        company.setFree(true);
        company.setAllowScorecards(true);

        setEndOfPeriod(subscriptionDto, company);

        for (JobProfileStatusObject scorecardStatus : subscriptionDto.getScorecardStatuses()) {
            JobProfile jobProfile = jobProfileService.findById(scorecardStatus.getScorecardId());
            jobProfile.setStatus(scorecardStatus.isStatusActive());
            jobProfileService.saveJobProfile(jobProfile);
        }


        companyService.saveCompany(company);

        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = "/api/payment/update", method = RequestMethod.POST)
    public void update(@RequestBody SubscriptionDTO subscriptionDto, HttpServletResponse response) throws Exception {
        Company company = companyService.findById(subscriptionDto.getCompanyId());
        List<StripeSubscription> stripeSubscriptions = stripeService.getStripeDao().getStripeAllSubscriptionsByCompanyId(company.getId());
        if (!stripeSubscriptions.isEmpty()) {

            StripeSubscription stripeSubscription = stripeSubscriptions.get(0);
            stripeService.calculatePrice(subscriptionDto);

            if (company.isFree() || stripeSubscription.getSubscriptionId() == null || stripeSubscription.getSubscriptionId().isEmpty()) {

                Plan plan = stripeService.createTariffPlan(subscriptionDto);

                Customer customer = stripeService.createCustomer(subscriptionDto);

                StripeData stripeData = new StripeData(subscriptionDto.getCompanyId(), customer.getId());
                stripeService.getStripeDao().insertStripeData(stripeData);

                Subscription subscription = stripeService.subscribeCustomer(plan, customer);

                stripeSubscription = new StripeSubscription();
                stripeSubscription.setCompanyId(subscriptionDto.getCompanyId());
                stripeSubscription.setCurrentStatus(true);
                stripeSubscription.setSubscriptionId(subscription.getId());
                stripeSubscription.setEmail(subscriptionDto.getEmail());
                stripeSubscription.setPrice(subscriptionDto.getPrice());
                stripeSubscription.setScoreCardCount(subscriptionDto.getScorecardCount());
                stripeSubscription.setScorecardsLeft(subscriptionDto.getScorecardCount());
                stripeSubscription.setType(subscriptionDto.getPaymentPeriod().getValue());

                stripeService.getStripeDao().updateStripeStripeSubscription(stripeSubscription);

                company.setTrial(false);
                company.setPaid(true);
                company.setAllowUsage(true);
                company.setFree(false);
                company.setAllowScorecards(true);

                setEndOfPeriod(subscriptionDto, company);

                for (JobProfileStatusObject scorecardStatus : subscriptionDto.getScorecardStatuses()) {
                    JobProfile jobProfile = jobProfileService.findById(scorecardStatus.getScorecardId());
                    jobProfile.setStatus(scorecardStatus.isStatusActive());
                    jobProfileService.saveJobProfile(jobProfile);
                }

                companyService.saveCompany(company);

            } else {

                Subscription subscription = stripeService.updateSubscription(stripeSubscription, subscriptionDto);

                stripeSubscription.setCurrentStatus(true);
                stripeSubscription.setSubscriptionId(subscription.getId());
                stripeSubscription.setEmail(subscriptionDto.getEmail());
                stripeSubscription.setPrice(subscriptionDto.getPrice());
                stripeSubscription.setScoreCardCount(subscriptionDto.getScorecardCount());
                stripeSubscription.setScorecardsLeft(subscriptionDto.getScorecardCount());
                stripeSubscription.setType(subscriptionDto.getPaymentPeriod().getValue());
                stripeService.getStripeDao().updateStripeStripeSubscription(stripeSubscription);
                stripeService.getStripeDao().updateLastPaymentDate(stripeSubscription.getSubscriptionId());
                company.setTrial(false);
                company.setPaid(true);
                company.setAllowUsage(true);
                company.setAllowScorecards(true);
                company.setFree(false);

                setEndOfPeriod(subscriptionDto, company);

                for (JobProfileStatusObject scorecardStatus : subscriptionDto.getScorecardStatuses()) {
                    JobProfile jobProfile = jobProfileService.findById(scorecardStatus.getScorecardId());
                    jobProfile.setStatus(scorecardStatus.isStatusActive());
                    jobProfileService.saveJobProfile(jobProfile);
                }

                companyService.saveCompany(company);
            }
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            throw new Exception("Customer not subscribed yet");
        }
    }

    private void setEndOfPeriod(@RequestBody SubscriptionDTO subscriptionDto, Company company) {
        if (subscriptionDto.getPaymentPeriod() == PaymentPeriod.MONTHLY) {
            company.setEndOfPeriod(DateUtils.addMonths(new Date(), 1));
        } else if (subscriptionDto.getPaymentPeriod() == PaymentPeriod.ANNUALLY) {
            company.setEndOfPeriod(DateUtils.addYears(new Date(), 1));
        } else {
            company.setEndOfPeriod(DateUtils.addDays(new Date(), 1));
        }
    }


    @RequestMapping(value = "/api/payment/updatetofree", method = RequestMethod.POST)
    public void updateToFree(@RequestBody SubscriptionDTO subscriptionDto, HttpServletResponse response) throws Exception {
        Company company = companyService.findById(subscriptionDto.getCompanyId());
        List<StripeSubscription> stripeSubscriptions = stripeService.getStripeDao().getStripeAllSubscriptionsByCompanyId(company.getId());
        if (!stripeSubscriptions.isEmpty()) {

            StripeSubscription stripeSubscription = stripeSubscriptions.get(0);
            if (!company.isFree() && stripeSubscription.getSubscriptionId() != null && !stripeSubscription.getSubscriptionId().isEmpty()) {
                stripeService.cancelSubscription(stripeSubscription.getSubscriptionId());
            }
            stripeService.calculatePrice(subscriptionDto);

            stripeSubscription.setCurrentStatus(true);
            stripeSubscription.setSubscriptionId("");
            stripeSubscription.setEmail(subscriptionDto.getEmail());
            stripeSubscription.setPrice(subscriptionDto.getPrice());
            stripeSubscription.setScoreCardCount(subscriptionDto.getScorecardCount());
            stripeSubscription.setScorecardsLeft(subscriptionDto.getScorecardCount());
            stripeSubscription.setType(subscriptionDto.getPaymentPeriod().getValue());
            stripeService.getStripeDao().updateStripeStripeSubscription(stripeSubscription);
            stripeService.getStripeDao().updateLastPaymentDate(stripeSubscription.getSubscriptionId());
            company.setTrial(false);
            company.setPaid(true);
            company.setFree(true);
            company.setAllowUsage(true);
            company.setAllowScorecards(true);
            setEndOfPeriod(subscriptionDto, company);

            for (JobProfileStatusObject scorecardStatus : subscriptionDto.getScorecardStatuses()) {
                JobProfile jobProfile = jobProfileService.findById(scorecardStatus.getScorecardId());
                jobProfile.setStatus(scorecardStatus.isStatusActive());
                jobProfileService.saveJobProfile(jobProfile);
            }

            companyService.saveCompany(company);
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            throw new Exception("Customer not subscribed yet");
        }
    }

    @RequestMapping(value = "/api/payment/cancel", method = RequestMethod.POST)
    public void cancel(HttpServletResponse response) throws StripeException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Long companyId = loggedUser.getCompany().getId();
        StripeSubscription subscription = stripeService.getStripeDao().getStripeSubscriptionByCompanyId(companyId);
        stripeService.cancelSubscription(subscription.getSubscriptionId());
        subscription.setCurrentStatus(false);
        stripeService.getStripeDao().updateStripeStripeSubscription(subscription);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = "/api/payment/cancelasadmin/{companyId}", method = RequestMethod.POST)
    public void cancelAsAdmin(@PathVariable Long companyId, HttpServletResponse response) throws StripeException {
        StripeSubscription subscription = stripeService.getStripeDao().getStripeSubscriptionByCompanyId(companyId);
        Company company = companyService.findById(companyId);
        if (company.isFree()) {
            company.setFree(false);
            companyService.saveCompany(company);
        } else {
            stripeService.cancelSubscription(subscription.getSubscriptionId());
        }
        subscription.setCurrentStatus(false);
        stripeService.getStripeDao().updateStripeStripeSubscription(subscription);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = "/web/payment/handle", method = RequestMethod.POST)
    public Object handleWebHook(HttpServletRequest httpRequest, HttpServletResponse response) {

        String requestBody = extractPostRequestBody(httpRequest);


//        String sigHeader = httpRequest.getHeader("Stripe-Signature");
//        try {
//            Webhook.constructEvent(requestBody, sigHeader, webHookSecret);
//
//        } catch (JsonSyntaxException | SignatureVerificationException e) {
//            // Invalid payload
//            response.setStatus(400);
//            return "";
//        }

        // Do something with event
        JsonElement json = new JsonParser().parse(requestBody);
        String eventType = json.getAsJsonObject().getAsJsonPrimitive("type").getAsString();
        if (!eventType.equals("invoice.payment_succeeded") && !eventType.equals("invoice.payment_failed")) {
            response.setStatus(200);
            logger.info("Not handled type " + eventType);
            return "";
        }
        logger.info(requestBody);

        StripeEvent stripeEvent = new StripeEvent();
        stripeEvent.setCreated(new Date());
        String subscriptionId = json.getAsJsonObject().getAsJsonObject("data").getAsJsonObject("object").getAsJsonPrimitive("subscription").getAsString();
        String customerId = json.getAsJsonObject().getAsJsonObject("data").getAsJsonObject("object").getAsJsonPrimitive("customer").getAsString();
        stripeEvent.setSubscriptionId(subscriptionId);
        StripeData stripeData = stripeService.getStripeDao().getStripeDataByStripeId(customerId);
        Company company = companyService.findById(stripeData.getCompanyId());
        stripeEvent.setCompanyName(company.getName());
        stripeEvent.setCompanyId(company.getId());

        switch (eventType) {
            case "invoice.payment_succeeded":
                stripeEvent.setStatus(true);
                stripeService.getStripeDao().insertStripeEvent(stripeEvent);
                stripeService.getStripeDao().resetScorecardCount(company.getId());
                company.setAllowScorecards(true);
                company.setAllowUsage(true);
                company.setReason("");

                StripeSubscription stripeSubscription = stripeService.getStripeDao().getStripeSubscriptionByCompanyId(company.getId());
                if (stripeSubscription.getType().equals(PaymentPeriod.MONTHLY.getValue())) {
                    company.setEndOfPeriod(DateUtils.addMonths(new Date(), 1));
                } else if (stripeSubscription.getType().equals(PaymentPeriod.ANNUALLY.getValue())) {
                    company.setEndOfPeriod(DateUtils.addYears(new Date(), 1));
                } else {
                    company.setEndOfPeriod(DateUtils.addDays(new Date(), 1));
                }

                companyService.saveCompany(company);

                stripeService.getStripeDao().updateLastPaymentDate(subscriptionId);
                response.setStatus(200);
                return "";
            case "invoice.payment_failed":
                stripeEvent.setStatus(false);
                stripeService.getStripeDao().insertStripeEvent(stripeEvent);
                company.setReason("Payment failed");
                companyService.saveCompany(company);
                response.setStatus(200);
                return "";
            default:
                response.setStatus(200);
                return "";
        }

    }


    @RequestMapping(value = "/subscription_status/{companyId}", method = RequestMethod.GET)
    @ResponseBody
    public String isAllowCreateScorecard(@PathVariable long companyId) throws JsonProcessingException {

        @Getter
        @Setter
        class TempClass {
            private boolean allowScorecards;
            private String reason;
        }

        StripeSubscription subscription = stripeService.getStripeDao().getStripeSubscriptionByCompanyId(companyId);

        TempClass tempClass = new TempClass();

        Company company = companyService.findById(companyId);
        if (!company.isAllowScorecards()) {
            tempClass.setAllowScorecards(false);
            tempClass.setReason(company.getReason());
        } else {
            if (subscription.getScorecardsLeft() > 0) {
                tempClass.setAllowScorecards(true);
                tempClass.setReason("");
            } else {
                tempClass.setAllowScorecards(true);
                tempClass.setReason("Scorecards limit reached");
                company.setAllowScorecards(false);
                company.setReason("Scorecards limit reached");
                companyService.saveCompany(company);
            }
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(tempClass);

    }

}
