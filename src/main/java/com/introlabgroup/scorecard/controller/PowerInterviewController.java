package com.introlabgroup.scorecard.controller;

import com.introlabgroup.scorecard.model.*;
import com.introlabgroup.scorecard.model.pi.*;
import com.introlabgroup.scorecard.model.pi.Assessment;
import com.introlabgroup.scorecard.repository.pi.JobAddressDetailsRepository;
import com.introlabgroup.scorecard.service.*;
import com.introlabgroup.scorecard.utils.CandidateUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author vitalii.
 */
@RestController
@RequestMapping("/api/pi")
public class PowerInterviewController {

    @Value("${attachments.samples.directory}")
    private String samplesDirectory;

    @Value("${attachments.certifications.directory}")
    private String certificationsDirectory;

    @Value("${attachments.licenses.directory}")
    private String licensesDirectory;

    @Value("${attachments.academic.directory}")
    private String academicHistoryDirectory;

    @Value("${attachments.assessments.directory}")
    private String assessmentsDirectory;

    @Value("${attachments.letters.directory}")
    private String lettersDirectory;

    @Value("${attachments.factors.directory}")
    private String factorsDirectory;

    private final JobProfileService jobProfileService;

    private final CandidateService candidateService;

    private final PowerInterviewService powerInterviewService;

    private final GoogleMapsService mapsService;

    private final JobAddressDetailsRepository addressDetailsRepository;

    private final UserService userService;

    @Autowired
    public PowerInterviewController(JobProfileService jobProfileService, CandidateService candidateService, PowerInterviewService powerInterviewService, GoogleMapsService mapsService, JobAddressDetailsRepository addressDetailsRepository, UserService userService) {
        this.jobProfileService = jobProfileService;
        this.candidateService = candidateService;
        this.powerInterviewService = powerInterviewService;
        this.mapsService = mapsService;
        this.addressDetailsRepository = addressDetailsRepository;
        this.userService = userService;
    }

    @RequestMapping(value = "/samples/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public WorkSample uploadWorkSample(@Valid WorkSample workSample, @RequestParam Long interviewId) {
        PowerInterview interview = powerInterviewService.uploadWorkSample(workSample, interviewId);
        return interview.getWorkSamples().stream().max(Comparator.comparing(WorkSample::getId)).orElse(new WorkSample());
    }

    @RequestMapping(value = "/samples/update", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void updateWorkSample(@Valid WorkSample workSample, @RequestParam Long interviewId) {
        powerInterviewService.updateWorkSample(workSample, interviewId);
    }

    @RequestMapping(value = "/samples/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadSample(@RequestParam Long interviewId, @RequestParam String name) throws FileNotFoundException {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        File file = new File(samplesDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/" + name);
        return downloadAttachment(file);
    }

    @RequestMapping(value = "/certifications/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Certifications uploadCertifications(@Valid Certifications certifications, @RequestParam Long interviewId) {
        PowerInterview interview = powerInterviewService.uploadCertifications(certifications, interviewId);
        return interview.getCertifications().stream().max(Comparator.comparing(Certifications::getId)).orElse(new Certifications());
    }


    @RequestMapping(value = "/certifications/update", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void updateCertifications(@Valid Certifications certifications, @RequestParam Long interviewId) {
        powerInterviewService.updateCertification(certifications, interviewId);
    }

    @RequestMapping(value = "/certifications/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadCertification(@RequestParam Long interviewId, @RequestParam String name) throws FileNotFoundException {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        File file = new File(certificationsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/" + name);
        return downloadAttachment(file);
    }

    @RequestMapping(value = "/licenses/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Certifications uploadLicenses(@Valid Licenses certifications, @RequestParam Long interviewId) {
        PowerInterview interview = powerInterviewService.uploadLicenses(certifications, interviewId);
        return interview.getCertifications().stream().max(Comparator.comparing(Certifications::getId)).orElse(new Certifications());
    }


    @RequestMapping(value = "/licenses/update", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void updateLicenses(@Valid Licenses certifications, @RequestParam Long interviewId) {
        powerInterviewService.updateLicenses(certifications, interviewId);
    }

    @RequestMapping(value = "/licenses/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadLicenses(@RequestParam Long interviewId, @RequestParam String name) throws FileNotFoundException {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        File file = new File(licensesDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/" + name);
        return downloadAttachment(file);
    }

    @RequestMapping(value = "/academic/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Certifications uploadAcademicHistory(@Valid AcademicHistory certifications, @RequestParam Long interviewId) {
        PowerInterview interview = powerInterviewService.uploadAcademicHistory(certifications, interviewId);
        return interview.getCertifications().stream().max(Comparator.comparing(Certifications::getId)).orElse(new Certifications());
    }


    @RequestMapping(value = "/academic/update", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void updateDownloadAcademicHistory(@Valid AcademicHistory certifications, @RequestParam Long interviewId) {
        powerInterviewService.updateAcademicHistory(certifications, interviewId);
    }

    @RequestMapping(value = "/academic/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadDownloadAcademicHistory(@RequestParam Long interviewId, @RequestParam String name) throws FileNotFoundException {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        File file = new File(academicHistoryDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/" + name);
        return downloadAttachment(file);
    }

    @RequestMapping(value = "/assessments/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Assessment uploadAssessments(@Valid Assessment assessment, @RequestParam(required = false) Long interviewId) {
        PowerInterview interview = powerInterviewService.uploadAssessment(assessment, interviewId);
        return interview.getAssessments().stream().max(Comparator.comparing(Assessment::getId)).orElse(new Assessment());
    }

    @RequestMapping(value = "/assessments/update", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void updateAssessments(@Valid Assessment assessment, @RequestParam(required = false) Long interviewId) {
        powerInterviewService.updateAssessment(assessment, interviewId);
    }

    @RequestMapping(value = "/assessments/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadAssessments(@RequestParam Long interviewId, @RequestParam String name) throws FileNotFoundException {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        File file = new File(assessmentsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/" + name);
        return downloadAttachment(file);
    }

    private ResponseEntity<Resource> downloadAttachment(File file) throws FileNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "attachment; filename=" + file.getName());

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    @RequestMapping(value = "/recommendationletter/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public RecommendationLetter uploadRecommendationLetter(@Valid RecommendationLetter letter, @RequestParam(required = false) Long interviewId) {
        PowerInterview interview = powerInterviewService.uploadRecommendationLetter(letter, interviewId);
        return interview.getRecommendationLetters().stream().max(Comparator.comparing(RecommendationLetter::getId)).orElse(new RecommendationLetter());
    }


    @RequestMapping(value = "/recommendationletter/update", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void updateRecommendationLetter(@Valid RecommendationLetter letter, @RequestParam(required = false) Long interviewId) {
        powerInterviewService.updateRecommendationLetter(letter, interviewId);
    }

    @RequestMapping(value = "/recommendationletter/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadRecommendationLetter(@RequestParam Long interviewId, @RequestParam String name) throws FileNotFoundException {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        File file = new File(lettersDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/" + name);
        return downloadAttachment(file);
    }

    @RequestMapping(value = "/specialfactors/add", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Transactional
    public SpecialFactor addSpecialFactor(@Valid SpecialFactor specialFactor, @RequestParam Long interviewId) {
        PowerInterview interview = powerInterviewService.uploadSpecialFactor(specialFactor, interviewId);
        return interview.getSpecialFactors().stream().max(Comparator.comparing(SpecialFactor::getId)).orElse(new SpecialFactor());
    }

    @RequestMapping(value = "/specialfactors/update", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Transactional
    public void updateSpecialFactor(@Valid SpecialFactor specialFactor, @RequestParam Long interviewId) {
        powerInterviewService.updateSpecialFactors(specialFactor, interviewId);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void UploadFile(@RequestParam("files") MultipartFile file) {
        System.out.println(file.getOriginalFilename());
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public PowerInterview createPowerInterview(@RequestParam Long candidateId, @RequestParam Long jobProfileId) {
        JobProfile jobProfile = jobProfileService.findById(jobProfileId);
        PowerInterview powerInterview = new PowerInterview();
        powerInterview.setName(jobProfile.getTitle());
        powerInterview.setCandidateId(candidateId);
        powerInterview.setJobProfileID(jobProfileId);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/createonly", method = RequestMethod.POST)
    public PowerInterview createPowerInterview(@Valid @RequestBody PowerInterview powerInterviewDTO) {
        JobProfile jobProfile = new JobProfile();
        jobProfile.setStatus(false);
        jobProfile.setFake(true);
        jobProfile.setTitle(powerInterviewDTO.getName() + "(for PI)");
        Candidate candidate = candidateService.findById(powerInterviewDTO.getCandidateId());
        Company company = candidate.getCompany();
        CompanyDepartment companyDepartment = new CompanyDepartment();
        companyDepartment.setCompany(company);
        companyDepartment.setName("Default");
        jobProfile.setDepartment(company.getDepartments().stream().findFirst().orElse(companyDepartment));

        jobProfile = jobProfileService.saveJobProfile(jobProfile);
        PowerInterview powerInterview = new PowerInterview();
        powerInterview.setName(jobProfile.getTitle());
        powerInterview.setCandidateId(powerInterviewDTO.getCandidateId());
        powerInterview.setJobProfileID(jobProfile.getId());
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public PowerInterview update(@Valid @RequestBody PowerInterview powerInterview) {
        return powerInterviewService.update(powerInterview);
    }

    @RequestMapping(value = "/address/add", method = RequestMethod.POST)
    @Transactional
    public JobAddressDetails addJobAddressDetails(@Valid JobAddressDetails addressDetails, @RequestParam Long interviewId) {

        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getAddressDetails().add(addressDetails);
        PowerInterview interview = powerInterviewService.save(powerInterview);

        JobAddressDetails jobAddressDetails = interview.getAddressDetails().stream().max(Comparator.comparing(JobAddressDetails::getId)).orElse(new JobAddressDetails());

        Candidate candidate = candidateService.findById(powerInterview.getCandidateId());

        String originAddress = candidate.getStreet() + " " + candidate.getBuildingNumber() + ", " + candidate.getCity();
        String targetAddress = jobAddressDetails.getStreetName() + " " + jobAddressDetails.getBuildingNumber() + ", " + jobAddressDetails.getCity();

        jobAddressDetails.setWays(new ArrayList<>());

        if (addressDetails.getToWork() == null) {
            List<TimeOnTheWay> timeDistances = mapsService.calculateTripData(originAddress, targetAddress, CandidateUtils.getInstance().findNextMondayAtTime(jobAddressDetails.getStartTime()), "job");
            for (TimeOnTheWay timeDistance : timeDistances) {
                timeDistance.setAddressId(jobAddressDetails.getId());
                jobAddressDetails.setToWork(timeDistance.getDurationValue() / 60);
                jobAddressDetails.getWays().add(timeDistance);
            }
        }

        if (addressDetails.getToHome() == null) {
            List<TimeOnTheWay> timeDistances = mapsService.calculateTripData(targetAddress, originAddress, CandidateUtils.getInstance().findNextMondayAtTime(jobAddressDetails.getEndTime()), "home");
            for (TimeOnTheWay timeDistance : timeDistances) {
                timeDistance.setAddressId(jobAddressDetails.getId());
                jobAddressDetails.setToHome(timeDistance.getDurationValue() / 60);
                jobAddressDetails.getWays().add(timeDistance);
            }
        }

        return addressDetailsRepository.save(jobAddressDetails);
    }

    @RequestMapping(value = "/address/update", method = RequestMethod.POST)
    @Transactional
    public JobAddressDetails updateJobAddressDetails(HttpServletRequest request, @Valid JobAddressDetails addressDetails) {

        Integer interviewId = Integer.parseInt(request.getParameter("interviewId"));
        PowerInterview powerInterview = powerInterviewService.findById(interviewId.longValue());

        JobAddressDetails existed = addressDetailsRepository.findOne(addressDetails.getId());

        Candidate candidate = candidateService.findById(powerInterview.getCandidateId());
        String originAddress = candidate.getStreet() + " " + candidate.getBuildingNumber() + ", " + candidate.getCity();

        String targetAddress = addressDetails.getStreetName() + " " + addressDetails.getBuildingNumber() + ", " + addressDetails.getCity();

        if ((existed.getToWork() == null || existed.getToWork() == 0 || existed.getToWork().longValue() == addressDetails.getToWork().longValue()) && !existed.equals(addressDetails)) {
            List<TimeOnTheWay> timeDistances = mapsService.calculateTripData(originAddress, targetAddress, CandidateUtils.getInstance().findNextMondayAtTime(addressDetails.getStartTime()), "job");
            for (TimeOnTheWay timeDistance : timeDistances) {
                addressDetails.setToWork(timeDistance.getDurationValue() / 60);
            }
        }

        if ((existed.getToHome() == null || existed.getToHome() == 0 || existed.getToHome().longValue() == addressDetails.getToHome().longValue()) && !existed.equals(addressDetails)) {
            List<TimeOnTheWay> timeDistances = mapsService.calculateTripData(targetAddress, originAddress, CandidateUtils.getInstance().findNextMondayAtTime(addressDetails.getEndTime()), "home");
            for (TimeOnTheWay timeDistance : timeDistances) {
                addressDetails.setToHome(timeDistance.getDurationValue() / 60);
            }
        }
        addressDetailsRepository.save(addressDetails);
        powerInterviewService.save(powerInterview);
        return addressDetails;
    }

    @RequestMapping(value = "/culture/add", method = RequestMethod.POST)
    @Transactional
    public CompanyCulture addCompanyCulture(@Valid CompanyCulture companyCulture, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getCompanyCulture().add(companyCulture);
        PowerInterview interview = powerInterviewService.save(powerInterview);
        return interview.getCompanyCulture().stream().max(Comparator.comparing(CompanyCulture::getId)).orElse(new CompanyCulture());
    }

    @RequestMapping(value = "/culture/update", method = RequestMethod.POST)
    @Transactional
    public void updateCompanyCulture(@Valid CompanyCulture companyCulture) {
        powerInterviewService.updateCompanyCulture(companyCulture);
    }

    @RequestMapping(value = "/references/add", method = RequestMethod.POST)
    @Transactional
    public References addReferences(@Valid References references, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getReferences().add(references);
        PowerInterview interview = powerInterviewService.save(powerInterview);
        return interview.getReferences().stream().max(Comparator.comparing(References::getId)).orElse(new References());
    }

    @RequestMapping(value = "/references/update", method = RequestMethod.POST)
    @Transactional
    public void addReferences(@Valid References references) {
        powerInterviewService.updateReferences(references);
    }

    @RequestMapping(value = "/achievements/add", method = RequestMethod.POST)
    @Transactional
    public Achievements addAchievements(@Valid Achievements achievements, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getAchievements().add(achievements);
        PowerInterview interview = powerInterviewService.save(powerInterview);
        return interview.getAchievements().stream().max(Comparator.comparing(Achievements::getId)).orElse(new Achievements());
    }

    @RequestMapping(value = "/achievements/update", method = RequestMethod.POST)
    @Transactional
    public void addAchievements(@Valid Achievements achievements) {
        powerInterviewService.updateAchievements(achievements);
    }

    @ApiOperation(value = "Add career history!",
            notes = "date in following format: yyy-MM-dd")
    @RequestMapping(value = "/jobstability/add", method = RequestMethod.POST)
    @Transactional
    public JobStability addReferences(@Valid JobStability jobStability, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getJobStability().add(jobStability);
        PowerInterview interview = powerInterviewService.save(powerInterview);
        return interview.getJobStability().stream().max(Comparator.comparing(JobStability::getId)).orElse(new JobStability());
    }

    @ApiOperation(value = "Add career history!",
            notes = "date in following format: yyy-MM-dd")
    @RequestMapping(value = "/jobstability/update", method = RequestMethod.POST)
    @Transactional
    public void addReferences(@Valid JobStability jobStability) {
        powerInterviewService.updateJobStability(jobStability);
    }

    @RequestMapping(value = "/reasonforleaving/add", method = RequestMethod.POST)
    @Transactional
    public LeavingReason addReasonForLeaving(@Valid LeavingReason leavingReason, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getLeavingReasons().add(leavingReason);
        PowerInterview interview = powerInterviewService.save(powerInterview);
        return interview.getLeavingReasons().stream().max(Comparator.comparing(LeavingReason::getId)).orElse(new LeavingReason());
    }

    @RequestMapping(value = "/reasonforleaving/update", method = RequestMethod.POST)
    @Transactional
    public void updateReasonForLeaving(@Valid LeavingReason leavingReason) {
        powerInterviewService.updateLeavingReason(leavingReason);
    }

    @RequestMapping(value = "/compensation/add", method = RequestMethod.POST)
    @Transactional
    public Compensation addCompensation(@Valid Compensation compensation, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getCompensations().add(compensation);
        PowerInterview interview = powerInterviewService.save(powerInterview);
        return interview.getCompensations().stream().max(Comparator.comparing(Compensation::getId)).orElse(new Compensation());
    }

    @RequestMapping(value = "/compensation/update", method = RequestMethod.POST)
    @Transactional
    public void updateCompensation(@Valid Compensation compensation) {
        powerInterviewService.updateCompensation(compensation);
    }

    @RequestMapping(value = "/measuring/add", method = RequestMethod.POST)
    @Transactional
    public Measuring addMeasuring(@Valid Measuring measuring, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.getMeasuring().add(measuring);
        PowerInterview interview = powerInterviewService.save(powerInterview);
        return interview.getMeasuring().stream().max(Comparator.comparing(Measuring::getId)).orElse(new Measuring());
    }

    @RequestMapping(value = "/measuring/update", method = RequestMethod.POST)
    @Transactional
    public void updateMeasuring(@Valid Measuring measuring) {
        powerInterviewService.updateMeasuring(measuring);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public PowerInterview getPowerInterview(@RequestParam Long interviewId) {
        return powerInterviewService.findById(interviewId);
    }

    @RequestMapping(value = "/get/{candidateId}", method = RequestMethod.GET)
    public List<PowerInterview> getPowerInterviewsForCandidate(@PathVariable Long candidateId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        return powerInterviewService.findByCandidateId(candidateId, loggedUser);
    }

    @RequestMapping(value = "/factors/drivingrecords", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateDrivingRecords(@RequestParam Boolean factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setHasCleanDrivingRecords(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/factors/drugtest", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateDrugTest(@RequestParam Boolean factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setCanPassDrugTest(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/factors/creditcheck", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateCreditCheck(@RequestParam Boolean factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setCanPassCreditCheck(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/factors/backgroundcheck", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateBackgroundCheck(@RequestParam Boolean factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setCanPassBackgroundCheck(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/factors/physicalcheck", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updatePhysicalCheck(@RequestParam Boolean factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setCanPassPhysical(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/ranking/compensation", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateCompensationRanking(@RequestParam Integer factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setCompensationsScale(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/ranking/jobstability", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateJobStabilityRanking(@RequestParam Integer factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setJobStabilityScale(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/ranking/references", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateReferencesRanking(@RequestParam Integer factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setReferencesScale(factorValue);
        return powerInterviewService.save(powerInterview);
    }

    @RequestMapping(value = "/ranking/measuring", method = RequestMethod.POST)
    @Transactional
    public PowerInterview updateMeasuringRanking(@RequestParam Integer factorValue, @RequestParam Long interviewId) {
        PowerInterview powerInterview = powerInterviewService.findById(interviewId);
        powerInterview.setMeasuringScale(factorValue);
        return powerInterviewService.save(powerInterview);
    }
}