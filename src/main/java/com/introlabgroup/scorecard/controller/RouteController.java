package com.introlabgroup.scorecard.controller;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.User;
import com.introlabgroup.scorecard.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author vitalii.
 */
@Controller
public class RouteController {


    private final UserService userService;

    @Autowired
    public RouteController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/admin/companies")
    public ModelAndView companies() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("user", loggedUser);
        modelAndView.addObject("newCompany", new Company());
        modelAndView.addObject("newUser", new User());
        modelAndView.addObject("userName", loggedUser.getName() + " " + loggedUser.getLastName());
        modelAndView.setViewName("admin/companies");
        return modelAndView;
    }
    @GetMapping("/admin/super")
    public ModelAndView superAdmins() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("user", loggedUser);
        modelAndView.addObject("newCompany", new Company());
        modelAndView.addObject("newUser", new User());
        modelAndView.addObject("userName", loggedUser.getName() + " " + loggedUser.getLastName());
        modelAndView.setViewName("admin/super");
        return modelAndView;
    }

}
