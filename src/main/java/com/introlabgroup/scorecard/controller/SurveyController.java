package com.introlabgroup.scorecard.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabgroup.scorecard.error.CreateScoreCardNotAllowedException;
import com.introlabgroup.scorecard.model.*;
import com.introlabgroup.scorecard.model.dto.DataJsonObject;
import com.introlabgroup.scorecard.model.dto.JobProfileDTO;
import com.introlabgroup.scorecard.model.dto.JobProfileStatusObject;
import com.introlabgroup.scorecard.model.dto.QuestionJsonObject;
import com.introlabgroup.scorecard.model.onet.MeasuringType;
import com.introlabgroup.scorecard.model.onet.Occupation;
import com.introlabgroup.scorecard.model.onet.PriorityRanking;
import com.introlabgroup.scorecard.model.payment.StripeSubscription;
import com.introlabgroup.scorecard.repository.onet.OccupationRepository;
import com.introlabgroup.scorecard.service.*;
import com.introlabgroup.scorecard.service.sovren.ResumeParser;
import com.introlabgroup.scorecard.utils.CandidateUtils;
import com.introlabgroup.scorecard.utils.CommonUtils;
import com.introlabgroup.scorecard.utils.FileUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.introlabgroup.scorecard.utils.UserUtils.isManager;

/**
 * @author vitalii.
 */
@RestController
@RequestMapping("/api/survey")
public class SurveyController {

    private final JobProfileService jobProfileService;

    private final SurveyService surveyService;

    private final CandidateService candidateService;

    private final UserService userService;

    private final DepartmentService departmentService;

    private final CompanyService companyService;

    private final OccupationRepository occupationRepository;

    private final StripeService stripeService;

    @Value("${attachments.jobs.directory}")
    private String jobsDirectory;

    private static final Logger logger = LoggerFactory.getLogger(SurveyController.class);

    private final ResumeParser resumeParser;

    @Autowired
    public SurveyController(JobProfileService jobProfileService, SurveyService surveyService, CandidateService candidateService, UserService userService, DepartmentService departmentService, CompanyService companyService, OccupationRepository occupationRepository, StripeService stripeService, ResumeParser resumeParser) {
        this.jobProfileService = jobProfileService;
        this.surveyService = surveyService;
        this.candidateService = candidateService;
        this.userService = userService;
        this.departmentService = departmentService;
        this.companyService = companyService;
        this.occupationRepository = occupationRepository;
        this.stripeService = stripeService;
        this.resumeParser = resumeParser;
    }

    @RequestMapping(value = "/jobs/upload/{departmentId}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public JobProfileDTO uploadWorkSample(@Valid UploadedFile file, @PathVariable Long departmentId) throws IOException {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());

        String directory = jobsDirectory + loggedUser.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, file.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }

        final JobParsedModel jobParsedModel = resumeParser.parseJobDescription(loggedUser.getId() + "/" + file.getFile().getOriginalFilename());

        final String occupationId = CandidateUtils.getInstance().generateCustomId(jobParsedModel.getTitle());

        Occupation occupation = occupationRepository.findOne(occupationId);


        if (occupation == null) {
            occupation = new Occupation();
            occupation.setOnetsoc_code(occupationId);
            occupation.setTitle(jobParsedModel.getTitle());
            occupation.setDescription("");
            initWithEmptyQualities(occupation);
            for (String skill : jobParsedModel.getSkills()) {
                surveyService.assignQuality(occupation, skill);
            }
        }

        final JobProfileDTO jobProfileDTO = new JobProfileDTO();
        jobProfileDTO.setDepartment(departmentService.findById(departmentId, true));
        jobProfileDTO.setMeasuringType(MeasuringType.WEIGHT);
        jobProfileDTO.setName(occupation.getTitle());
        jobProfileDTO.setOccupation(occupation);
        return jobProfileDTO;

    }

    private void initWithEmptyQualities(Occupation occupation) {
        occupation.setAbilities(new ArrayList<>());
        occupation.setInterests(new ArrayList<>());
        occupation.setSkills(new ArrayList<>());
        occupation.setWorkActivities(new ArrayList<>());
        occupation.setWorkStyles(new ArrayList<>());
        occupation.setWorkValues(new ArrayList<>());
        occupation.setKnowledge(new ArrayList<>());
        occupation.setCustomQualities(new ArrayList<>());
    }

    @RequestMapping(value = "/config/finish", method = RequestMethod.POST)
    @ResponseBody
    public String testEdit(@RequestParam Long profileId) {
        JobProfile jobProfile = jobProfileService.findById(profileId);
        jobProfile.setFullyConfigured(true);
        jobProfileService.saveJobProfile(jobProfile);
        return "OK:200";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestParam Long profileId) {
        jobProfileService.delete(profileId);
        return "OK:200";
    }

    @RequestMapping(value = "/update/weight", method = RequestMethod.POST)
    @ResponseBody
    public String updateWeight(@RequestParam long profileId,
                               @RequestParam String qualityId,
                               @RequestParam double weight) throws IOException {
        JobProfile jobProfile = jobProfileService.findById(profileId);
        ObjectMapper mapper = new ObjectMapper();
        Occupation occupation = mapper.readValue(jobProfile.getOccupationJson(), Occupation.class);
        surveyService.updateQualityWeight(occupation, qualityId, weight);
        jobProfile.setOccupationJson(mapper.writeValueAsString(occupation));
        jobProfileService.saveJobProfile(jobProfile);
        return "OK";
    }


    @RequestMapping(value = "/update/ranking", method = RequestMethod.POST)
    @ResponseBody
    public String updatePriority(@RequestParam long profileId,
                                 @RequestParam String qualityId,
                                 @RequestParam PriorityRanking priorityRanking) throws IOException {
        JobProfile jobProfile = jobProfileService.findById(profileId);
        ObjectMapper mapper = new ObjectMapper();
        Occupation occupation = mapper.readValue(jobProfile.getOccupationJson(), Occupation.class);
        surveyService.updatePriorityRanking(occupation, qualityId, priorityRanking);
        jobProfile.setOccupationJson(mapper.writeValueAsString(occupation));
        jobProfileService.saveJobProfile(jobProfile);
        return "OK";
    }


    @RequestMapping(value = "/update/requirements", method = RequestMethod.POST)
    @ResponseBody
    public String updateRequirements(@RequestParam long profileId,
                                     @RequestParam String qualityId) throws IOException {
        JobProfile jobProfile = jobProfileService.findById(profileId);
        ObjectMapper mapper = new ObjectMapper();
        Occupation occupation = mapper.readValue(jobProfile.getOccupationJson(), Occupation.class);
        surveyService.updateRequirements(occupation, qualityId);
        jobProfile.setOccupationJson(mapper.writeValueAsString(occupation));
        jobProfileService.saveJobProfile(jobProfile);
        return "OK";
    }

    @RequestMapping(value = "/questions/addown", method = RequestMethod.POST)
    @ResponseBody
    public String addQuestion(@RequestParam long profileId,
                              @RequestParam String qualityId,
                              @RequestParam String question) throws IOException {
        JobProfile jobProfile = jobProfileService.findById(profileId);
        ObjectMapper mapper = new ObjectMapper();
        Occupation occupation = mapper.readValue(jobProfile.getOccupationJson(), Occupation.class);
        surveyService.addQualityQuestion(occupation, qualityId, new LeadingQuestion(question));
        jobProfile.setOccupationJson(mapper.writeValueAsString(occupation));
        jobProfileService.saveJobProfile(jobProfile);
        return "OK";
    }

    @RequestMapping(value = "/questions/list/{profileId}", method = RequestMethod.GET)
    @ResponseBody
    public QuestionJsonObject leadingQuestions(@PathVariable long profileId) throws IOException {
        QuestionJsonObject questionJsonObject = new QuestionJsonObject();
        if (profileId == 0) {
            return questionJsonObject;
        }
        JobProfile jobProfile = jobProfileService.findById(profileId);
        ObjectMapper mapper = new ObjectMapper();
        Occupation occupation = mapper.readValue(jobProfile.getOccupationJson(), Occupation.class);
        questionJsonObject.setData(surveyService.getLeadingQuestions(occupation));
        questionJsonObject.setRecordsTotal(questionJsonObject.getData().size());
        questionJsonObject.setRecordsTotal(questionJsonObject.getData().size() > 10 ? 10 : questionJsonObject.getData().size());
        return questionJsonObject;
    }

    @RequestMapping(value = "/finish/{profileId}/{candidateId}", method = RequestMethod.POST)
    @ResponseBody
    public String test(@PathVariable long profileId,
                       @PathVariable long candidateId,
                       @RequestBody String json) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Assessment assessment = new Assessment();
        assessment.setInterviewerName(loggedUser.getFullName());
        assessment.setCandidate(candidateService.findById(candidateId));
        JobProfile jobProfile = jobProfileService.findById(profileId);
        assessment.setJobProfile(jobProfile);
        String answers = URLDecoder.decode(json, "UTF-8");
        assessment.setBody(answers);
        ObjectMapper objectMapper = new ObjectMapper();
        Occupation occupation = objectMapper.readValue(jobProfile.getOccupationJson(), Occupation.class);
        JsonElement parse = new JsonParser().parse(assessment.getBody());
        JsonObject answersJson = parse.getAsJsonObject();
        assessment.setFinished(CandidateUtils.getInstance().isFinished(occupation, answersJson));
        assessment.setOccupationJson(jobProfile.getOccupationJson());
        assessment.setMeasuringType(jobProfile.getMeasuringType());
        assessment.setCreated(new Date());
        assessment.setName(jobProfile.getTitle());
        surveyService.saveAssessment(assessment);
        return "";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public JobProfileDTO create(@Valid @RequestBody JobProfileDTO jobProfileDTO) throws IOException {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());

        Company originCompany = loggedUser.getCompany();
        if (!originCompany.isAllowScorecards()) {
            throw new CreateScoreCardNotAllowedException("Not allowed to create scorecards for this client");
        }

        if (originCompany.isTrial() && jobProfileService.findAllActiveByDepartments(departmentService.findAllByCompany(originCompany, true)).size() > 1) {
            originCompany.setAllowScorecards(false);
            originCompany.setReason("Scorecards limit for trial reached");
            companyService.saveCompany(originCompany);
            throw new CreateScoreCardNotAllowedException("Not allowed to create scorecards for this client. Limit reached");
        }

        JobProfile jobProfile = new JobProfile();
        jobProfile.setTitle(jobProfileDTO.getName());
        jobProfile.setMeasuringType(jobProfileDTO.getMeasuringType());
        jobProfile.setDepartment(jobProfileDTO.getDepartment());
        jobProfile.setStatus(true);
        jobProfile.setDescription(jobProfileDTO.getOccupation().getDescription());
        ObjectMapper objectMapper = new ObjectMapper();
        jobProfile.setOccupationJson(objectMapper.writeValueAsString(jobProfileDTO.getOccupation()));
        JobProfile jobProfileSaved = jobProfileService.saveJobProfile(jobProfile);
        jobProfileDTO.setProfileId(jobProfileSaved.getId());
        Company company = companyService.findById(originCompany.getId());
        if (company.isPaid() && !company.isTrial()) {
            stripeService.getStripeDao().decreaseScorecardCount(company.getId());
            logger.info("Scorecards count decreased");
            StripeSubscription subscription = stripeService.getStripeDao().getStripeSubscriptionByCompanyId(company.getId());
            if (subscription.getScorecardsLeft() < 1) {
                company.setAllowScorecards(false);
                company.setReason("Scorecards limit reached");
                companyService.saveCompany(company);
            }
        }
        if (company.isTrial() && jobProfileService.findAllActiveByDepartments(departmentService.findAllByCompany(originCompany, true)).size() > 1) {
            company.setAllowScorecards(false);
            company.setReason("Scorecards limit for trial reached");
            companyService.saveCompany(company);
        }
        return jobProfileDTO;
    }

    @RequestMapping(value = "/clone", method = RequestMethod.POST)
    @ResponseBody
    public void clone(@RequestParam long profileId, @RequestParam long departmentId) {

        JobProfile jobProfile = jobProfileService.findById(profileId);
        jobProfile.setId(null);
        jobProfile.setDepartment(departmentService.findById(departmentId, false));
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public JobProfileDTO update(@Valid @RequestBody JobProfileDTO jobProfileDTO) throws IOException {

        JobProfile jobProfile = jobProfileService.findById(jobProfileDTO.getProfileId());
        jobProfile.setTitle(jobProfileDTO.getName());
        jobProfile.setMeasuringType(jobProfileDTO.getMeasuringType());
        jobProfile.setDepartment(jobProfileDTO.getDepartment());
        jobProfile.setDescription(jobProfileDTO.getOccupation().getDescription());
        ObjectMapper objectMapper = new ObjectMapper();
        jobProfile.setOccupationJson(objectMapper.writeValueAsString(jobProfileDTO.getOccupation()));
        JobProfile jobProfileSaved = jobProfileService.saveJobProfile(jobProfile);
        jobProfileDTO.setProfileId(jobProfileSaved.getId());
        return jobProfileDTO;
    }


    @RequestMapping(value = "/updatestatus", method = RequestMethod.POST)
    @ResponseBody
    public void updateActiveStatus(@Valid @RequestBody JobProfileStatusObject jobProfileDTO) {

        JobProfile jobProfile = jobProfileService.findById(jobProfileDTO.getScorecardId());
        jobProfile.setStatus(jobProfileDTO.isStatusActive());
        jobProfileService.saveJobProfile(jobProfile);
    }

    @ApiOperation(value = "Get Positions for Department. Deprecated!",
            notes = "Deprecated")
    @RequestMapping(value = "/positions/{departmentId}", method = RequestMethod.GET)
    @ResponseBody
    public Collection<JobProfile> getPositions(@PathVariable long departmentId,
                                               @RequestParam(defaultValue = "0", name = "start") Integer start,
                                               @RequestParam(defaultValue = "20", name = "length") Integer length,
                                               @ApiParam(value = "Search keyword") @RequestParam(defaultValue = "", name = "search") String keyword,
                                               @ApiParam(value = "Sort column name like 'title' or 'department.name'") @RequestParam(defaultValue = "title", name = "sort") String column,
                                               @ApiParam(value = "Sort direction: asc/desc") @RequestParam(defaultValue = "asc", name = "order") String direction) {

        Page<JobProfile> profiles = jobProfileService.findAllActiveByDepartment(departmentService.findById(departmentId, true), keyword, start / length, length, direction, column);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(profiles.getTotalElements());
        personJsonObject.setRecordsFiltered(profiles.getTotalElements());
        List<JobProfile> content = profiles.getContent();
        profiles.forEach(candidate -> candidate.setDepartment(null));
        personJsonObject.setData(content);
        return profiles.getContent();
    }

    @ApiOperation(value = "Get Positions for Department")
    @RequestMapping(value = "/positions/department/{departmentId}", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject getPositionsForDepartment(@PathVariable long departmentId,
                                                    @RequestParam(defaultValue = "0", name = "page") Integer page,
                                                    @RequestParam(defaultValue = "20", name = "length") Integer length,
                                                    @ApiParam(value = "Search keyword") @RequestParam(defaultValue = "", name = "search") String keyword,
                                                    @ApiParam(value = "Sort column name like 'title' or 'department.name'") @RequestParam(defaultValue = "title", name = "sort") String column,
                                                    @ApiParam(value = "Sort direction: asc/desc") @RequestParam(defaultValue = "asc", name = "order") String direction) {

        Page<JobProfile> profiles = jobProfileService.findAllActiveByDepartment(departmentService.findById(departmentId, true), keyword, page, length, direction, column);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(profiles.getTotalElements());
        personJsonObject.setRecordsFiltered(profiles.getTotalElements());
        personJsonObject.setData(profiles.getContent().stream().map(CommonUtils::jobProfileToViewDTO).collect(Collectors.toList()));
        return personJsonObject;
    }

    @RequestMapping(value = "/positions/company/{companyId}", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject getActivePositionsForCompany(@PathVariable long companyId,
                                                       @RequestParam(defaultValue = "0", name = "page") Integer page,
                                                       @RequestParam(defaultValue = "20", name = "length") Integer length,
                                                       @ApiParam(value = "Search keyword") @RequestParam(defaultValue = "", name = "search") String keyword,
                                                       @ApiParam(value = "Sort column name like 'title' or 'department.name'") @RequestParam(defaultValue = "title", name = "sort") String column,
                                                       @ApiParam(value = "Sort direction: asc/desc") @RequestParam(defaultValue = "asc", name = "order") String direction) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Collection<CompanyDepartment> allByCompany;
        Company company = companyService.findById(companyId);
        if (isManager(loggedUser)) {

            allByCompany = loggedUser.getDepartments();

        } else {
            allByCompany = departmentService.findAllByCompany(company, true);

        }

        if (allByCompany.size() == 0) {
            return new DataJsonObject();
        }
        Page<JobProfile> candidates = jobProfileService.findAllActiveByDepartments(allByCompany, keyword, page, length, direction, column);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(candidates.getTotalElements());
        personJsonObject.setRecordsFiltered(candidates.getTotalElements());
        personJsonObject.setData(candidates.getContent().stream().map(CommonUtils::jobProfileToViewDTO).collect(Collectors.toList()));
        return personJsonObject;
    }

    @RequestMapping(value = "/positions/company/{companyId}/all", method = RequestMethod.GET)
    @ResponseBody
    public DataJsonObject getAllPositionsForCompany(@PathVariable long companyId) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmail(auth.getName());
        Collection<CompanyDepartment> allByCompany;
        Company company = companyService.findById(companyId);
        if (isManager(loggedUser)) {

            allByCompany = loggedUser.getDepartments();

        } else {
            allByCompany = departmentService.findAllByCompany(company, true);

        }

        if (allByCompany.size() == 0) {
            return new DataJsonObject();
        }
        List<JobProfile> candidates = jobProfileService.findAllByDepartments(allByCompany);
        DataJsonObject personJsonObject = new DataJsonObject();
        personJsonObject.setRecordsTotal(candidates.size());
        personJsonObject.setRecordsFiltered(candidates.size());
        personJsonObject.setData(candidates.stream().map(CommonUtils::jobProfileToViewDTO).collect(Collectors.toList()));
        return personJsonObject;
    }
}
