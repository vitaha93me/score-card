package com.introlabgroup.scorecard.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PAYMENT_REQUIRED)
public class CreateScoreCardNotAllowedException extends RuntimeException {
    public CreateScoreCardNotAllowedException(String exception) {
        super(exception);
    }
}