package com.introlabgroup.scorecard.error;

import com.stripe.exception.StripeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@RestControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler {


    private static final Logger logger = LoggerFactory.getLogger(CustomizedResponseEntityExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR.toString(), new Date(), ex.getMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IncorrectLoginOrPasswordException.class)
    public final ResponseEntity<ErrorDetails> handleIncorrectLoginOrPasswordException(IncorrectLoginOrPasswordException ex, WebRequest request) {
        String message = ex.getMessage();
        logger.error(message, ex);
        String statusCode = HttpStatus.UNAUTHORIZED.toString();
        Date timestamp = new Date();
        String description = request.getDescription(false);
        String exceptionClassSimpleName = ex.getClass().getSimpleName();
        ErrorDetails errorDetails = new ErrorDetails(statusCode, timestamp, message,
                description, exceptionClassSimpleName);
        return new ResponseEntity<>(errorDetails, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(StripeException.class)
    public final ResponseEntity<ErrorDetails> handleStripeException(StripeException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR.toString(), new Date(), ex.getMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NullPointerException.class)
    public final ResponseEntity<ErrorDetails> handleNullPointerException(NullPointerException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR.toString(), new Date(), "NullPointerException at " + ex.getLocalizedMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public final ResponseEntity<ErrorDetails> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR.toString(), new Date(), "Record not found in database: " + ex.getLocalizedMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleUserNotFoundException(RecordNotFoundException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_FOUND.toString(), new Date(), ex.getLocalizedMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleUserNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_FOUND.toString(), new Date(), ex.getLocalizedMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CreateScoreCardNotAllowedException.class)
    public final ResponseEntity<ErrorDetails> handleUserNotFoundException(CreateScoreCardNotAllowedException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.PAYMENT_REQUIRED.toString(), new Date(), ex.getLocalizedMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.PAYMENT_REQUIRED);
    }

    @ExceptionHandler(UnExpectedParsingError.class)
    public final ResponseEntity<ErrorDetails> unExpectedParsingException(UnExpectedParsingError ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR.toString(), new Date(), ex.getLocalizedMessage(),
                request.getDescription(true), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
