package com.introlabgroup.scorecard.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class UnExpectedParsingError extends RuntimeException {


    public UnExpectedParsingError(String message, Exception e) {
        super(message, e);

    }
}