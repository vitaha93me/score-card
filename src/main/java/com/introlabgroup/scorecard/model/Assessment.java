package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.introlabgroup.scorecard.model.onet.MeasuringType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author vitalii.
 */
@Entity
@Getter
@Setter
public class Assessment implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private double overallScore;

    private boolean finished;

//    @OneToOne
//    @JoinColumn(name = "user_id")
//    private User interviewer;

    private String interviewerName;

    @Lob
    private String body;

    @Lob
    private String occupationJson;

    @Enumerated(EnumType.STRING)
    @Column(length = 100)
    private MeasuringType measuringType;

    @ManyToOne
    @JoinColumn(name = "fk_job_profile")
    private JobProfile jobProfile;

    @ManyToOne
    @JoinColumn(name = "fk_candidate")
    @JsonBackReference
    private Candidate candidate;

    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
}
