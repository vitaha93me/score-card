package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author vitalii.
 */
@Entity
@Table(name = "candidate")
@Getter
@Setter
public class Candidate implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "candidate_id")
    private Long id;

    @Column(name = "cats_id", unique = true)
    private Long catsId;

    @Column(name = "first_name")
    private String name;
    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Assessment> assessments = new HashSet<>();

    @OneToMany(mappedBy = "candidate", fetch = FetchType.EAGER)
    @JsonManagedReference
    private Set<CareerHistory> careerHistories = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_company")
    private Company company;

    @OneToMany(mappedBy = "candidate",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<CandidateReference> candidateReferences = new ArrayList<>();

    private String email;
    private String phone;
    private String city;
    private String street;
    private String buildingNumber;
    private String state;
    private String zip;
    private String supervisor;
    private String qualificationSummary;
    private String sovrenDescription;
    private boolean drugTest;
    private boolean drivingRecords;
    private boolean creditCheck;
    private boolean backgroundCheck;
    private boolean physical;
    private String resumeFileName;

    @Transient
    private MultipartFile resumeFile;

}
