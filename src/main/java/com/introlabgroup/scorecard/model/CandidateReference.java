package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity()
public class CandidateReference {

    @Id
    @GeneratedValue
    private Integer id;

    private String personName;
    private String company;
    private String positionTitle;
    private String phoneNumber;
    private String email;

    @ManyToOne
    @JoinColumn(name = "fk_candidate")
    @JsonBackReference
    private Candidate candidate;
}
