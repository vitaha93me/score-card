package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author vitalii.
 */
@Entity
@Setter
@Getter
@Table(name = "career_history")
public class CareerHistory {

    @Id
    @GeneratedValue
    private Long id;

    private String jobTitle;
    private String company;
    private Date startDate;
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "fk_candidate")
    @JsonBackReference
    private Candidate candidate;
}
