package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author vitalii.
 */
@Entity
@Table(name = "company")
@Getter
@Setter
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "company_id")
    private Long id;

    @Column(name = "qb_id")
    private Long qbId;

    @Column(name = "name")
    private String name;

    private boolean allowScorecards;

    private boolean allowUsage;

    private String reason;

    private boolean trial;

    private boolean paid;

    private boolean free;

    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;

    private Date endOfPeriod;

    @OneToMany(mappedBy = "company")
    @JsonManagedReference
    private Set<User> users = new HashSet<>();

    @OneToMany(mappedBy = "company")
    @JsonManagedReference
    private Set<CompanyDepartment> departments = new HashSet<>();

}
