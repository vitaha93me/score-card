package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * @author vitalii.
 */
@Entity
@Setter
@Getter
@Table(name = "department")
public class CompanyDepartment {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    private String city;

    private String state;

    private String zip;

    private String address;

    private String phone;

    private String email;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "fk_company")
    private Company company;

    @OneToMany(mappedBy = "department")
    @JsonManagedReference
    private List<JobProfile> openPositions;

}
