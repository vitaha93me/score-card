package com.introlabgroup.scorecard.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

import javax.persistence.Column;

@Getter
@Setter
public class CompanyRegistrationDTO {


    @NotEmpty(message = "*Please provide an email")
    private String email;

    @NotEmpty(message = "*Please provide your password")
    @Transient
    private String password;

    @NotEmpty(message = "*Please provide your name")
    private String firstName;
    @Column(name = "last_name")

    @NotEmpty(message = "*Please provide your last name")
    private String lastName;

    @NotEmpty(message = "*Please provide a company name")
    private String companyName;

}
