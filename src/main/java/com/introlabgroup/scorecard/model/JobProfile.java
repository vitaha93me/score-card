package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.introlabgroup.scorecard.model.onet.MeasuringType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author vitalii.
 */

@Entity
@Setter
@Getter
@Table(name = "job_profile")
public class JobProfile implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @Lob
    private String description;

    @Lob
    private String occupationJson;

    @Enumerated(EnumType.STRING)
    @Column(length = 100)
    private MeasuringType measuringType;

    private boolean fullyConfigured;

    @Column(name = "status", nullable = false, columnDefinition = "boolean default true")
    private boolean status;

    @Column(name = "fake", nullable = false)
    private boolean fake;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "fk_department")
    private CompanyDepartment department;

    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
}
