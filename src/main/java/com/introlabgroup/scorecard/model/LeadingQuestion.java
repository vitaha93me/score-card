package com.introlabgroup.scorecard.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.introlabgroup.scorecard.model.onet.OnetItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity
public class LeadingQuestion {

    @Id
    @GeneratedValue
    private Long id;

    @Lob
    private String question;

    @ManyToOne
    @JoinColumn(name = "fk_onet_item")
    @JsonBackReference
    private OnetItem onetItem;

    public LeadingQuestion() {
    }

    public LeadingQuestion(String question) {
        this.question = question;
    }
}
