package com.introlabgroup.scorecard.model;

/**
 * @author vitalii.
 */
public class MailTemplate {
    private String subject;
    private String body;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public MailTemplate(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public MailTemplate() {
    }
}
