package com.introlabgroup.scorecard.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author vitalii.
 */
public class States {

    private States() {
    }

    public static final List<String> list = Arrays.asList(
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin",
            "Wyoming"

    );

    public static final Map<String, String> map = new TreeMap<>();

    static {
        map.put("Alabama", "AL");
        map.put("Alaska", "AK");
        map.put("Arizona", "AZ");
        map.put("Arkansas", "AR");
        map.put("California", "CA");
        map.put("Colorado", "CO");
        map.put("Connecticut", "CT");
        map.put("Delaware", "DE");
        map.put("Florida", "FL");
        map.put("Georgia", "GA");
        map.put("Hawaii", "HI");
        map.put("Idaho", "ID");
        map.put("Illinois", "IL");
        map.put("Indiana", "IN");
        map.put("Iowa", "IA");
        map.put("Kansas", "KS");
        map.put("Kentucky", "KY");
        map.put("Louisiana", "LA");
        map.put("Maine", "ME");
        map.put("Maryland", "MD");
        map.put("Massachusetts", "MA");
        map.put("Michigan", "MI");
        map.put("Minnesota", "MN");
        map.put("Mississippi", "MS");
        map.put("Missouri", "MO");
        map.put("Montana", "MT");
        map.put("Nebraska", "NE");
        map.put("Nevada", "NV");
        map.put("New Hampshire", "NH");
        map.put("New Jersey", "NJ");
        map.put("New Mexico", "NM");
        map.put("New York", "NY");
        map.put("North Carolina", "NC");
        map.put("North Dakota", "ND");
        map.put("Ohio", "OH");
        map.put("Oklahoma", "OK");
        map.put("Oregon", "OR");
        map.put("Pennsylvania", "PA");
        map.put("Rhode Island", "RI");
        map.put("South Carolina", "SC");
        map.put("South Dakota", "SD");
        map.put("Tennessee", "TN");
        map.put("Texas", "TX");
        map.put("Utah", "UT");
        map.put("Vermont", "VT");
        map.put("Virginia", "VA");
        map.put("Washington", "WA");
        map.put("West Virginia", "WV");
        map.put("Wisconsin", "WI");
        map.put("Wyoming", "WY");

    }
}
