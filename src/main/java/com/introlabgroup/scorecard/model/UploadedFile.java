package com.introlabgroup.scorecard.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * @author vitalii.
 */
@Getter
@Setter
public class UploadedFile {

    @Transient
    private MultipartFile file;
}
