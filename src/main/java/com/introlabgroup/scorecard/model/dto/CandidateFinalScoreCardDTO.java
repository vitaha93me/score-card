package com.introlabgroup.scorecard.model.dto;

import com.introlabgroup.scorecard.model.CandidateReference;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

/**
 * @author vitalii.
 */

@Getter
@Setter
public class CandidateFinalScoreCardDTO {

    private Long id;

    private String name;

    private String lastName;

    private String email;
    private String phone;
    private String city;
    private String street;
    private String buildingNumber;
    private String state;
    private String zip;
    private String supervisor;
    private String resume;
    private boolean drugTest;
    private boolean drivingRecords;
    private boolean creditCheck;
    private boolean backgroundCheck;
    private boolean physical;

    private Map<String, Double> scoreMap = new TreeMap<>();

    private List<InnerCandidateAssessment> assessments;
    private List<CandidateReference> candidateReferences;

}
