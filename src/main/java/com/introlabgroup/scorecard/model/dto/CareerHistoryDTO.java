package com.introlabgroup.scorecard.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author vitalii.
 */
@Setter
@Getter
public class CareerHistoryDTO {
    private Long id;
    private String jobTitle;
    private String company;
    private String startDate;
    private String endDate;
}
