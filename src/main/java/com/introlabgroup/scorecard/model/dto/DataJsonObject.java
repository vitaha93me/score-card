package com.introlabgroup.scorecard.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author vitalii.
 */
@Getter
@Setter
public class DataJsonObject {

    private long recordsTotal;

    private long recordsFiltered;

    private List<?> data;
}