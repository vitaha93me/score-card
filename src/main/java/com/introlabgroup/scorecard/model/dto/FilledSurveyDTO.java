package com.introlabgroup.scorecard.model.dto;

import com.introlabgroup.scorecard.model.onet.Occupation;
import lombok.Getter;
import lombok.Setter;

/**
 * @author vitalii.
 */
@Getter
@Setter
public class FilledSurveyDTO {
    private Long id;
    private String name;
    private String interviewer;
    private String answers;
    private Occupation occupation;
    private Long jobProfileId;
    private double overallScore;
}
