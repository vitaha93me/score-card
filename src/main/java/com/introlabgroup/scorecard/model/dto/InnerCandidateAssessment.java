package com.introlabgroup.scorecard.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author vitalii.
 */
@Getter
@Setter
public class InnerCandidateAssessment {

    private Long jobProfileId;
    private List<FilledSurveyDTO> assessments;
}
