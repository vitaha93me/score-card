package com.introlabgroup.scorecard.model.dto;

import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.model.onet.MeasuringType;
import com.introlabgroup.scorecard.model.onet.Occupation;
import lombok.Getter;
import lombok.Setter;

/**
 * @author vitalii.
 */
@Setter
@Getter
public class JobProfileDTO {

    private String name;
    private Occupation occupation;
    private MeasuringType measuringType;
    private boolean fullyConfigured;
    private CompanyDepartment department;
    private long profileId;
}
