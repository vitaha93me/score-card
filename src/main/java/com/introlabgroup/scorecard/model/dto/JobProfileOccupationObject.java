package com.introlabgroup.scorecard.model.dto;

import com.introlabgroup.scorecard.model.onet.Occupation;
import lombok.Getter;
import lombok.Setter;

/**
 * @author vitalii.
 */
@Setter
@Getter
public class JobProfileOccupationObject {

    private JobProfileViewJsonObject jobProfile;
    private Occupation occupation;
}
