package com.introlabgroup.scorecard.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobProfileStatusObject {
    private Long scorecardId;
    private boolean statusActive;
}
