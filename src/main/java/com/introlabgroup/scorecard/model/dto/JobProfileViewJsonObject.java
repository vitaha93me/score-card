package com.introlabgroup.scorecard.model.dto;

import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.model.onet.MeasuringType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author vitalii.
 */
@Setter
@Getter
public class JobProfileViewJsonObject {

    private Long id;
    private String title;
    private String occupationJson;
    private MeasuringType measuringType;
    private String description;
    private boolean statusActive;
    private boolean fake;
    private boolean fullyConfigured;
    private CompanyDepartment department;
}
