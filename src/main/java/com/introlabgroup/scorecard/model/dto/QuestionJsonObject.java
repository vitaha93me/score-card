package com.introlabgroup.scorecard.model.dto;

import com.introlabgroup.scorecard.model.LeadingQuestion;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author vitalii.
 */
@Getter
@Setter
public class QuestionJsonObject {

    private long recordsTotal;

    private long recordsFiltered;

    private List<LeadingQuestion> data;
}