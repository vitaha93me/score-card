package com.introlabgroup.scorecard.model.dto;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.Role;
import com.introlabgroup.scorecard.model.payment.StripeSubscription;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

/**
 * @author vitalii.
 */
@Getter
@Setter
@ToString
public class UserDTO {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private Set<Role> roles;
    private Company company;
    private StripeSubscription subscription;
}
