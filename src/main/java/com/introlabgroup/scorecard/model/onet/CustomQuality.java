package com.introlabgroup.scorecard.model.onet;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@IdClass(OnetItemIDClass.class)
public class CustomQuality extends QualityCategory implements Serializable {

    @Id
    @Column(name = "onetsoc_code", length = 20971520)
    private String occupation;

    @Id
    @ManyToOne
    @JoinColumn(name = "element_id")
    private OnetItem onetItem;

    @Column(name = "data_value")
    private Double weight;

    private boolean required = true;

    private boolean dealBreaker;


    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    private PriorityRanking priorityRanking;

    public static CustomQuality fromOnetItem(OnetItem onetItem, String occupationId) {
        CustomQuality skills = new CustomQuality();
        skills.setOccupation(occupationId);
        skills.setOnetItem(onetItem);
        skills.setWeight(4d);
        skills.setPriorityRanking(PriorityRanking.MUST_HAVE);
        return skills;
    }
}
