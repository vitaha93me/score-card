package com.introlabgroup.scorecard.model.onet;

/**
 * @author vitalii.
 */
public enum MeasuringType {

    WEIGHT {
        @Override
        public String toString() {
            return "Calculate scores with O'Net weights";
        }
    },
    PRIORITY {
        @Override
        public String toString() {
            return "Calculate scores with priority ranking";
        }
    }
}
