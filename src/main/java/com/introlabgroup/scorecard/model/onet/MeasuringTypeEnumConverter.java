package com.introlabgroup.scorecard.model.onet;

import java.beans.PropertyEditorSupport;

/**
 * @author vitalii.
 */
public class MeasuringTypeEnumConverter extends PropertyEditorSupport {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        MeasuringType measuringType = MeasuringType.valueOf(text);
        setValue(measuringType);
    }
}
