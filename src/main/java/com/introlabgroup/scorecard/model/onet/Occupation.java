package com.introlabgroup.scorecard.model.onet;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "occupation_data")
public class Occupation {

    @Id
    @Column(columnDefinition = "text")
    private String onetsoc_code;
    private String title;
    private String description;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<Abilities> abilities;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<Interests> interests;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<Skills> skills;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<Knowledge> knowledge;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<WorkActivities> workActivities;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<WorkStyles> workStyles;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<WorkValues> workValues;

    @OneToMany(mappedBy = "occupation", fetch = FetchType.LAZY)
    private List<CustomQuality> customQualities;
}
