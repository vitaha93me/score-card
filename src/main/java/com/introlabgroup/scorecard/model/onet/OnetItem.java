package com.introlabgroup.scorecard.model.onet;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.introlabgroup.scorecard.model.LeadingQuestion;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "content_model_reference")
public class OnetItem {

    @Id
    private String elementId;
    private String elementName;
    private String description;
    private boolean custom;

    @OneToMany(mappedBy = "onetItem", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<LeadingQuestion> leadingQuestions;

}
