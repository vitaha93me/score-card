package com.introlabgroup.scorecard.model.onet;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author vitalii.
 */

public class OnetItemIDClass implements Serializable {

    private String occupation;

    private String onetItem;

    public OnetItemIDClass(String occupation, String onetItem) {
        this.occupation = occupation;
        this.onetItem = onetItem;
    }

    public OnetItemIDClass() {
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOnetItem() {
        return onetItem;
    }

    public void setOnetItem(String onetItem) {
        this.onetItem = onetItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OnetItemIDClass)) return false;
        OnetItemIDClass that = (OnetItemIDClass) o;
        return Objects.equals(getOccupation(), that.getOccupation()) &&
                Objects.equals(getOnetItem(), that.getOnetItem());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOccupation(), getOnetItem());
    }
}