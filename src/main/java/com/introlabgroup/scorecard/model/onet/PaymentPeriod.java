package com.introlabgroup.scorecard.model.onet;

/**
 * @author vitalii.
 */
public enum PaymentPeriod {

    MONTHLY {
        @Override
        public String toString() {
            return "Monthly";
        }

        @Override
        public String getValue() {
            return "MONTHLY";
        }
    },
    ANNUALLY {
        @Override
        public String toString() {
            return "Annually";
        }

        @Override
        public String getValue() {
            return "ANNUALLY";
        }
    },
    DAILY {
        @Override
        public String toString() {
            return "Daily";
        }

        @Override
        public String getValue() {
            return "DAILY";
        }
    };

    public String getValue() {
        return "DAILY";
    }
}
