package com.introlabgroup.scorecard.model.onet;

import java.beans.PropertyEditorSupport;

/**
 * @author vitalii.
 */
public class PaymentPeriodEnumConverter extends PropertyEditorSupport {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        PaymentPeriod paymentPeriod = PaymentPeriod.valueOf(text);
        setValue(paymentPeriod);
    }
}
