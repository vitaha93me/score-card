package com.introlabgroup.scorecard.model.onet;

/**
 * @author vitalii.
 */
public enum PriorityRanking {

    MUST_HAVE {
        @Override
        public String toString() {
            return "Must Have Quality";
        }

        @Override
        public double getDoubleVale() {
            return 1D;
        }
    },
    NICE_TO_HAVE {
        @Override
        public String toString() {
            return "Nice to have Quality";
        }

        @Override
        public double getDoubleVale() {
            return 0.5;
        }
    },
    LOW_PRIORITY {
        @Override
        public String toString() {
            return "Low priority Quality";
        }

        @Override
        public double getDoubleVale() {
            return 0.25;
        }
    },
    DEAL_BREAKER {
        @Override
        public String toString() {
            return "Deal breaker";
        }
    },
    NO_PRIORITY {
        @Override
        public String toString() {
            return "No priority";
        }
    };

    public double getDoubleVale() {
        return 0;
    }
}
