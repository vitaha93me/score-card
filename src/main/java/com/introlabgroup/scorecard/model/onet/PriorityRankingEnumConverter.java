package com.introlabgroup.scorecard.model.onet;

import java.beans.PropertyEditorSupport;

/**
 * @author vitalii.
 */
public class PriorityRankingEnumConverter extends PropertyEditorSupport {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        PriorityRanking priorityRanking = PriorityRanking.valueOf(text);
        setValue(priorityRanking);
    }
}
