package com.introlabgroup.scorecard.model.onet;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class QualityCategory implements Serializable {

    private String occupation;

    private OnetItem onetItem;

    private Double weight;

    private boolean required = true;

    private boolean dealBreaker;

    private PriorityRanking priorityRanking;
}
