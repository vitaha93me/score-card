package com.introlabgroup.scorecard.model.payment;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StripeData {
    private long id;
    private long companyId;
    private String stripeId;
    private Date created;

    public StripeData(Long companyId, String stripeId) {
        this.stripeId = stripeId;
        this.companyId = companyId;
    }
}
