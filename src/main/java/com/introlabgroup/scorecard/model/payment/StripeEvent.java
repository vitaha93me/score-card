package com.introlabgroup.scorecard.model.payment;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class StripeEvent {
    private Long id;
    private long companyId;
    private String subscriptionId;
    private String companyName;
    private boolean status;
    private Date created;
}
