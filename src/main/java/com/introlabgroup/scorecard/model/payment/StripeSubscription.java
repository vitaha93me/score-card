package com.introlabgroup.scorecard.model.payment;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StripeSubscription {
    private Long id;
    private Long companyId;
    private String subscriptionId;
    private String type;
    private String email;
    private Integer price;
    private Integer scoreCardCount;
    private Boolean currentStatus;
    private Date created;
    private Integer scorecardsLeft;
    private Date lastPayment;
}
