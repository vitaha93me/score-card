package com.introlabgroup.scorecard.model.payment;

import com.introlabgroup.scorecard.model.dto.JobProfileStatusObject;
import com.introlabgroup.scorecard.model.onet.PaymentPeriod;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class SubscriptionDTO {
    private String cardToken;
    private long companyId;
    private String companyName;
    private int scorecardCount;
    private Integer price;
    private PaymentPeriod paymentPeriod;
    private String email;
    private String firstName;
    private String lastName;
    private List<JobProfileStatusObject> scorecardStatuses;

}
