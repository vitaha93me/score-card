package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_assessment")
public class Assessment {

    @Id
    @GeneratedValue
    private Integer id;

    private String attachment;

    @Transient
    private MultipartFile[] files;

    private Integer ranking;
}
