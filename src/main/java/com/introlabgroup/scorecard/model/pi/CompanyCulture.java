package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_company_culture")
public class CompanyCulture {

    @Id
    @GeneratedValue
    private Integer id;
    private String cultureItem;
    @Lob
    private String description;
    private Integer ranking;
}
