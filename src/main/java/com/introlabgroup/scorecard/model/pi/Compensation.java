package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_compensation")
public class Compensation {

    @Id
    @GeneratedValue
    private Integer id;
    @Lob
    private String description;
    private String company;
    private String title;
    private Double baseAnnualIncome;
    private Double cashIncentives;
    private Double benefits;
    private Boolean healthInsurance;
    private Boolean companyPhone;
    private Boolean companyCar;
    private Boolean retirement;
    private Boolean other;
    private Integer ranking;

}
