package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_job_address")
public class JobAddressDetails {

    @Id
    @GeneratedValue
    private Integer id;

    private String streetName;
    private String buildingNumber;
    private String city;
    private String startTime; //ex. 9:30
    private String endTime; //ex. 9:30
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "addressId")
    private List<TimeOnTheWay> ways;

    private Integer toHome;
    private Integer toWork;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobAddressDetails that = (JobAddressDetails) o;
        return Objects.equals(streetName, that.streetName) &&
                Objects.equals(city, that.city) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(streetName, city, startTime, endTime);
    }
}
