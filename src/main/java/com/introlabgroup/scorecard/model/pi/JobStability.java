package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_job_stability")
public class JobStability {

    @Id
    @GeneratedValue
    private Integer id;
    private String jobTitle;
    private String company;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date startDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date endDate;
}
