package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vitalii.
 */
@Setter
@Getter
@Entity(name = "power_interview")
public class PowerInterview {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private Long candidateId;
    private Long jobProfileID;

    private Integer referencesScale;
    private Integer jobStabilityScale;
    private Integer achievementsScale;
    private Integer leavingReasonScale;
    private Integer companyCultureScale;
    private Integer workSamplesScale;
    private Integer timeOnTheWayScale;
    private Integer recommendationLetter;
    private Integer assessmentsScale;
    private Integer compensationsScale;
    private Integer measuringScale;
    private Boolean specialFactorScale;
    private Boolean hasCleanDrivingRecords;
    private Boolean canPassDrugTest;
    private Boolean canPassCreditCheck;
    private Boolean canPassBackgroundCheck;
    private Boolean canPassPhysical;


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<JobAddressDetails> addressDetails = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<References> references = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<JobStability> jobStability = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<Achievements> achievements = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<LeavingReason> leavingReasons = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<CompanyCulture> companyCulture = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<WorkSample> workSamples = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<TimeOnTheWay> timeOnTheWay = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<RecommendationLetter> recommendationLetters = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<Assessment> assessments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<Certifications> certifications = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<Licenses> licenses = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<AcademicHistory> academicHistories = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<Compensation> compensations = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<Measuring> measuring = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pi_id")
    private List<SpecialFactor> specialFactors = new ArrayList<>();
}