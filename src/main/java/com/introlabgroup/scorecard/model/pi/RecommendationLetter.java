package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_recommendation_letter")
public class RecommendationLetter {

    @Id
    @GeneratedValue
    private Integer id;

    @Lob
    private String description;

    private String attachment;

    @Transient
    private MultipartFile file;

}
