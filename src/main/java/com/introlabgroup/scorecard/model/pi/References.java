package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_references")
public class References {

    @Id
    @GeneratedValue
    private Integer id;

    @Lob
    private String expectedComments;
    @Lob
    private String actualComments;

    private String personName;
    private String title;
    private String companyAssociation;
    private String phone;
    private String workPhone;
    private String email;
}
