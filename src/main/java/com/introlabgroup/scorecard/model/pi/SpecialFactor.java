package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_special_factor")
public class SpecialFactor {

    @Id
    @GeneratedValue
    private Integer id;

    @Lob
    private String description;

    private String link;
    private Boolean passed;
    private String name;
    private String attachment;

    @Transient
    private MultipartFile[] files;

}
