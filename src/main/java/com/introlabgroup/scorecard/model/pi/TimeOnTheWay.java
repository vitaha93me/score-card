package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_time_on_the_way")
public class TimeOnTheWay {

    @Id
    @GeneratedValue
    private Integer id;
    private Integer addressId;
    private String mode;
    private String direction;
    private String originAddress;
    private String destinationAddress;
    private String distanceText;
    private int distanceValue;
    private String durationText;
    private int durationValue;
}
