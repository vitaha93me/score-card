package com.introlabgroup.scorecard.model.pi;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Entity(name = "pi_work_sample")
public class WorkSample {

    @Id
    @GeneratedValue
    private Integer id;

    @Lob
    private String taskDescription;

    private String link;

    private String attachment;

    @Transient
    private MultipartFile file;

    private Integer ranking;
}
