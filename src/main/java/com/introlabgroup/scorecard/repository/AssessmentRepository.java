package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.Assessment;
import com.introlabgroup.scorecard.model.JobProfile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface AssessmentRepository extends PagingAndSortingRepository<Assessment, Long> {

    void deleteAssessmentByJobProfile(JobProfile jobProfile);

    @Modifying
    @Query(nativeQuery = true,value = "DELETE FROM assessment WHERE fk_job_profile=?1")
    void deleteAssessmentsByJobProfileId(Long profileId);
}

