package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.CandidateReference;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface CandidateReferencesRepository extends PagingAndSortingRepository<CandidateReference, Integer> {
}
