package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.Candidate;
import com.introlabgroup.scorecard.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author vitalii.
 */
@Repository
public interface CandidateRepository extends PagingAndSortingRepository<Candidate, Long> {

    @Query(value = "select c from Candidate c where c.company=?3 and (c.name like %?1% or c.lastName like %?2%)")
    Page<Candidate> findAllByNameContainsOrLastNameContainsAndCompany(String name, String lastName, Company company, Pageable pageable);

    Candidate findByCatsId(long catsId);

    List<Candidate> findAllByCompany(Company company);
}
