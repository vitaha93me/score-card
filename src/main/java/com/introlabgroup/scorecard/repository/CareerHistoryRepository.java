package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.CareerHistory;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface CareerHistoryRepository extends PagingAndSortingRepository<CareerHistory, Long> {
}
