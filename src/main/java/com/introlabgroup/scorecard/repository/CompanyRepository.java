package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    Company findByName(String name);

    @Query(nativeQuery = true,
            value = "select count(*) from company c where c.company_id=?1 and c.paid=false and now() < adddate(c.end_of_period, interval 7 day)")
    Long countForTrial(Long id);

    @Query(nativeQuery = true,
            value = "select count(*) from company c where c.company_id=?1 and c.paid=true and now() < adddate(c.end_of_period, interval 7 day)")
    Long countForPayedPeriod(Long id);

}
