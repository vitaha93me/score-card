package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.CompanyDepartment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author vitalii.
 */
@Repository
public interface DepartmentRepository extends JpaRepository<CompanyDepartment, Long> {

    CompanyDepartment findByName(String name);

    CompanyDepartment findByNameAndCompany(String name, Company company);

    @Query(value = "select cd from CompanyDepartment cd where cd.company=?1 and (cd.name like %?2% or cd.address like %?2% or cd.description like %?2% or cd.city like %?2%)")
    Page<CompanyDepartment> findAllByCompany(Company company, String keyword, Pageable pageable);

    List<CompanyDepartment> findAllByCompany(Company company);

}
