package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.model.JobProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * @author vitalii.
 */
@Repository
public interface JobProfileRepository extends PagingAndSortingRepository<JobProfile, Long> {


    @Query(value = "select j from JobProfile j where j.department=?1 and j.title like %?2% and j.status=true")
    Page<JobProfile> findAllActiveByDepartment(CompanyDepartment department, String keyword, Pageable pageable);

    @Query(value = "select j from JobProfile j where j.department in (?1) and j.title like %?2% and j.status=true")
    Page<JobProfile> findAllActiveByDepartmentIn(Collection<CompanyDepartment> departments, String keyword, Pageable pageable);

    @Query(value = "select j from JobProfile j where j.department in (?1) and j.status=true")
    List<JobProfile> findAllActiveByDepartmentIn(Collection<CompanyDepartment> departments);

    @Query(value = "select j from JobProfile j where j.department in (?1)")
    List<JobProfile> findAllByDepartmentIn(Collection<CompanyDepartment> departments);

    List<JobProfile> findAll();

    Page<JobProfile> findAll(Pageable pageable);

}
