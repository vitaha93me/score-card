package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.Assessment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface PaymentRepository extends PagingAndSortingRepository<Assessment, Long> {
}
