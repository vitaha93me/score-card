package com.introlabgroup.scorecard.repository;

import com.introlabgroup.scorecard.model.pi.PowerInterview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author vitalii.
 */
@Repository
public interface PowerInterviewRepository extends JpaRepository<PowerInterview, Long> {

    List<PowerInterview> findByCandidateId(Long candidateId);
}
