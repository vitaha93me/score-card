package com.introlabgroup.scorecard.repository;


import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.Role;
import com.introlabgroup.scorecard.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    List<User> findUsersByEmail(String email);

    User findByEmailAndPassword(String email, String password);

    List<User> findByCompany(Company company);

    @Query(value = "select u from User u join u.roles roles where u.company=?1 and (u.name like %?2% or u.lastName like %?2% or u.jobTitle like %?2%) and roles in ?3")
    Page<User> findByCompany(Company company, String keyword, Collection<Role> roles, Pageable pageable);


}
