package com.introlabgroup.scorecard.repository.onet;

import com.introlabgroup.scorecard.model.onet.CustomQuality;
import com.introlabgroup.scorecard.model.onet.OnetItemIDClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface CustomQualityRepository extends JpaRepository<CustomQuality, OnetItemIDClass> {


}
