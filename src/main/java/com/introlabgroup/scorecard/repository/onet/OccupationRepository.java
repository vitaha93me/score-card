package com.introlabgroup.scorecard.repository.onet;

import com.introlabgroup.scorecard.model.onet.Occupation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author vitalii.
 */
@Repository
public interface OccupationRepository extends JpaRepository<Occupation, String> {

    List<Occupation> findByTitleStartsWith(String keyword);

    Set<Occupation> findByTitleContainingIgnoreCase(String keyword);
}
