package com.introlabgroup.scorecard.repository.onet;

import com.introlabgroup.scorecard.model.onet.OnetItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author vitalii.
 */
@Repository
public interface OnetItemRepository extends JpaRepository<OnetItem, String> {

    List<OnetItem> findAllByElementNameContainsAndCustom(String keyword, boolean custom);

    List<OnetItem> findByElementNameIgnoreCase(String keyword);

    List<OnetItem> findByElementNameContainingIgnoreCase(String keyword);
}
