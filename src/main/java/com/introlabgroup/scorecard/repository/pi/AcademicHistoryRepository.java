package com.introlabgroup.scorecard.repository.pi;

import com.introlabgroup.scorecard.model.pi.AcademicHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface AcademicHistoryRepository extends JpaRepository<AcademicHistory, Long> {
}
