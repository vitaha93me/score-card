package com.introlabgroup.scorecard.repository.pi;

import com.introlabgroup.scorecard.model.pi.Achievements;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface AchievementsRepository extends JpaRepository<Achievements, Long> {
}
