package com.introlabgroup.scorecard.repository.pi;

import com.introlabgroup.scorecard.model.pi.JobAddressDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface JobAddressDetailsRepository extends JpaRepository<JobAddressDetails, Integer> {
}
