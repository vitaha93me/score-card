package com.introlabgroup.scorecard.repository.pi;

import com.introlabgroup.scorecard.model.pi.JobStability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface JobStabilityRepository extends JpaRepository<JobStability, Long> {
}
