package com.introlabgroup.scorecard.repository.pi;

import com.introlabgroup.scorecard.model.pi.SpecialFactor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vitalii.
 */
@Repository
public interface SpecialFactorRepository extends JpaRepository<SpecialFactor, Long> {
}
