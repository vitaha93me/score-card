package com.introlabgroup.scorecard.service;


import com.introlabgroup.scorecard.model.Candidate;
import com.introlabgroup.scorecard.model.CandidateReference;
import com.introlabgroup.scorecard.model.CareerHistory;
import com.introlabgroup.scorecard.model.Company;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CandidateService {

    Page<Candidate> findAll(Company company, String name, String lastName, int page, int size, String direction, String column);

    List<Candidate> findAll(Company company);

    Candidate save(Candidate candidate);

    void delete(Long id);

    Candidate findById(long id);

    CareerHistory addCareerHistory(CareerHistory careerHistory);

    CandidateReference saveCandidateReference(CandidateReference candidateReference);
}