package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.*;
import com.introlabgroup.scorecard.repository.CandidateReferencesRepository;
import com.introlabgroup.scorecard.repository.CandidateRepository;
import com.introlabgroup.scorecard.repository.CareerHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * @author vitalii.
 */
@Repository
public class CandidateServiceImpl implements CandidateService {

    private final CandidateRepository candidateRepository;

    private final CareerHistoryRepository historyRepository;

    private final CandidateReferencesRepository referencesRepository;

    @Autowired
    public CandidateServiceImpl(CandidateRepository candidateRepository, CareerHistoryRepository historyRepository, CandidateReferencesRepository referencesRepository) {
        this.candidateRepository = candidateRepository;
        this.historyRepository = historyRepository;
        this.referencesRepository = referencesRepository;
    }

    @Override
    @Transactional
    public Page<Candidate> findAll(Company company, String name, String lastName, int page, int size, String direction, String column) {
        PageRequest pageable = new PageRequest(page, size, direction.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, column);
        return candidateRepository.findAllByNameContainsOrLastNameContainsAndCompany(name, lastName, company, pageable);
    }

    @Override
    public List<Candidate> findAll(Company company) {
        return candidateRepository.findAllByCompany(company);
    }

    @Override
    public Candidate save(Candidate candidate) {
        return candidateRepository.save(candidate);
    }


    @Override
    public void delete(Long id) {
        candidateRepository.delete(id);
    }

    @Override
    @Transactional
    @SuppressWarnings("all")
    public Candidate findById(long id) {
        Candidate one = candidateRepository.findOne(id);
        Set<CompanyDepartment> departments = one.getCompany().getDepartments();
        System.out.println(one.getCompany().getDepartments().size());
        return one;
    }

    @Override
    public CareerHistory addCareerHistory(CareerHistory careerHistory) {
        return historyRepository.save(careerHistory);
    }

    @Override
    public CandidateReference saveCandidateReference(CandidateReference candidateReference) {
        return referencesRepository.save(candidateReference);
    }


}
