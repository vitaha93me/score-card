package com.introlabgroup.scorecard.service;


import com.introlabgroup.scorecard.model.Company;

import java.util.List;

public interface CompanyService {
    Company findById(Long id);

    Company findByName(String name);

    Company saveCompany(Company company);

    boolean isTrial(Long companyId);

    boolean isPeriodFinished(Long companyId);

    void delete(Company company);

    void delete(long id);

    List<Company> findAll();
}
