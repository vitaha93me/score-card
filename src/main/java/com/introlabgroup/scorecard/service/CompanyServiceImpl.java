package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.Candidate;
import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.model.User;
import com.introlabgroup.scorecard.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vitalii.
 */
@Repository
public class CompanyServiceImpl implements CompanyService {

    private final JdbcTemplate jdbcTemplate;

    private final CompanyRepository companyRepository;

    private final UserService userService;

    private final CandidateService candidateService;

    private final DepartmentService departmentService;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, DataSource dataSource, UserService userService, CandidateService candidateService, DepartmentService departmentService) {
        this.companyRepository = companyRepository;
        this.jdbcTemplate = new JdbcTemplate(dataSource);

        this.userService = userService;
        this.candidateService = candidateService;
        this.departmentService = departmentService;
    }

    @Override
    public Company findById(Long id) {
        return companyRepository.findOne(id);
    }

    @Override
    public Company findByName(String name) {
        return companyRepository.findByName(name);
    }

    @Override
    public Company saveCompany(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public boolean isTrial(Long companyId) {
        return companyRepository.countForTrial(companyId) > 0;
    }

    @Override
    public boolean isPeriodFinished(Long companyId) {
        return companyRepository.countForPayedPeriod(companyId) < 1;
    }

    @Override
    public void delete(Company company) {
        companyRepository.delete(company);
    }

    @Override
    public void delete(long id) {
        Company company = findById(id);

        List<Candidate> candidates = candidateService.findAll(company);

        for (Candidate candidate : candidates) {
            jdbcTemplate.update("delete from candidate_reference where fk_candidate=?", candidate.getId());
            jdbcTemplate.update("delete from career_history where fk_candidate=?", candidate.getId());
            jdbcTemplate.update("delete from assessment where fk_candidate=?", candidate.getId());
        }
        jdbcTemplate.update("delete from candidate where fk_company=?", company.getId());

        List<User> users = userService.findAllByCompany(company);
        for (User user : users) {
            jdbcTemplate.update("delete from user_role where user_id=?", user.getId());
            jdbcTemplate.update("delete from user_department where user_id=?", user.getId());
        }
        jdbcTemplate.update("delete from user where company_id=?", company.getId());

        List<CompanyDepartment> departments = departmentService.findAllByCompany(company, false);
        for (CompanyDepartment department : departments) {
            jdbcTemplate.update("delete from job_profile where fk_department=?", department.getId());
        }
        jdbcTemplate.update("delete from department where fk_company=?", id);
        jdbcTemplate.update("delete from company where company_id=?", id);

    }

    @Override
    public List<Company> findAll() {
        List<Company> companies = companyRepository.findAll();
        return companies.stream().peek(company -> {
            company.setUsers(new HashSet<>());
            company.setDepartments(new HashSet<>());
        }).collect(Collectors.toList());
    }

}
