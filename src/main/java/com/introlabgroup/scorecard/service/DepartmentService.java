package com.introlabgroup.scorecard.service;


import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.CompanyDepartment;
import org.springframework.data.domain.Page;

import java.util.List;

public interface DepartmentService {
    CompanyDepartment findById(Long id, boolean lazy);


    CompanyDepartment findByNameAndCompany(String name, Company company);

    void saveDepartment(CompanyDepartment department);

    void delete(CompanyDepartment department);

    void delete(long id);

    List<CompanyDepartment> findAll();

    Page<CompanyDepartment> findAllByCompany(Company company, String keyword, int page, int size, String direction, String column, boolean lazy);

    List<CompanyDepartment> findAllByCompany(Company company,  boolean lazy);
}
