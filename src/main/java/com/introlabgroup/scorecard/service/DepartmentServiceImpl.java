package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vitalii.
 */
@Repository
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    @Transactional
    public CompanyDepartment findById(Long id, boolean lazy) {
        CompanyDepartment department = departmentRepository.findOne(id);
        if (lazy) {
            department.setOpenPositions(new ArrayList<>());
            return department;
        } else {
            if (department != null && department.getOpenPositions() != null) {
                department.getOpenPositions().size();
            }
            return department;
        }
    }

    @Override
    public CompanyDepartment findByNameAndCompany(String name, Company company) {
        return departmentRepository.findByNameAndCompany(name, company);
    }


    @Override
    public void saveDepartment(CompanyDepartment department) {
        departmentRepository.save(department);
    }

    @Override
    @Transactional
    public void delete(CompanyDepartment department) {
        departmentRepository.delete(department);
    }

    @Override
    @Transactional
    public void delete(long id) {
        departmentRepository.delete(id);
    }

    @Override
    public List<CompanyDepartment> findAll() {
        return departmentRepository.findAll();
    }

    @Override
    public Page<CompanyDepartment> findAllByCompany(Company company, String keyword, int page, int size, String direction, String column, boolean lazy) {
        PageRequest pageable = new PageRequest(page, size, direction.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, column);

        Page<CompanyDepartment> departments = departmentRepository.findAllByCompany(company, keyword, pageable);
        if (lazy) {
            departments.forEach(department -> department.setOpenPositions(new ArrayList<>()));
        } else {
            for (CompanyDepartment department : departments) {
                department.getOpenPositions().size();
            }
        }
        return departments;
    }

    @Override
    @Transactional
    public List<CompanyDepartment> findAllByCompany(Company company, boolean lazy) {
        List<CompanyDepartment> departments = departmentRepository.findAllByCompany(company);
        if (lazy) {
            return departments.stream().peek(department -> department.setOpenPositions(new ArrayList<>())).collect(Collectors.toList());
        } else {
            for (CompanyDepartment department : departments) {
                department.getOpenPositions().size();
            }
        }
        return departments;
    }

}
