package com.introlabgroup.scorecard.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabgroup.scorecard.model.pi.TimeOnTheWay;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author vitalii.
 */
@Service
public class GoogleMapsService {

    private static final Logger logger = LoggerFactory.getLogger(GoogleMapsService.class);


    @Value("${google.maps.api.key}")
    private String MAPS_API_KEY;

    private static final List<String> tripModes = Collections.singletonList("driving");//, "walking", "bicycling", "transit");

    private static final String URL_HOME = "https://maps.googleapis.com/maps/api/distancematrix/json?";
    private static final String URL_PATTERN = "units=metric&origins={0}&destinations={1}&mode={2}&arrival_time={3}&key={4}";

    public List<TimeOnTheWay> calculateTripData(String origin, String destination, long time, String direction) {
        List<TimeOnTheWay> timeDistances = new ArrayList<>();
        for (String tripMode : tripModes) {
            String requestUrl = buildRequestUrl(origin, destination, tripMode, time);
            String json = null;
            try {
                String url = URL_HOME + requestUrl;
                System.out.println(url);
                Document document = Jsoup.connect(url).ignoreContentType(true).get();
                json = document.text();
                if (json.contains("ZERO_RESULTS")) {
                    continue;
                }
                System.out.println(json);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObject jsonElement = new JsonParser().parse(json != null ? json : "").getAsJsonObject();
            String destinationAddress = jsonElement.getAsJsonArray("destination_addresses").getAsString();
            String originAddresses = jsonElement.getAsJsonArray("origin_addresses").getAsString();

            JsonObject tripElement = jsonElement.getAsJsonArray("rows").get(0).getAsJsonObject().getAsJsonArray("elements").get(0).getAsJsonObject();

            JsonObject distance = tripElement.getAsJsonObject("distance");
            String distanceText = distance.getAsJsonPrimitive("text").getAsString();
            int distanceValue = distance.getAsJsonPrimitive("value").getAsInt();

            JsonObject duration = tripElement.getAsJsonObject("duration");
            String durationText = duration.getAsJsonPrimitive("text").getAsString();
            int durationValue = duration.getAsJsonPrimitive("value").getAsInt();

            TimeOnTheWay timeDistance = new TimeOnTheWay();
            timeDistance.setMode(tripMode);
            timeDistance.setDestinationAddress(destinationAddress);
            timeDistance.setOriginAddress(originAddresses);
            timeDistance.setDistanceText(distanceText);
            timeDistance.setDistanceValue(distanceValue);
            timeDistance.setDurationText(durationText);
            timeDistance.setDurationValue(durationValue);
            timeDistance.setDirection(direction);
            timeDistances.add(timeDistance);
            System.out.println(timeDistance);
        }
        return timeDistances;
    }

    private String buildRequestUrl(String origin, String destination, String mode, long time) {
        return MessageFormat.format(URL_PATTERN, origin, destination, mode, Long.toString(time), MAPS_API_KEY);
    }
}
