package com.introlabgroup.scorecard.service;


import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.model.JobProfile;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface JobProfileService {
    JobProfile findById(Long id);

    JobProfile saveJobProfile(JobProfile jobProfile);

    void delete(JobProfile jobProfile);

    void delete(long id);

    List<JobProfile> findAll();

    Page<JobProfile> findAll(int page, int size, String direction, String column);

    List<JobProfile> findAllByCompany(Company company);

    Page<JobProfile> findAllActiveByDepartment(CompanyDepartment department, String keyword, int page, int size, String direction, String column);

    Page<JobProfile> findAllActiveByDepartments(Collection<CompanyDepartment> departments, String keyword, int page, int size, String direction, String column);

    List<JobProfile> findAllActiveByDepartments(Collection<CompanyDepartment> departments);

    List<JobProfile> findAllByDepartments(Collection<CompanyDepartment> departments);

//    Page<JobProfile> findAllByDepartment(CompanyDepartment department, String keyword, int page, int size, String direction, String column);
//
//    Page<JobProfile> findAllByDepartments(Collection<CompanyDepartment> departments, String keyword, int page, int size, String direction, String column);
//
//    List<JobProfile> findAllByDepartments(Collection<CompanyDepartment> departments);
}
