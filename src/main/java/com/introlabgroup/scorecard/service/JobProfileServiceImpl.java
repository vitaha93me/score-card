package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.model.JobProfile;
import com.introlabgroup.scorecard.repository.JobProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * @author vitalii.
 */
@Service
public class JobProfileServiceImpl implements JobProfileService {


    private final JobProfileRepository jobProfileRepository;

    @Autowired
    public JobProfileServiceImpl(JobProfileRepository jobProfileRepository) {
        this.jobProfileRepository = jobProfileRepository;
    }

    @Override
    public JobProfile findById(Long id) {
        return jobProfileRepository.findOne(id);
    }

    @Override
    public JobProfile saveJobProfile(JobProfile jobProfile) {
        return jobProfileRepository.save(jobProfile);
    }

    @Override
    public void delete(JobProfile jobProfile) {
        jobProfileRepository.delete(jobProfile);
    }

    @Override
    public void delete(long id) {
        jobProfileRepository.delete(id);
    }

    @Override
    public Page<JobProfile> findAll(int page, int size, String direction, String column) {
        PageRequest pageable = new PageRequest(page, size, direction.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, column);
        return jobProfileRepository.findAll(pageable);
    }

    @Override
    public List<JobProfile> findAll() {
        return jobProfileRepository.findAll();
    }

    @Override
    public Page<JobProfile> findAllActiveByDepartment(CompanyDepartment department, String keyword, int page, int size, String direction, String column) {
        PageRequest pageable = new PageRequest(page, size, direction.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, column);
        return jobProfileRepository.findAllActiveByDepartment(department, keyword, pageable);
    }

    @Override
    public Page<JobProfile> findAllActiveByDepartments(Collection<CompanyDepartment> departments, String keyword, int page, int size, String direction, String column) {
        PageRequest pageable = new PageRequest(page, size, direction.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, column);
        return jobProfileRepository.findAllActiveByDepartmentIn(departments, keyword, pageable);
    }

    @Override
    public List<JobProfile> findAllActiveByDepartments(Collection<CompanyDepartment> departments) {
        return jobProfileRepository.findAllActiveByDepartmentIn(departments);
    }

    @Override
    public List<JobProfile> findAllByDepartments(Collection<CompanyDepartment> departments) {
        return jobProfileRepository.findAllByDepartmentIn(departments);
    }

    @Override
    @Transactional
    public List<JobProfile> findAllByCompany(Company company) {
        return jobProfileRepository.findAllByDepartmentIn(company.getDepartments());
    }
}
