package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author vitalii.
 */
@Service
public class MailSendingService {

    private static final String ADMIN_TEMPLATE_ID = "d15aea94-b6f8-4eb3-91d1-e7bce2590978";
    private static final String MANAGER_TEMPLATE_ID = "19fd977f-88f1-4571-9419-9b4ec1eb645e";
    private static final String SUPER_ADMIN_TEMPLATE_ID = "ae75faed-5563-4f15-8ccf-5968c40fd16c";

    private final SendGridService sendGridService;

    @Autowired
    public MailSendingService(SendGridService sendGridService) {
        this.sendGridService = sendGridService;
    }

    public void sendAdminNotification(User user, String invitePerson ){
        sendGridService.sendCredentials(user, invitePerson, ADMIN_TEMPLATE_ID);
    }

    public void sendManagerNotification(User user, String invitePerson ){
        sendGridService.sendCredentials(user, invitePerson, MANAGER_TEMPLATE_ID);
    }

    public void sendSuperAdminNotification(User user, String invitePerson ){
        sendGridService.sendCredentials(user, invitePerson, SUPER_ADMIN_TEMPLATE_ID);
    }
}
