package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.User;
import com.introlabgroup.scorecard.model.pi.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author vitalii.
 */
@Repository
public interface PowerInterviewService {

    PowerInterview save(PowerInterview powerInterview);

    PowerInterview uploadWorkSample(WorkSample workSample, Long powerInterviewId);

    PowerInterview uploadCertifications(Certifications certifications, Long powerInterviewId);
    PowerInterview uploadLicenses(Licenses certifications, Long powerInterviewId);
    PowerInterview uploadAcademicHistory(AcademicHistory certifications, Long powerInterviewId);

    PowerInterview uploadAssessment(Assessment assessment, Long powerInterviewId);

    PowerInterview uploadRecommendationLetter(RecommendationLetter letter, Long powerInterviewId);

    PowerInterview uploadSpecialFactor(SpecialFactor specialFactor, Long powerInterviewId);

    PowerInterview update(PowerInterview powerInterview);

    PowerInterview findById(Long id);

    List<PowerInterview> findByCandidateId(Long candidateId, User loggedUser);

    void delete(PowerInterview powerInterview);

    void delete(Long id);

    void updateAchievements(Achievements achievements);
    void updateCompanyCulture(CompanyCulture companyCulture);
    void updateAssessment(Assessment assessment, Long interviewId);
    void updateCertification(Certifications certifications, Long interviewId);
    void updateLicenses(Licenses certifications, Long interviewId);
    void updateAcademicHistory(AcademicHistory certifications, Long interviewId);
    void updateCompensation(Compensation compensation);
    void updateJobAddressDetails(JobAddressDetails jobAddressDetails);
    void updateJobStability(JobStability jobStability);
    void updateLeavingReason(LeavingReason leavingReason);
    void updateMeasuring(Measuring measuring);
    void updateRecommendationLetter(RecommendationLetter recommendationLetter, Long interviewId);
    void updateReferences(References references);
    void updateSpecialFactors(SpecialFactor specialFactor, Long interviewId);
    void updateWorkSample(WorkSample workSample, Long interviewId);

}
