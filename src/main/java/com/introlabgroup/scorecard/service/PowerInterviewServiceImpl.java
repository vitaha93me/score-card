package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.Candidate;
import com.introlabgroup.scorecard.model.CompanyDepartment;
import com.introlabgroup.scorecard.model.User;
import com.introlabgroup.scorecard.model.pi.*;
import com.introlabgroup.scorecard.repository.PowerInterviewRepository;
import com.introlabgroup.scorecard.repository.pi.*;
import com.introlabgroup.scorecard.utils.CandidateUtils;
import com.introlabgroup.scorecard.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.introlabgroup.scorecard.utils.UserUtils.isManager;

/**
 * @author vitalii.
 */
@Service
public class PowerInterviewServiceImpl implements PowerInterviewService {

    private static final Logger logger = LoggerFactory.getLogger(PowerInterviewServiceImpl.class);

    @Value("${attachments.samples.directory}")
    private String samplesDirectory;

    @Value("${attachments.certifications.directory}")
    private String certificationsDirectory;

    @Value("${attachments.licenses.directory}")
    private String licensesDirectory;

    @Value("${attachments.academic.directory}")
    private String academicHistoryDirectory;

    @Value("${attachments.assessments.directory}")
    private String assessmentsDirectory;

    @Value("${attachments.letters.directory}")
    private String lettersDirectory;

    @Value("${attachments.factors.directory}")
    private String factorsDirectory;

    private final GoogleMapsService mapsService;

    private final CandidateService candidateService;

    private final PowerInterviewRepository repository;

    private final AchievementsRepository achievementsRepository;

    private final AssessmentPIRepository assessmentPIRepository;

    private final CertificationsRepository certificationsRepository;

    private final LicencesRepository licencesRepository;

    private final AcademicHistoryRepository academicHistoryRepository;

    private final CompensationRepository compensationRepository;

    private final JobAddressDetailsRepository jobAddressDetailsRepository;

    private final JobStabilityRepository jobStabilityRepository;

    private final LeavingReasonRepository leavingReasonRepository;

    private final MeasuringRepository measuringRepository;

    private final RecommendationLetterRepository recommendationLetterRepository;

    private final ReferencesRepository referencesRepository;

    private final SpecialFactorRepository specialFactorRepository;

    private final WorkSampleRepository workSampleRepository;

    private final CompanyCultureRepository companyCultureRepository;

    private final JobProfileService jobProfileService;

    @Autowired
    public PowerInterviewServiceImpl(CandidateService candidateService, GoogleMapsService mapsService, PowerInterviewRepository repository, AchievementsRepository achievementsRepository, SpecialFactorRepository specialFactorRepository, WorkSampleRepository workSampleRepository, JobProfileService jobProfileService, AssessmentPIRepository assessmentPIRepository, CertificationsRepository certificationsRepository, LicencesRepository licencesRepository, CompanyCultureRepository companyCultureRepository, AcademicHistoryRepository academicHistoryRepository, CompensationRepository compensationRepository, MeasuringRepository measuringRepository, ReferencesRepository referencesRepository, JobAddressDetailsRepository jobAddressDetailsRepository, RecommendationLetterRepository recommendationLetterRepository, JobStabilityRepository jobStabilityRepository, LeavingReasonRepository leavingReasonRepository) {
        this.candidateService = candidateService;
        this.mapsService = mapsService;
        this.repository = repository;
        this.achievementsRepository = achievementsRepository;
        this.specialFactorRepository = specialFactorRepository;
        this.workSampleRepository = workSampleRepository;
        this.jobProfileService = jobProfileService;
        this.assessmentPIRepository = assessmentPIRepository;
        this.certificationsRepository = certificationsRepository;
        this.licencesRepository = licencesRepository;
        this.companyCultureRepository = companyCultureRepository;
        this.academicHistoryRepository = academicHistoryRepository;
        this.compensationRepository = compensationRepository;
        this.measuringRepository = measuringRepository;
        this.referencesRepository = referencesRepository;
        this.jobAddressDetailsRepository = jobAddressDetailsRepository;
        this.recommendationLetterRepository = recommendationLetterRepository;
        this.jobStabilityRepository = jobStabilityRepository;
        this.leavingReasonRepository = leavingReasonRepository;
    }

    @Override
    public PowerInterview save(PowerInterview powerInterview) {
        return repository.save(powerInterview);
    }

    @Override
    @Transactional
    public PowerInterview uploadWorkSample(WorkSample workSample, Long powerInterviewId) {
        PowerInterview powerInterview = findById(powerInterviewId);
        String directory = samplesDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, workSample.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }
        powerInterview.getWorkSamples().add(workSample);
        save(powerInterview);
        return powerInterview;
    }

    @Override
    @Transactional
    public PowerInterview uploadCertifications(Certifications certifications, Long powerInterviewId) {
        PowerInterview powerInterview = findById(powerInterviewId);
        String directory = certificationsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, certifications.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }
        powerInterview.getCertifications().add(certifications);
        save(powerInterview);
        return powerInterview;
    }

    @Override
    public PowerInterview uploadLicenses(Licenses certifications, Long powerInterviewId) {
        PowerInterview powerInterview = findById(powerInterviewId);
        String directory = licensesDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, certifications.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }
        powerInterview.getLicenses().add(certifications);
        save(powerInterview);
        return powerInterview;
    }

    @Override
    public PowerInterview uploadAcademicHistory(AcademicHistory certifications, Long powerInterviewId) {
        PowerInterview powerInterview = findById(powerInterviewId);
        String directory = academicHistoryDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, certifications.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }
        powerInterview.getAcademicHistories().add(certifications);
        save(powerInterview);
        return powerInterview;
    }

    @Override
    @Transactional
    public PowerInterview uploadAssessment(Assessment assessment, Long powerInterviewId) {
        PowerInterview powerInterview = findById(powerInterviewId);
        String directory = assessmentsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        for (MultipartFile multipartFile : assessment.getFiles()) {
            try {
                FileUtils.saveAttachment(directory, multipartFile);
            } catch (IOException ie) {
                logger.error(ie.getMessage(), ie);
            }
        }
        powerInterview.getAssessments().add(assessment);
        save(powerInterview);
        return powerInterview;
    }


    @Override
    @Transactional
    public PowerInterview uploadRecommendationLetter(RecommendationLetter letter, Long powerInterviewId) {
        PowerInterview powerInterview = findById(powerInterviewId);
        String directory = lettersDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        try {
            FileUtils.saveAttachment(directory, letter.getFile());
        } catch (IOException ie) {
            logger.error(ie.getMessage(), ie);
        }
        powerInterview.getRecommendationLetters().add(letter);
        save(powerInterview);
        return powerInterview;
    }


    @Override
    @Transactional
    public PowerInterview uploadSpecialFactor(SpecialFactor specialFactor, Long powerInterviewId) {
        PowerInterview powerInterview = findById(powerInterviewId);
        String directory = factorsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        for (MultipartFile multipartFile : specialFactor.getFiles()) {
            try {
                FileUtils.saveAttachment(directory, multipartFile);
            } catch (IOException ie) {
                logger.error(ie.getMessage(), ie);
            }
        }

        powerInterview.getSpecialFactors().add(specialFactor);
        save(powerInterview);
        return powerInterview;
    }


    @Override
    public PowerInterview update(PowerInterview powerInterview) {
        Candidate candidate = candidateService.findById(powerInterview.getCandidateId());
        List<JobAddressDetails> addressDetails = powerInterview.getAddressDetails();
        String originAddress = candidate.getStreet() + " " + candidate.getBuildingNumber() + ", " + candidate.getCity();
        List<TimeOnTheWay> timeOnTheWays = new ArrayList<>();
        for (JobAddressDetails addressDetail : addressDetails) {
            String targetAddress = addressDetail.getStreetName() + " " + addressDetail.getBuildingNumber() + ", " + addressDetail.getCity();
            List<TimeOnTheWay> timeDistances = mapsService.calculateTripData(originAddress, targetAddress, CandidateUtils.getInstance().findNextMondayAtTime(addressDetail.getStartTime()), "job");
            timeOnTheWays.forEach(way -> {
                way.setAddressId(addressDetail.getId());
                if (way.getMode().equals("driving")) {
                    addressDetail.setToWork(way.getDurationValue() / 60);
                }
            });
            timeOnTheWays.addAll(timeDistances);
        }
        for (JobAddressDetails addressDetail : addressDetails) {
            String targetAddress = addressDetail.getStreetName() + " " + addressDetail.getBuildingNumber() + ", " + addressDetail.getCity();
            List<TimeOnTheWay> timeDistances = mapsService.calculateTripData(targetAddress, originAddress, CandidateUtils.getInstance().findNextMondayAtTime(addressDetail.getEndTime()), "home");
            timeOnTheWays.forEach(way -> way.setAddressId(addressDetail.getId()));
            timeOnTheWays.forEach(way -> {
                way.setAddressId(addressDetail.getId());
                if (way.getMode().equals("driving")) {
                    addressDetail.setToHome(way.getDurationValue() / 60);
                }
            });
            timeOnTheWays.addAll(timeDistances);
        }

        powerInterview.setTimeOnTheWay(timeOnTheWays);
        return repository.save(powerInterview);
    }

    @Override
    @Transactional
    public PowerInterview findById(Long id) {
        PowerInterview interview = repository.findOne(id);
        interview.getAddressDetails().forEach(System.out::print);
        interview.getAchievements().forEach(System.out::print);
        interview.getAssessments().forEach(System.out::print);
        interview.getCertifications().forEach(System.out::print);
        interview.getCompanyCulture().forEach(System.out::print);
        interview.getCompensations().forEach(System.out::print);
        interview.getJobStability().forEach(System.out::print);
        interview.getLeavingReasons().forEach(System.out::print);
        interview.getMeasuring().forEach(System.out::print);
        interview.getRecommendationLetters().forEach(System.out::print);
        interview.getSpecialFactors().forEach(System.out::print);
        interview.getTimeOnTheWay().forEach(System.out::print);
        interview.getWorkSamples().forEach(System.out::print);
        interview.getReferences().forEach(System.out::print);
        interview.getLicenses().forEach(System.out::print);
        interview.getAcademicHistories().forEach(System.out::print);
        return interview;
    }

    @Override
    @Transactional
    public List<PowerInterview> findByCandidateId(Long candidateId, User user) {
        List<PowerInterview> powerInterviewList = repository.findByCandidateId(candidateId);
        List<PowerInterview> powerInterviewListFiltered = new ArrayList<>();
        for (PowerInterview aPowerInterviewList : powerInterviewList) {

            aPowerInterviewList.getAddressDetails().forEach(System.out::print);
            aPowerInterviewList.getAchievements().forEach(System.out::print);
            aPowerInterviewList.getAssessments().forEach(System.out::print);
            aPowerInterviewList.getCertifications().forEach(System.out::print);
            aPowerInterviewList.getCompanyCulture().forEach(System.out::print);
            aPowerInterviewList.getCompensations().forEach(System.out::print);
            aPowerInterviewList.getJobStability().forEach(System.out::print);
            aPowerInterviewList.getLeavingReasons().forEach(System.out::print);
            aPowerInterviewList.getMeasuring().forEach(System.out::print);
            aPowerInterviewList.getRecommendationLetters().forEach(System.out::print);
            aPowerInterviewList.getSpecialFactors().forEach(System.out::print);
            aPowerInterviewList.getTimeOnTheWay().forEach(System.out::print);
            aPowerInterviewList.getWorkSamples().forEach(System.out::print);
            aPowerInterviewList.getReferences().forEach(System.out::print);
            aPowerInterviewList.getLicenses().forEach(System.out::print);
            aPowerInterviewList.getAcademicHistories().forEach(System.out::print);

            try {
                if (!isManager(user)) {
                    powerInterviewListFiltered.add(aPowerInterviewList);
                } else {
                    final Long departmentId = jobProfileService.
                            findById(aPowerInterviewList
                                    .getJobProfileID())
                            .getDepartment().getId();

                    if (isUserHasDepartment(user, departmentId)) {
                        powerInterviewListFiltered.add(aPowerInterviewList);
                    }
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                e.printStackTrace();
            }
        }

        return powerInterviewListFiltered;
    }

    private boolean isUserHasDepartment(User loggedUser, Long departmentId) {
        if (!isManager(loggedUser)) {
            return true;
        }
        boolean hasAccess = false;
        for (CompanyDepartment companyDepartment : loggedUser.getDepartments()) {
            if (companyDepartment.getId().longValue() == departmentId.longValue())
                hasAccess = true;
        }
        return hasAccess;
    }

    @Override
    public void delete(PowerInterview powerInterview) {
        repository.delete(powerInterview);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public void updateAchievements(Achievements achievements) {
        achievementsRepository.save(achievements);
    }

    @Override
    public void updateCompanyCulture(CompanyCulture companyCulture) {
        companyCultureRepository.save(companyCulture);
    }

    @Override
    public void updateAssessment(Assessment assessment, Long interviewId) {
        PowerInterview powerInterview = findById(interviewId);
        String directory = assessmentsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        MultipartFile[] files = assessment.getFiles();
        saveFiles(directory, files);
        assessmentPIRepository.save(assessment);
    }

    private void saveFiles(String directory, MultipartFile[] files) {
        if (files != null && files.length > 0) {
            for (MultipartFile multipartFile : files) {
                try {
                    FileUtils.saveAttachment(directory, multipartFile);
                } catch (IOException ie) {
                    logger.error(ie.getMessage(), ie);
                }
            }
        }
    }

    @Override
    public void updateCertification(Certifications certifications, Long interviewId) {
        PowerInterview powerInterview = findById(interviewId);
        String directory = certificationsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        if (certifications.getFile() != null && !certifications.getFile().isEmpty()) {
            try {
                FileUtils.saveAttachment(directory, certifications.getFile());
            } catch (IOException ie) {
                logger.error(ie.getMessage(), ie);
            }
        }
        certificationsRepository.save(certifications);
    }

    @Override
    public void updateLicenses(Licenses certifications, Long interviewId) {
        PowerInterview powerInterview = findById(interviewId);
        String directory = licensesDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        if (certifications.getFile() != null && !certifications.getFile().isEmpty()) {
            try {
                FileUtils.saveAttachment(directory, certifications.getFile());
            } catch (IOException ie) {
                logger.error(ie.getMessage(), ie);
            }
        }
        licencesRepository.save(certifications);
    }

    @Override
    public void updateAcademicHistory(AcademicHistory certifications, Long interviewId) {
        PowerInterview powerInterview = findById(interviewId);
        String directory = academicHistoryDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        if (certifications.getFile() != null && !certifications.getFile().isEmpty()) {
            try {
                FileUtils.saveAttachment(directory, certifications.getFile());
            } catch (IOException ie) {
                logger.error(ie.getMessage(), ie);
            }
        }
        academicHistoryRepository.save(certifications);
    }

    @Override
    public void updateCompensation(Compensation compensation) {
        compensationRepository.save(compensation);
    }

    @Override
    public void updateJobAddressDetails(JobAddressDetails jobAddressDetails) {
        jobAddressDetailsRepository.save(jobAddressDetails);
    }

    @Override
    public void updateJobStability(JobStability jobStability) {
        jobStabilityRepository.save(jobStability);
    }

    @Override
    public void updateLeavingReason(LeavingReason leavingReason) {
        leavingReasonRepository.save(leavingReason);
    }

    @Override
    public void updateMeasuring(Measuring measuring) {
        measuringRepository.save(measuring);
    }

    @Override
    public void updateRecommendationLetter(RecommendationLetter recommendationLetter, Long interviewId) {
        PowerInterview powerInterview = findById(interviewId);
        String directory = lettersDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        if (recommendationLetter.getFile() != null && !recommendationLetter.getFile().isEmpty()) {
            try {
                FileUtils.saveAttachment(directory, recommendationLetter.getFile());
            } catch (IOException ie) {
                logger.error(ie.getMessage(), ie);
            }
        }
        recommendationLetterRepository.save(recommendationLetter);
    }

    @Override
    public void updateReferences(References references) {
        referencesRepository.save(references);
    }

    @Override
    public void updateSpecialFactors(SpecialFactor specialFactor, Long interviewId) {
        PowerInterview powerInterview = findById(interviewId);
        MultipartFile[] files = specialFactor.getFiles();
        String directory = factorsDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        saveFiles(directory, files);
        specialFactorRepository.save(specialFactor);
    }

    @Override
    public void updateWorkSample(WorkSample workSample, Long interviewId) {
        PowerInterview powerInterview = findById(interviewId);
        String directory = samplesDirectory + powerInterview.getCandidateId() + "/" + powerInterview.getId() + "/";
        if (workSample.getFile() != null && !workSample.getFile().isEmpty()) {
            try {
                FileUtils.saveAttachment(directory, workSample.getFile());
            } catch (IOException ie) {
                logger.error(ie.getMessage(), ie);
            }
        }
        workSampleRepository.save(workSample);
    }
}
