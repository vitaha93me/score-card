package com.introlabgroup.scorecard.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabgroup.scorecard.configuration.CustomUserDetailsService;
import com.introlabgroup.scorecard.model.MailTemplate;
import com.introlabgroup.scorecard.model.User;
import com.sendgrid.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author vitalii.
 */
@Service
public class SendGridService {

    private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);


    @Value("${sendgrid.api.key}")
    private String SENDGRID_API_KEY;

    private SendGrid sendGrid;

    @Value("${sendgrid.sender.email}")
    private String senderEmail;

    private void sendEmail(String fromAddress, String toAddress, String subject, String body) {
        Email from = new Email(fromAddress);
        Email to = new Email(toAddress);
        Content content = new Content("text/html", body);
        Mail mail = new Mail(from, subject, to, content);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
            logger.info(String.valueOf(response.getStatusCode()));
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    void sendCredentials(User user, String invitePerson, String templateId) {
        MailTemplate mailTemplate = getMailTemplate(templateId);
        String body;
        if (mailTemplate != null) {
            body = mailTemplate.getBody()
                    .replace("{firstName}", user.getName())
                    .replace("{company}", user.getCompany().getName())
                    .replace("{password}", user.getPassword())
                    .replace("{invitePerson}", invitePerson);
            sendEmail(senderEmail, user.getEmail(), mailTemplate.getSubject(), body);
        }

    }

    private MailTemplate getMailTemplate(String templateId) {

        Request request = new Request();
        request.setMethod(Method.GET);
        request.setEndpoint("templates/" + templateId);
        Response response;
        try {
            sendGrid = new SendGrid(SENDGRID_API_KEY);
            response = sendGrid.api(request);
            JsonObject jsonTemplate = new JsonParser().parse(response.getBody()).getAsJsonObject().getAsJsonArray("versions").get(0).getAsJsonObject();
            String subject = jsonTemplate.getAsJsonPrimitive("subject").getAsString();
            String body = jsonTemplate.getAsJsonPrimitive("html_content").getAsString();
            MailTemplate mailTemplate = new MailTemplate();
            mailTemplate.setBody(body);
            mailTemplate.setSubject(subject);
            return mailTemplate;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
