package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.payment.StripeData;
import com.introlabgroup.scorecard.model.payment.StripeEvent;
import com.introlabgroup.scorecard.model.payment.StripeSubscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author vitalii.
 */
@Component
public class StripeDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StripeDao(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public StripeSubscription getStripeSubscriptionByCompanyId(Long companyId) {
        return jdbcTemplate.queryForObject("SELECT * FROM stripe_subscriptions WHERE companyId = ?", new BeanPropertyRowMapper<>(StripeSubscription.class), companyId);
    }

    public List<StripeSubscription> getStripeActiveSubscriptionsByCompanyId(Long companyId) {
        return jdbcTemplate.query("SELECT * FROM stripe_subscriptions WHERE companyId = ? and  currentStatus=true", new BeanPropertyRowMapper<>(StripeSubscription.class), companyId);
    }

    public List<StripeSubscription> getStripeDeActivatedSubscriptionsByCompanyId(Long companyId) {
        return jdbcTemplate.query("SELECT * FROM stripe_subscriptions WHERE companyId = ? and  currentStatus=false ", new BeanPropertyRowMapper<>(StripeSubscription.class), companyId);
    }

    public List<StripeSubscription> getStripeAllSubscriptionsByCompanyId(Long companyId) {
        return jdbcTemplate.query("SELECT * FROM stripe_subscriptions WHERE companyId = ?", new BeanPropertyRowMapper<>(StripeSubscription.class), companyId);
    }

    public void decreaseScorecardCount(Long companyId) {
        jdbcTemplate.update("update stripe_subscriptions set scorecardsLeft = (scorecardsLeft-1) where companyId = ?", companyId);
    }

    public void resetScorecardCount(Long companyId) {
        jdbcTemplate.update("update stripe_subscriptions set scorecardsLeft=scorecardCount where companyId = ?", companyId);
    }

    public StripeData getStripeDataByCompanyId(Long companyId) {
        return jdbcTemplate.queryForObject("SELECT * FROM stripe_data WHERE companyId = ?", new BeanPropertyRowMapper<>(StripeData.class), companyId);
    }

    public StripeData getStripeDataByStripeId(String companyId) {
        return jdbcTemplate.queryForObject("SELECT * FROM stripe_data WHERE stripeId = ?", new BeanPropertyRowMapper<>(StripeData.class), companyId);
    }

    public void insertStripeData(StripeData stripeData) {
        jdbcTemplate.update("insert into stripe_data(companyId, stripeId) VALUES (?,?)", stripeData.getCompanyId(), stripeData.getStripeId());
    }

    public void insertStripeStripeSubscription(StripeSubscription subscription) {
        jdbcTemplate.update("insert into stripe_subscriptions(companyId, subscriptionId, type, email, price, scorecardCount, scorecardsLeft, currentStatus) VALUES (?,?,?,?,?,?,?,?)",
                subscription.getCompanyId(), subscription.getSubscriptionId(), subscription.getType(), subscription.getEmail(), subscription.getPrice(), subscription.getScoreCardCount(), subscription.getScorecardsLeft(), subscription.getCurrentStatus());
    }

    public void updateStripeStripeSubscription(StripeSubscription subscription) {
        jdbcTemplate.update("update stripe_subscriptions set subscriptionId=?, type=?, email=?, price=?, scorecardCount=?, scorecardsLeft=?, currentStatus=? where companyId=?",
                subscription.getSubscriptionId(), subscription.getType(), subscription.getEmail(), subscription.getPrice(), subscription.getScoreCardCount(), subscription.getScorecardsLeft(), subscription.getCurrentStatus(), subscription.getCompanyId());
    }

    public void updateLastPaymentDate(String subscriptionId) {
        jdbcTemplate.update("update stripe_subscriptions set lastPayment=now() where subscriptionId=?", subscriptionId);
    }

    public List<StripeEvent> getStripeEvents(String companyId) {
        return jdbcTemplate.query("SELECT * FROM stripe_history WHERE companyId = ?", new BeanPropertyRowMapper<>(StripeEvent.class), companyId);
    }

    public void insertStripeEvent(StripeEvent stripeData) {
        jdbcTemplate.update("insert into stripe_history(companyId, companyName, subscriptionId, status, created) VALUES (?,?,?,?,now())",
                stripeData.getCompanyId(), stripeData.getCompanyName(), stripeData.getSubscriptionId(), stripeData.isStatus());
    }

    boolean isPeriodFinished(Long companyId, String interval) {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM stripe_subscriptions WHERE currentStatus=false and companyId = ? and lastPayment < date_sub(now(), interval 1 " + interval + ")", Integer.class, companyId) > 0;
    }
}
