package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.onet.PaymentPeriod;
import com.introlabgroup.scorecard.model.payment.StripeSubscription;
import com.introlabgroup.scorecard.model.payment.SubscriptionDTO;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Getter
public class StripeService {

    private static final Logger logger = LoggerFactory.getLogger(StripeService.class);

    @Value("${stripe.api.key}")
    private String stripeApiKey;
    @Value("${stripe.product.id}")
    private String productId;
    //private String stripeApiKey = "sk_live_EpZgF9tbgzLLEfuputrPwWil";
    //private String productId = "prod_DddXFDT8hk6BET";
    private int pricePerCard = 2500;

    private final StripeDao stripeDao;

    @Autowired
    public StripeService(StripeDao stripeDao) {
        this.stripeDao = stripeDao;
    }

    //    public void calculatePrice(SubscriptionDTO subscription) {
//        subscription.setPrice(100);
//    }

    public void calculatePrice(SubscriptionDTO subscription) {
        if (subscription.getPaymentPeriod() == PaymentPeriod.ANNUALLY) {
            subscription.setPrice(subscription.getScorecardCount() * pricePerCard - ((subscription.getScorecardCount() * pricePerCard) * 2 / 10));
        } else if ((subscription.getPaymentPeriod() == PaymentPeriod.MONTHLY)) {
            subscription.setPrice(subscription.getScorecardCount() * pricePerCard);
        } else {
            subscription.setPrice(100);
        }
    }


    public Plan createTariffPlan(SubscriptionDTO subscription) throws StripeException {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> params = new HashMap<>();
        String period;
        period = fetchPaymentInterval(subscription);
        params.put("product", productId);
        params.put("nickname", "Scorecard for " + subscription.getCompanyName());
        params.put("interval", period);
        params.put("currency", "usd");
        params.put("amount", subscription.getPrice());
        Plan plan = Plan.create(params);
        logger.info(plan.toJson());
        return plan;
    }

    private String fetchPaymentInterval(SubscriptionDTO subscription) {
        String period;
        if (subscription.getPaymentPeriod() == PaymentPeriod.DAILY) {
            period = "day";
        } else if (subscription.getPaymentPeriod() == PaymentPeriod.ANNUALLY) {
            period = "year";
        } else {
            period = "month";
        }
        return period;
    }


    public void cancelSubscription(String subscriptionId) throws StripeException {
        if (subscriptionId == null || subscriptionId.isEmpty()) {
            return;
        }
        Stripe.apiKey = stripeApiKey;
        Subscription subscription = Subscription.retrieve(subscriptionId);
        subscription.cancel(new HashMap<>());
    }

    public Subscription subscribeCustomer(Plan plan, Customer customer) throws StripeException {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> item = new HashMap<>();
        item.put("plan", plan.getId());
        Map<String, Object> items = new HashMap<>();
        items.put("0", item);
        Map<String, Object> params = new HashMap<>();
        params.put("customer", customer.getId());
        params.put("items", items);
        Subscription subscription = Subscription.create(params);
        System.out.println(subscription);
        return subscription;
    }

    public Subscription updateSubscription(StripeSubscription stripeSubscription, SubscriptionDTO subscriptionDTO) throws StripeException {
        Stripe.apiKey = stripeApiKey;

        Subscription subscription = Subscription.retrieve(stripeSubscription.getSubscriptionId());

        Plan tariffPlan = createTariffPlan(subscriptionDTO);

        Map<String, Object> item = new HashMap<>();
        item.put("id", subscription.getSubscriptionItems().getData().get(0).getId());
        item.put("plan", tariffPlan.getId());

        Map<String, Object> items = new HashMap<>();
        items.put("0", item);

        Map<String, Object> params = new HashMap<>();
        params.put("cancel_at_period_end", false);
        params.put("items", items);

        subscription = subscription.update(params);

        return subscription;
    }


    public void testApiCall() {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> params = new HashMap<>();
        params.put("amount", 999);
        params.put("currency", "usd");
        params.put("source", "tok_visa");
        params.put("receipt_email", "vitaha.me@gmail.com");
        try {
            Charge charge = Charge.create(params);
            System.out.println(charge.toJson());
        } catch (StripeException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void createServiceAsProduct() {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> params = new HashMap<>();
        params.put("name", "Candidate scorecard");
        params.put("type", "service");

        try {
            Product product = Product.create(params);
            System.out.println(product.toJson());
        } catch (StripeException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public String createTariffPlan() throws StripeException {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> params = new HashMap<>();
        params.put("product", productId);
        params.put("nickname", "my test plan for 100 USD");
        params.put("interval", "day");
        params.put("currency", "usd");
        params.put("amount", 10000);
        Plan plan = Plan.create(params);
        System.out.println(plan.toJson());
        return plan.getId();
    }

    public void subscribeCustomer() {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> item = new HashMap<>();
        item.put("plan", "plan_DVPqwiLdMWoKuY");
        Map<String, Object> items = new HashMap<>();
        items.put("0", item);
        Map<String, Object> params = new HashMap<>();
        params.put("customer", "cus_DVQNWShrBnkv4D");
        params.put("items", items);
        try {
            Subscription subscription = Subscription.create(params);
            System.out.println(subscription);
        } catch (StripeException e) {
            e.printStackTrace();
        }
    }

    public void createCustomer() {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> params = new HashMap<>();
        params.put("email", "vitaha.me@gmail.com");
        params.put("source", "tok_mastercard");
        try {
            Customer customer = Customer.create(params);
            System.out.println(customer.toJson());
        } catch (StripeException e) {
            e.printStackTrace();
        }
    }

    boolean isPeriodFinished(StripeSubscription subscription) {
        if (subscription.getType().equals(PaymentPeriod.MONTHLY.getValue())) {
            return stripeDao.isPeriodFinished(subscription.getCompanyId(), "month");
        } else if (subscription.getType().equals(PaymentPeriod.DAILY.getValue())) {
            return stripeDao.isPeriodFinished(subscription.getCompanyId(), "day");
        } else {
            return stripeDao.isPeriodFinished(subscription.getCompanyId(), "year");
        }
    }

    public Customer createCustomer(SubscriptionDTO subscription) throws StripeException {
        Stripe.apiKey = stripeApiKey;

        Map<String, Object> params = new HashMap<>();
        params.put("email", subscription.getEmail());
        params.put("source", subscription.getCardToken());
        params.put("description", subscription.getCompanyName());
        Customer customer = Customer.create(params);
        System.out.println(customer.toJson());
        return customer;
    }
}
