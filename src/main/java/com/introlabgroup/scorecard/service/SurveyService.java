package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.Assessment;
import com.introlabgroup.scorecard.model.JobProfile;
import com.introlabgroup.scorecard.model.LeadingQuestion;
import com.introlabgroup.scorecard.model.onet.*;
import com.introlabgroup.scorecard.repository.AssessmentRepository;
import com.introlabgroup.scorecard.repository.onet.OccupationRepository;
import com.introlabgroup.scorecard.repository.onet.OnetItemRepository;
import com.introlabgroup.scorecard.utils.CandidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author vitalii.
 */

@Service
public class SurveyService {

    private final AssessmentRepository assessmentRepository;

    private final OccupationRepository occupationRepository;

    private final OnetItemRepository onetItemRepository;

    @Autowired
    public SurveyService(AssessmentRepository assessmentRepository, OccupationRepository occupationRepository, OnetItemRepository onetItemRepository) {
        this.assessmentRepository = assessmentRepository;
        this.occupationRepository = occupationRepository;
        this.onetItemRepository = onetItemRepository;
    }

    public List<Occupation> searchOccupation(String keyword) {
        List<Occupation> occupations = occupationRepository.findByTitleStartsWith(keyword);
        return occupations.stream().map(occupation -> {
            Occupation occupationDto = new Occupation();
            occupationDto.setTitle(occupation.getTitle());
            occupationDto.setOnetsoc_code(occupation.getOnetsoc_code());
            return occupationDto;
        }).collect(Collectors.toList());
    }

    public Set<Occupation> findSimilar(String keyword) {


        Set<Occupation> occupations = occupationRepository.findByTitleContainingIgnoreCase(keyword);
        if (occupations == null) {
            occupations = new TreeSet<>();
        }
        List<String> keys = Stream.of(keyword.split(" "))
                .filter(key -> !key.equals("and"))
                .filter(key -> !key.equals("&"))
                .filter(key -> !key.equals("of"))
                .filter(key -> !key.equals("with"))
                .filter(key -> !key.equals("the"))
                .filter(key -> !key.equals("and"))
                .map(key -> {
                    key = key.replace(",", "");
                    key = key.replace(".", "");
                    key = key.replace("!", "");
                    key = key.replace(";", "");
                    return key;
                })
                .collect(Collectors.toList());


        for (String key : keys) {
            occupations.addAll(occupationRepository.findByTitleContainingIgnoreCase(key));
        }


        return occupations;
    }

    @Transactional
    public void deleteAssessmentsByJobProfile(JobProfile jobProfile) {
        assessmentRepository.deleteAssessmentByJobProfile(jobProfile);
    }

    @Transactional
    public void deleteAssessmentsByJobProfileId(JobProfile jobProfile) {
        assessmentRepository.deleteAssessmentsByJobProfileId(jobProfile.getId());
    }

    @Transactional
    public void deleteAssessment(Long id) {
        assessmentRepository.delete(id);
    }

    @Transactional
    public Occupation findOneOccupation(String keyword) {
        Occupation occupation = occupationRepository.findOne(keyword);
        occupation.getCustomQualities().size();
        occupation.getSkills().size();
        occupation.getInterests().size();
        occupation.getWorkActivities().size();
        occupation.getWorkStyles().size();
        occupation.getWorkValues().size();
        occupation.getKnowledge().size();
        occupation.getAbilities().size();
        return occupation;
    }

    public void assignQuality(Occupation occupation, String quality) {

        List<OnetItem> onetItems = onetItemRepository.findByElementNameIgnoreCase(quality);
        if (!onetItems.isEmpty()) {

            for (OnetItem onetItem : onetItems) {
                if (onetItem.getElementId().startsWith("2.A") || onetItem.getElementId().startsWith("2.B")) {
                    occupation.getSkills().add(Skills.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                } else if (onetItem.getElementId().startsWith("1.A")) {
                    occupation.getAbilities().add(Abilities.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                } else if (onetItem.getElementId().startsWith("2.B.2")) {
                    occupation.getWorkValues().add(WorkValues.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                } else if (onetItem.getElementId().startsWith("1.C")) {
                    occupation.getWorkStyles().add(WorkStyles.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                } else if (onetItem.getElementId().startsWith("4.A")) {
                    occupation.getWorkActivities().add(WorkActivities.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                } else if (onetItem.getElementId().startsWith("2.C")) {
                    occupation.getKnowledge().add(Knowledge.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                } else if (onetItem.getElementId().startsWith("1.B.1")) {
                    occupation.getInterests().add(Interests.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                } else {
                    occupation.getCustomQualities().add(CustomQuality.fromOnetItem(onetItem, occupation.getOnetsoc_code()));
                }
            }
        } else {
            OnetItem onetItem = new OnetItem();
            onetItem.setCustom(true);
            onetItem.setDescription("");
            onetItem.setElementName(quality);
            onetItem.setElementId(CandidateUtils.getInstance().generateCustomId(quality));
            CustomQuality customQuality = new CustomQuality();
            customQuality.setOnetItem(onetItem);
            customQuality.setOccupation(occupation.getOnetsoc_code());
            customQuality.setWeight(4D);
            customQuality.setPriorityRanking(PriorityRanking.MUST_HAVE);
            occupation.getCustomQualities().add(customQuality);
        }

    }

    public void saveAssessment(Assessment assessment) {
        assessmentRepository.save(assessment);
    }

    private void updateQualityWeight(List<? extends QualityCategory> qualities, String id, double weight) {
        for (QualityCategory knowledge : qualities) {
            if (knowledge.getOnetItem().getElementId().equals(id)) {
                knowledge.setWeight(weight);
            }
        }
    }

    public void updateQualityWeight(Occupation occupation, String id, double weight) {
        updateQualityWeight(occupation.getAbilities(), id, weight);
        updateQualityWeight(occupation.getKnowledge(), id, weight);
        updateQualityWeight(occupation.getWorkValues(), id, weight);
        updateQualityWeight(occupation.getWorkStyles(), id, weight);
        updateQualityWeight(occupation.getWorkActivities(), id, weight);
        updateQualityWeight(occupation.getInterests(), id, weight);
        updateQualityWeight(occupation.getSkills(), id, weight);
        updateQualityWeight(occupation.getCustomQualities(), id, weight);
    }

    private void updatePriorityRanking(List<? extends QualityCategory> qualities, String id, PriorityRanking priorityRanking) {
        for (QualityCategory knowledge : qualities) {
            if (knowledge.getOnetItem().getElementId().equals(id)) {
                knowledge.setPriorityRanking(priorityRanking);
            }
        }
    }


    private void updateRequirements(List<? extends QualityCategory> qualities, String id) {
        for (QualityCategory knowledge : qualities) {
            if (knowledge.getOnetItem().getElementId().equals(id)) {
                knowledge.setRequired(!knowledge.isRequired());
            }
        }
    }

    public void updatePriorityRanking(Occupation occupation, String id, PriorityRanking priorityRanking) {
        updatePriorityRanking(occupation.getAbilities(), id, priorityRanking);
        updatePriorityRanking(occupation.getKnowledge(), id, priorityRanking);
        updatePriorityRanking(occupation.getWorkValues(), id, priorityRanking);
        updatePriorityRanking(occupation.getWorkStyles(), id, priorityRanking);
        updatePriorityRanking(occupation.getWorkActivities(), id, priorityRanking);
        updatePriorityRanking(occupation.getInterests(), id, priorityRanking);
        updatePriorityRanking(occupation.getSkills(), id, priorityRanking);
        updatePriorityRanking(occupation.getCustomQualities(), id, priorityRanking);
    }

    public void updateRequirements(Occupation occupation, String id) {
        updateRequirements(occupation.getAbilities(), id);
        updateRequirements(occupation.getKnowledge(), id);
        updateRequirements(occupation.getWorkValues(), id);
        updateRequirements(occupation.getWorkStyles(), id);
        updateRequirements(occupation.getWorkActivities(), id);
        updateRequirements(occupation.getInterests(), id);
        updateRequirements(occupation.getSkills(), id);
        updateRequirements(occupation.getCustomQualities(), id);
    }


    public void addQualityQuestion(Occupation occupation, String id, LeadingQuestion question) {
        addLeadingQuestion(occupation.getAbilities(), id, question);
        addLeadingQuestion(occupation.getKnowledge(), id, question);
        addLeadingQuestion(occupation.getWorkValues(), id, question);
        addLeadingQuestion(occupation.getWorkStyles(), id, question);
        addLeadingQuestion(occupation.getWorkActivities(), id, question);
        addLeadingQuestion(occupation.getInterests(), id, question);
        addLeadingQuestion(occupation.getSkills(), id, question);
        addLeadingQuestion(occupation.getCustomQualities(), id, question);
    }

    private void addLeadingQuestion(List<? extends QualityCategory> qualities, String id, LeadingQuestion question) {
        for (QualityCategory knowledge : qualities) {
            if (knowledge.getOnetItem().getElementId().equals(id)) {
                knowledge.getOnetItem().getLeadingQuestions().add(question);
            }
        }
    }

    private List<LeadingQuestion> getLeadingQuestions(List<? extends QualityCategory> qualities) {
        List<LeadingQuestion> leadingQuestions = new ArrayList<>();
        for (QualityCategory knowledge : qualities) {
            leadingQuestions.addAll(knowledge.getOnetItem().getLeadingQuestions());
        }
        return leadingQuestions;
    }

    public List<LeadingQuestion> getLeadingQuestions(Occupation occupation) {
        List<LeadingQuestion> leadingQuestions = new ArrayList<>();
        leadingQuestions.addAll(getLeadingQuestions(occupation.getAbilities()));
        leadingQuestions.addAll(getLeadingQuestions(occupation.getKnowledge()));
        leadingQuestions.addAll(getLeadingQuestions(occupation.getWorkValues()));
        leadingQuestions.addAll(getLeadingQuestions(occupation.getWorkStyles()));
        leadingQuestions.addAll(getLeadingQuestions(occupation.getWorkActivities()));
        leadingQuestions.addAll(getLeadingQuestions(occupation.getInterests()));
        leadingQuestions.addAll(getLeadingQuestions(occupation.getSkills()));
        leadingQuestions.addAll(getLeadingQuestions(occupation.getCustomQualities()));
        return leadingQuestions;
    }

}