package com.introlabgroup.scorecard.service;


import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.User;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {
    User findUserByEmail(String email);

    List<User> findUsersByEmail(String email);

    User findUserById(Long id);

    List<User> findAllAdmins();

    List<User> findAllMasterAdmins();

    void remove(Long id);

    void remove(User user);

    User findUserByEmailAndPassword(String email, String password);

    void saveUser(User user);

    void createUser(User user, String role);

    List<User> findManagersByCompany(Company company);

    Page<User> findManagersByCompany(Company company, String keyword, int page, int size, String direction, String column);

    Page<User> findManagersAndAdminsByCompany(Company company, String keyword, int page, int size, String direction, String column);

    List<User> findAdminsByCompany(Company company);

    List<User> findAllByCompany(Company company);

}
