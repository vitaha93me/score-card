package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.Role;
import com.introlabgroup.scorecard.model.User;
import com.introlabgroup.scorecard.repository.RoleRepository;
import com.introlabgroup.scorecard.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> findUsersByEmail(String email) {
        return userRepository.findUsersByEmail(email);
    }

    @Override
    public List<User> findAllAdmins() {
        return userRepository.findAll()
                .stream()
                .filter(user -> {
                            for (Role role : user.getRoles()) {
                                return role.getRole().equals("ROLE_ADMIN");
                            }
                            return false;
                        }
                ).collect(Collectors.toList());
    }

    @Override
    public List<User> findAllMasterAdmins() {
        return userRepository.findAll()
                .stream()
                .filter(user -> {
                            for (Role role : user.getRoles()) {
                                return role.getRole().equals("ROLE_SUPER_ADMIN");
                            }
                            return false;
                        }
                ).collect(Collectors.toList());
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    @Transactional
    public void remove(Long id) {
        userRepository.delete(id);
    }

    @Override
    @Transactional
    public void remove(User user) {
        userRepository.delete(user);
    }

    @Override
    public User findUserByEmailAndPassword(String email, String password) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return null;
        }
        if (bCryptPasswordEncoder.matches(password, user.getPassword())) {
            return user;
        }
        return null;
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void createUser(User user, String role) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);
        Role userRole = roleRepository.findByRole(role);
        user.setRoles(new HashSet<>(Collections.singletonList(userRole)));
        userRepository.save(user);
    }

    @Override
    public List<User> findManagersByCompany(Company company) {
        return userRepository.findByCompany(company)
                .stream()
                .filter(user -> {
                            for (Role role : user.getRoles()) {
                                return role.getRole().equals("ROLE_USER");
                            }
                            return false;
                        }
                ).collect(Collectors.toList());
    }

    @Override
    public Page<User> findManagersByCompany(Company company, String keyword, int page, int size, String direction, String column) {
        PageRequest pageable = new PageRequest(page, size, direction.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, column);
        Role role_user = roleRepository.findByRole("ROLE_USER");
        return userRepository.findByCompany(company, keyword, Collections.singleton(role_user) , pageable);
    }


    @Override
    public Page<User> findManagersAndAdminsByCompany(Company company, String keyword, int page, int size, String direction, String column) {
        PageRequest pageable = new PageRequest(page, size, direction.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, column);
        List<Role> roleSet = new ArrayList<>();
        roleSet.add(roleRepository.findByRole("ROLE_USER"));
        roleSet.add(roleRepository.findByRole("ROLE_ADMIN"));
        return userRepository.findByCompany(company, keyword, roleSet, pageable);
    }

    @Override
    public List<User> findAdminsByCompany(Company company) {
        return userRepository.findByCompany(company)
                .stream()
                .filter(user -> {
                            for (Role role : user.getRoles()) {
                                return role.getRole().equals("ROLE_ADMIN");
                            }
                            return false;
                        }
                ).collect(Collectors.toList());
    }

    @Override
    public List<User> findAllByCompany(Company company) {
        return userRepository.findByCompany(company);
    }

}
