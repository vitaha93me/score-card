package com.introlabgroup.scorecard.service.cats;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.introlabgroup.scorecard.model.Candidate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * @author vitalii.
 */
@Service
public class CatsService {

    @Value("${cats.api.key}")
    private String API_KEY;

    private static final String BASE_URL = "https://api.catsone.com/v3/";
    private static final String CANDIDATE_INFO_END_POINT = "candidates/%s";

    public Candidate getCandidate(long id) throws IOException {

        final String uri = BASE_URL + String.format(CANDIDATE_INFO_END_POINT, id);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Token " + API_KEY);
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        System.out.println(result.getBody());

        ObjectMapper objectMapper = new ObjectMapper();

//read JSON like DOM Parser
        JsonNode rootNode = objectMapper.readTree(result.getBody());
        JsonNode idNode = rootNode.path("id");
        JsonNode firstName = rootNode.path("first_name");
        JsonNode lastName = rootNode.path("last_name");
        System.out.println("id = " + idNode.asInt());

        Candidate candidate = new Candidate();
        candidate.setCatsId(id);
        candidate.setName(firstName.asText());
        candidate.setLastName(lastName.asText());
        return candidate;
    }
}
