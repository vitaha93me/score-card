package com.introlabgroup.scorecard.service.sovren;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabgroup.scorecard.controller.JobParsedModel;
import com.introlabgroup.scorecard.model.Candidate;
import com.introlabgroup.scorecard.model.pi.JobStability;
import com.introlabgroup.scorecard.model.pi.PowerInterview;
import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author vitalii.
 */
@Service
public class ResumeParser {

    private static final Logger logger = LoggerFactory.getLogger(ResumeParser.class);

    @Value("${attachments.resume.directory}")
    private String resumeDirectory;

    @Value("${attachments.jobs.directory}")
    private String jobsDirectory;
    private static final String SOVREN_ACCOUNT_ID = "12872753";
    private static final String SOVREN_SERVICE_KEY = "5CeG4SmHRFIExevqzd672eTCAeG4dZ3dhpWgZHFb";
    private static final String RESUME_END_POINT = "https://rest.resumeparsing.com/v9/parser/resume";
    private static final String JOB_END_POINT = "https://rest.resumeparsing.com/v9/parser/joborder";

    public void parseResume(Candidate candidate, String path, Collection<PowerInterview> powerInterviews) throws IOException {

        if (candidate.getResumeFileName().isEmpty()) {
            return;
        }

        Path filePath = Paths.get(path);

        String response = executeRequest(filePath, RESUME_END_POINT);

        parseResponse(candidate, response, powerInterviews);
    }

    public JobParsedModel parseJobDescription(String fileName) throws IOException {

        if (fileName.isEmpty()) {
            return null;
        }

        Path filePath = Paths.get(jobsDirectory + fileName);

        String response = executeRequest(filePath, JOB_END_POINT);
        System.out.println(response);
        JsonElement jsonElement = new JsonParser().parse(response);
        String parsedString = jsonElement.getAsJsonObject().getAsJsonObject("Value").getAsJsonPrimitive("ParsedDocument").getAsString();
        jsonElement = new JsonParser().parse(parsedString);
        final String jobTitle = getJobTitle(jsonElement);

        List<String> skills = getSkills(jsonElement).stream().distinct().collect(Collectors.toList());

        System.out.println(jobTitle);
        System.out.println(skills);

        return new JobParsedModel(jobTitle, skills);

    }

    private List<String> getSkills(JsonElement jsonElement) {
        List<String> skills = new ArrayList<>();
        try {

            JsonArray taxonomyArray = jsonElement.getAsJsonObject().getAsJsonObject("SovrenData")
                    .getAsJsonArray("SkillsTaxonomyOutput")
                    .get(0).getAsJsonObject().getAsJsonArray("Taxonomy");

            for (JsonElement rootTaxonomy : taxonomyArray) {

                final JsonArray subTaxonomyArray = rootTaxonomy.getAsJsonObject().getAsJsonArray("Subtaxonomy");

                for (JsonElement element : subTaxonomyArray) {
                    final JsonArray localSkills = element.getAsJsonObject().getAsJsonArray("Skill");
                    for (JsonElement skill : localSkills) {
                        skills.add(skill.getAsJsonObject().getAsJsonPrimitive("@name").getAsString());
                        if (skill.getAsJsonObject().has("ChildSkill")) {
                            for (JsonElement childSkill : skill.getAsJsonObject().getAsJsonArray("ChildSkill")) {
                                skills.add(childSkill.getAsJsonObject().getAsJsonPrimitive("@name").getAsString());
                            }
                        }
                    }
                }
            }

            return skills;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private String getJobTitle(JsonElement jsonElement) {
        try {
            return jsonElement.getAsJsonObject().getAsJsonObject("SovrenData").getAsJsonObject("JobTitles").getAsJsonPrimitive("MainJobTitle").getAsString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private String executeRequest(Path filePath, String endPoint) throws IOException {
        byte[] encoded = Base64.getEncoder().encode(Files.readAllBytes(filePath));
        String base64Str = new String(encoded, StandardCharsets.UTF_8);

        URL url = new URL(endPoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);

        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("accept", "application/json");

        connection.setRequestProperty("Sovren-AccountId", SOVREN_ACCOUNT_ID);
        connection.setRequestProperty("Sovren-ServiceKey", SOVREN_SERVICE_KEY);

        String payload = "{ \"DocumentAsBase64String\": \"" + base64Str + "\" }";

        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(new DataOutputStream(connection.getOutputStream()), StandardCharsets.UTF_8));
        bw.write(payload);
        bw.flush();
        bw.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    private void parseResponse(Candidate candidate, String response, Collection<PowerInterview> powerInterviews) {

        JsonElement jsonElement = new JsonParser().parse(response);
        String parsedString = jsonElement.getAsJsonObject().getAsJsonObject("Value").getAsJsonPrimitive("ParsedDocument").getAsString();
        JsonObject resumeJsonObject = new JsonParser().parse(parsedString).getAsJsonObject().getAsJsonObject("Resume");
        JsonObject structuredXMLResume = resumeJsonObject.getAsJsonObject("StructuredXMLResume");
        try {
            parseAddressBlock(candidate, structuredXMLResume);
            parseNameBlock(candidate, structuredXMLResume);
            List<JobStability> jobStabilities = parseCareersBlock(structuredXMLResume);

            for (PowerInterview powerInterview : powerInterviews) {
                if (powerInterview.getJobStability().isEmpty()) {
                    powerInterview.setJobStability(jobStabilities);
                }
            }
        } catch (Exception e) {
//            throw new UnExpectedParsingError("Unable to parse resume file", e);
            logger.error(e.getMessage(), e);
        }

    }

    private void parseAddressBlock(Candidate candidate, JsonObject structuredXMLResume) {
        try {

            JsonArray contactMethods = structuredXMLResume.getAsJsonObject("ContactInfo").getAsJsonArray("ContactMethod");

            //parseResume address block
            for (JsonElement contactMethod : contactMethods) {
                if (contactMethod.toString().contains("PostalAddress")) {

                    JsonObject postalAddress = contactMethod.getAsJsonObject().getAsJsonObject("PostalAddress");

                    try {
                        String postalCode = postalAddress.getAsJsonPrimitive("PostalCode").getAsString();
                        candidate.setZip(postalCode);
                    } catch (Exception e) {
                        logger.error("Exception while parse ZIP", e);
                    }
                    try {
                        String municipality = postalAddress.getAsJsonPrimitive("Municipality").getAsString();
                        candidate.setCity(municipality);
                    } catch (Exception e) {
                        logger.error("Exception while parse City", e);
                    }
                    try {
                        String region = postalAddress.getAsJsonArray("Region").get(0).toString();
                        candidate.setState(region.replace("\"", ""));
                    } catch (Exception e) {
                        logger.error("Exception while parse Region", e);
                    }
                    try {
                        String addressLine = postalAddress.getAsJsonObject("DeliveryAddress").getAsJsonArray("AddressLine").get(0).getAsString();
                        candidate.setStreet(addressLine);
                    } catch (Exception e) {
                        logger.error("Exception while parse DeliveryAddress", e);
                    }


                } else if (contactMethod.toString().contains("FormattedNumber") && contactMethod.toString().contains("Mobile")) {
                    String phone = contactMethod.getAsJsonObject().getAsJsonObject("Mobile").getAsJsonPrimitive("FormattedNumber").getAsString();
                    candidate.setPhone(phone);
                } else if (contactMethod.toString().contains("FormattedNumber") && contactMethod.toString().contains("Telephone")) {
                    String phone = contactMethod.getAsJsonObject().getAsJsonObject("Telephone").getAsJsonPrimitive("FormattedNumber").getAsString();
                    candidate.setPhone(phone);
                } else if (contactMethod.toString().contains("InternetEmailAddress")) {
                    String email = contactMethod.getAsJsonObject().getAsJsonPrimitive("InternetEmailAddress").getAsString();
                    candidate.setEmail(email);
                }
            }
        } catch (Exception e) {
            logger.error("Exception while parse Address block", e);
        }
    }

    private void parseNameBlock(Candidate candidate, JsonObject structuredXMLResume) {
        try {
            JsonObject contactInfo = structuredXMLResume.getAsJsonObject("ContactInfo").getAsJsonObject("PersonName");
            candidate.setName(contactInfo.has("GivenName") ? contactInfo.getAsJsonPrimitive("GivenName").getAsString() : "");
            candidate.setLastName(contactInfo.has("GivenName") ? contactInfo.getAsJsonPrimitive("FamilyName").getAsString() : "");
        } catch (Exception e) {
            logger.error("Exception while parse name", e);
        }
    }


    private List<JobStability> parseCareersBlock(JsonObject structuredXMLResume) {
        try {
            JsonArray contactMethods = structuredXMLResume.getAsJsonObject("EmploymentHistory").getAsJsonArray("EmployerOrg");

            List<JobStability> jobStabilities = new ArrayList<>();
            //parseResume address block
            for (JsonElement contactMethod : contactMethods) {

                JobStability jobStability = new JobStability();

                try {
                    String orgName = contactMethod.getAsJsonObject().getAsJsonPrimitive("EmployerOrgName").getAsString();
                    jobStability.setCompany(orgName);
                } catch (Exception e) {
                    logger.error("Exception while parse position history's organisation name");
                }

                JsonObject positionHistory = contactMethod.getAsJsonObject().getAsJsonArray("PositionHistory").get(0).getAsJsonObject();

                try {
                    String title = positionHistory.getAsJsonPrimitive("Title").getAsString();
                    jobStability.setJobTitle(title);
                } catch (Exception e) {
                    logger.error("Exception while parse position history's title");
                }

                try {
                    JsonObject startDate = positionHistory.getAsJsonObject("StartDate");
                    JsonObject endDate = positionHistory.getAsJsonObject("EndDate");
                    Date start = resolveDate(startDate);
                    Date end = resolveDate(endDate);
                    jobStability.setStartDate(start);
                    jobStability.setEndDate(end);
                } catch (Exception e) {
                    logger.error("Exception while parse position history's date rage");
                }

                jobStabilities.add(jobStability);
            }
            return jobStabilities;
        } catch (Exception e) {
            logger.error("Exception while parse careers block");
        }
        return Collections.emptyList();
    }

    private Date resolveDate(JsonObject startDate) {
        if (startDate.has("Year")) {
            String startDateString = startDate.getAsJsonPrimitive("Year").getAsString();
            Calendar cal = Calendar.getInstance();
            cal.clear();
            cal.set(Calendar.YEAR, Integer.valueOf(startDateString));
            return cal.getTime();
        } else if (startDate.has("YearMonth")) {
            String startDateString = startDate.getAsJsonPrimitive("YearMonth").getAsString();
            Calendar cal = Calendar.getInstance();
            cal.clear();
            cal.set(Calendar.YEAR, Integer.valueOf(StringUtils.substringBefore(startDateString, "-")));
            cal.set(Calendar.MONTH, Integer.valueOf(StringUtils.substringAfterLast(startDateString, "-")));
            return cal.getTime();
        } else if (startDate.has("AnyDate")) {
            String startDateString = startDate.getAsJsonPrimitive("AnyDate").getAsString();
            Parser parser = new Parser();
            List<DateGroup> dateGroups = parser.parse(startDateString);
            return dateGroups.get(0).getDates().get(0);
        }
        return null;
    }
}
