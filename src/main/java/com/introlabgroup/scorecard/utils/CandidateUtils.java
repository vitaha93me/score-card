package com.introlabgroup.scorecard.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabgroup.scorecard.model.Assessment;
import com.introlabgroup.scorecard.model.Candidate;
import com.introlabgroup.scorecard.model.CareerHistory;
import com.introlabgroup.scorecard.model.dto.CareerHistoryDTO;
import com.introlabgroup.scorecard.model.onet.MeasuringType;
import com.introlabgroup.scorecard.model.onet.Occupation;
import com.introlabgroup.scorecard.model.onet.QualityCategory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.TemporalAdjusters.nextOrSame;

/**
 * @author vitalii.
 */
public class CandidateUtils {
    public static CandidateUtils getInstance() {
        return new CandidateUtils();
    }

    public CareerHistoryDTO careerHistoryToDto(CareerHistory careerHistory) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        CareerHistoryDTO careerHistoryDTO = new CareerHistoryDTO();
        careerHistoryDTO.setId(careerHistory.getId());
        careerHistoryDTO.setCompany(careerHistory.getCompany());
        careerHistoryDTO.setJobTitle(careerHistory.getJobTitle());
        careerHistoryDTO.setStartDate(dateFormat.format(careerHistory.getStartDate()));
        careerHistoryDTO.setEndDate(dateFormat.format(careerHistory.getEndDate()));
        return careerHistoryDTO;
    }

    public String generateCustomId(String string) {
        long h = 98764321261L;
        int l = string.length();
        char[] chars = string.toCharArray();

        for (int i = 0; i < l; i++) {
            h = 31 * h + chars[i];
        }
        return String.valueOf(h);
    }

    private void calculateWeightedScore(Assessment assessment, Occupation occupation) {
        if (!assessment.isFinished()) {
            return;
        }
        JsonElement parse = new JsonParser().parse(assessment.getBody());
        JsonObject answers = parse.getAsJsonObject();
        List<QualityCategory> resultQualities = CandidateUtils.this.getRatedQualities(occupation, answers);
        double w2 = 0;
        double wqSum = 0;
        try {
            for (QualityCategory quality : resultQualities) {
                double qualityScore = 0;
                if (!answers.get(quality.getOnetItem().getElementId()).isJsonNull()) {
                    qualityScore = answers.getAsJsonPrimitive(quality.getOnetItem().getElementId()).getAsDouble();
                }
                w2 += quality.getWeight();
                wqSum += quality.getWeight() * qualityScore;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        double s = 1D / w2 * wqSum;
        assessment.setOverallScore(s);

    }

    private void calculatePrioritizedScore(Assessment assessment, Occupation occupation) {
        if (!assessment.isFinished()) {
            return;
        }
        JsonElement parse = new JsonParser().parse(assessment.getBody());
        JsonObject answers = parse.getAsJsonObject();
        List<QualityCategory> resultQualities = CandidateUtils.this.getRatedQualities(occupation, answers);
        double p2 = 0;
        double pqSum = 0;

        for (QualityCategory quality : resultQualities) {
            double qualityScore = answers.getAsJsonPrimitive(quality.getOnetItem().getElementId()).getAsDouble();
            final double doubleVale = quality.getPriorityRanking() == null ? 1 : quality.getPriorityRanking().getDoubleVale();
            p2 += doubleVale;
            pqSum += doubleVale * qualityScore;
        }

        double s = (1D / p2 * pqSum);
        assessment.setOverallScore(s);
    }

    private List<QualityCategory> getRatedQualities(Occupation occupation, JsonObject answers) {
        List<QualityCategory> resultQualities = new ArrayList<>();
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getAbilities(), answers));
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getInterests(), answers));
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getSkills(), answers));
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getCustomQualities(), answers));
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getKnowledge(), answers));
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getWorkActivities(), answers));
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getWorkStyles(), answers));
        resultQualities.addAll(CandidateUtils.this.getQualitiesForCategory(occupation.getWorkValues(), answers));
        return resultQualities;
    }

    private List<QualityCategory> getQualitiesForCategory(List<? extends QualityCategory> qualities, JsonObject jsonObject) {
        List<QualityCategory> resultQualities = new ArrayList<>();
        for (QualityCategory quality : qualities) {
            if (jsonObject.has(quality.getOnetItem().getElementId())) {
                if (!quality.isDealBreaker()) {
                    resultQualities.add(quality);
                }
            }
        }
        return resultQualities;
    }

    private boolean hasUnratedMandatoryQualities(List<? extends QualityCategory> qualities, JsonObject jsonObject) {

        for (QualityCategory quality : qualities) {
            if (!jsonObject.has(quality.getOnetItem().getElementId()) && quality.isRequired()) {
                return true;
            }
        }
        return false;
    }

    public boolean isFinished(Occupation occupation, JsonObject answers) {
        List<Boolean> resultQualities = new ArrayList<>();
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getAbilities(), answers));
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getInterests(), answers));
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getSkills(), answers));
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getCustomQualities(), answers));
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getKnowledge(), answers));
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getWorkActivities(), answers));
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getWorkStyles(), answers));
        resultQualities.add(CandidateUtils.this.hasUnratedMandatoryQualities(occupation.getWorkValues(), answers));
        return !resultQualities.contains(true);
    }

    public long findNextMondayAtTime(String time) {

        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.parse(time, DateTimeFormatter.ofPattern("HH:mm"));
        LocalDateTime dateTime = LocalDateTime.of(localDate, localTime);
        LocalDateTime resultDateTime = dateTime.with(nextOrSame(DayOfWeek.MONDAY));
        return resultDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() / 1000;
    }

    public void calculateScores(Candidate candidate) {
        ObjectMapper objectMapper = new ObjectMapper();
        for (Assessment assessment : candidate.getAssessments()) {
            try {

                Occupation occupation = objectMapper.readValue(assessment.getOccupationJson(), Occupation.class);
                if (assessment.getMeasuringType() == MeasuringType.WEIGHT) {
                    CandidateUtils.this.calculateWeightedScore(assessment, occupation);
                    System.out.println("Calculate with weights");
                } else if (assessment.getMeasuringType() == MeasuringType.PRIORITY) {
                    System.out.println("Calculate with priorities");
                    CandidateUtils.this.calculatePrioritizedScore(assessment, occupation);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
