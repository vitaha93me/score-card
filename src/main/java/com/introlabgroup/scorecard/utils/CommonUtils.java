package com.introlabgroup.scorecard.utils;

import com.introlabgroup.scorecard.model.JobProfile;
import com.introlabgroup.scorecard.model.dto.JobProfileViewJsonObject;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author vitalii.
 */
public class CommonUtils {

    public static JobProfileViewJsonObject jobProfileToViewDTO(JobProfile jobProfile) {
        JobProfileViewJsonObject jsonObject = new JobProfileViewJsonObject();
        jsonObject.setDepartment(jobProfile.getDepartment());
        jsonObject.getDepartment().setOpenPositions(null);
        jsonObject.setFullyConfigured(jobProfile.isFullyConfigured());
        jsonObject.setTitle(jobProfile.getTitle());
        jsonObject.setStatusActive(jobProfile.isStatus());
        jsonObject.setFake(jobProfile.isFake());
        jsonObject.setMeasuringType(jobProfile.getMeasuringType());
        jsonObject.setDescription(jobProfile.getDescription());
        jsonObject.setOccupationJson(jobProfile.getOccupationJson());
        jsonObject.setId(jobProfile.getId());
        return jsonObject;
    }

    public static String extractPostRequestBody(HttpServletRequest request) {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            Scanner s = null;
            try {
                s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (s != null) {
                return s.hasNext() ? s.next() : "";
            }
        }
        return "";
    }
}
