package com.introlabgroup.scorecard.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author vitalii.
 */
public class FileUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public static void saveAttachment(String directory, MultipartFile file) throws IOException {

        if (file.isEmpty()) {
            return;
        }
        File targetDirectory = new File(directory);
        if (!targetDirectory.exists()){
            logger.info("Directory " + targetDirectory.getName() + " created:\n " + targetDirectory.mkdirs());

        }
        byte[] bytes = file.getBytes();
        Path path = Paths.get(directory + file.getOriginalFilename());
        Files.write(path, bytes);

    }
}
