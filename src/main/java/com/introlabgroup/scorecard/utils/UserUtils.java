package com.introlabgroup.scorecard.utils;

import com.introlabgroup.scorecard.model.Company;
import com.introlabgroup.scorecard.model.Role;
import com.introlabgroup.scorecard.model.User;
import com.introlabgroup.scorecard.model.dto.UserDTO;

/**
 * @author vitalii.
 */
public class UserUtils {

    public static UserDTO userToDTO(User user) {

        UserDTO userDTO = new UserDTO();
        Company company = user.getCompany();
        company.setDepartments(null);
        company.setUsers(null);
        userDTO.setCompany(company);
        userDTO.setEmail(user.getEmail());
        userDTO.setId(user.getId());
        userDTO.setLastName(user.getLastName());
        userDTO.setFirstName(user.getName());
        userDTO.setRoles(user.getRoles());
        return userDTO;
    }

    public static boolean hasAccessToUser(User loggedUser, User objectUser) {
        for (Role role : loggedUser.getRoles()) {
            if (role.getRole().contains("ROLE_SUPER_ADMIN")) {
                return true;
            }
        }
        for (Role role : loggedUser.getRoles()) {
            if (role.getRole().contains("ROLE_USER")) {
                return false;
            }
        }
        return objectUser.getCompany().getId().longValue() == loggedUser.getCompany().getId().longValue();
    }

    public static boolean isManager(User loggedUser) {
        for (Role role : loggedUser.getRoles()) {
            if (role.getRole().contains("ROLE_USER")) {
                return true;
            }
        }
        return false;
    }
}
