-- auto-generated definition
create table if not exists stripe_data
(
  id        int auto_increment
    primary key,
  companyId int                                null,
  stripeId  varchar(255)                       null,
  created   datetime default CURRENT_TIMESTAMP null
);

  -- auto-generated definition
create table if not exists stripe_subscriptions
(
  id             int auto_increment
    primary key,
  companyId      int                                null,
  subscriptionId varchar(255)                       null,
  type           varchar(255)                       null,
  email          varchar(255)                       null,
  price          double                             null,
  scorecardCount int                                null,
  currentStatus  tinyint(1) default '1'             null,
  created        datetime default CURRENT_TIMESTAMP null,
  lastPayment    datetime default CURRENT_TIMESTAMP null,
  scorecardsLeft int                                null
);

-- auto-generated definition
create table if not exists stripe_history
(
  id             int auto_increment
    primary key,
  companyId      int                                null,
  companyName    varchar(255)                       null,
  subscriptionId varchar(255)                       null,
  status         tinyint(1)                         null,
  created        datetime default CURRENT_TIMESTAMP null
);




