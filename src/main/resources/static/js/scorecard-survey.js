var occupation;
// AJAX call for autocomplete
$(document).ready(function () {
    $("#search-box").keyup(function () {
        if ($(this).val().length == 0) {
            $("#suggesstion-box ul").html("");
            return false;
        }
        $.ajax({
            type: "POST",
            url: "../api/occupations/search",
            data: 'keyword=' + $(this).val(),
            headers: {
                "Authorization": localStorage.getItem('Authorization'),
                "Accept": "application/json, text/javascript, */*; q=0.01"
            },
            beforeSend: function () {
                $("#search-box").css("background", "#FFF url(../images/LoaderIcon.gif) no-repeat 165px");
            },
            success: function (data) {
                $("#suggesstion-box").show();
                $("#suggesstion-box ul").html("");
                $.each(data, function (i, item) {
                    $("#suggesstion-box ul").append("<li onclick='selectOccupation(\"" + item.onetsoc_code + "\")'>" + item.title + "</li>");
                    if (i > 10) {
                        return false;
                    }
                });

                $("#search-box").css("background", "#FFF");
            }
        });
    });
    displaySurvey();
});

Survey.defaultBootstrapCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapCss.rating.item = "btn btn-default my-rating";
Survey.Survey.cssType = "bootstrap";

var displaySurvey = function () {

    $.ajax({
        type: "POST",
        url: "../api/survey/multipaged/test/" + profileId,
        contentType: "application/json",
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (json) {
            var survey = new Survey.Model(JSON.stringify(json));
            $('#main-content').html('<div id="surveyPreview"></div>');
            $("#surveyPreview").Survey({
                model: survey,
                showQuestionNumbers: "off",
                onComplete: finishSurvey
            });
        }
    });
};

var occTest = function () {

    jobProfile.occupation = customOccupation;
    var deptId = $('#departments select').val();
    if (deptId == 0) {
        alert("Please, select department!");
        return;
    } else {
        $.ajax({
            type: "GET",
            url: "../api/department/get",
            data: 'id=' + deptId,
            headers: {
                "Authorization": localStorage.getItem('Authorization'),
                "Accept": "application/json, text/javascript, */*; q=0.01"
            },
            success: function (data) {
                jobProfile.department = data;
                $.ajax({
                    type: "POST",
                    url: "../api/survey/multipaged/preview",
                    data: JSON.stringify(jobProfile),
                    contentType: "application/json",
                    headers: {
                        "Authorization": localStorage.getItem('Authorization'),
                        "Accept": "application/json, text/javascript, */*; q=0.01"
                    },
                    success: function (json) {
                        var survey = new Survey.Model(JSON.stringify(json));
                        survey.showProgressBar = 'bottom';

                        $('#main-content').html('<div id="surveyPreview"></div>');
                        $("#surveyPreview").Survey({
                            model: survey,
                            showQuestionNumbers: "off",
                            onComplete: sendDataToServer
                        });
                    }
                });
            }
        });
    }
};

var dto;

function printJobQualities() {
    printQualityRows(dto.occupation.abilities);
    printQualityRows(dto.occupation.knowledge);
    printQualityRows(dto.occupation.interests);
    printQualityRows(dto.occupation.workValues);
    printQualityRows(dto.occupation.workActivities);
    printQualityRows(dto.occupation.workStyles);
    printQualityRows(dto.occupation.skills);
    printQualityRows(dto.occupation.customQualities);
}

var occTestEdit = function () {

    jobProfile.occupation = customOccupation;
    var deptId = $('#departments select').val();
    var jobTitle = $('#jobTitle').val();
    var measuringType = $('#measuringType select').val();
    if (deptId == 0) {
        alert("Please, select department!");
        return;
    } else if (jobTitle === '') {
        alert("Please, select name for job profile!");
        return;
    } else if (measuringType == 0) {
        alert("Please, select measuring algorithm!");
        return;
    } else {
        $.ajax({
            type: "GET",
            url: "../api/department/get",
            data: 'id=' + deptId,
            headers: {
                "Authorization": localStorage.getItem('Authorization'),
                "Accept": "application/json, text/javascript, */*; q=0.01"
            },
            success: function (data) {
                jobProfile.department = data;
                jobProfile.name = jobTitle;
                $.ajax({
                    type: "POST",
                    url: "../api/survey/save",
                    data: JSON.stringify(jobProfile),
                    contentType: "application/json",
                    headers: {
                        "Authorization": localStorage.getItem('Authorization'),
                        "Accept": "application/json, text/javascript, */*; q=0.01"
                    },
                    success: function (json) {
                        $('#main-content').css("display", "none");
                        dto = JSON.parse(JSON.stringify(json));
                        printJobQualities();
                        $('#example-edit').SetEditable({
                            columnsEd: "3",
                            onEdit: function (row) {
                                console.log($(row).find("td:first-of-type").text());
                                console.log($(row).find("td:nth-of-type(3)").text());
                                console.log(dto.profileId);
                                updateWeight(dto.profileId, $(row).find("td:first-of-type").text(), $(row).find("td:nth-of-type(4)").text());
                            }
                        });

                        $('.editPage').css("display", "block");
                        var leadingQuestionsContentTable = "";
                        var qualityOptions = "";
                        for (item in qualities) {
                            qualityOptions += '<option value=' + qualities[item].elementId + '>' + qualities[item].elementName + '</option>';
                            for (questionNumber in qualities[item].leadingQuestions) {
                                leadingQuestionsContentTable += '\
                                            <tr>\
                                                <td>' + qualities[item].elementId + '</td>\
                                                <td>' + qualities[item].elementName + '</td>\
                                                <td>' + qualities[item].leadingQuestions[questionNumber].question + '</td>\
                                                </tr>'
                            }
                        }
                        $('#leading-questions > tbody').html(leadingQuestionsContentTable);
                        $('#qualityForQuestion').html(qualityOptions);
                    }
                });
            }
        });
    }
};


var customOccupation;

function selectOccupation(val) {

    $.ajax({
        type: "GET",
        url: "../api/occupations/get",
        data: 'keyword=' + val,
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            occupation = data;
            customOccupation = JSON.parse(JSON.stringify(data));
            customOccupation.abilities = [];
            customOccupation.knowledge = [];
            customOccupation.interests = [];
            customOccupation.workValues = [];
            customOccupation.workActivities = [];
            customOccupation.workStyles = [];
            customOccupation.skills = [];
            customOccupation.customQualities = [];
            $("#search-box").val(data.title);
            $("#suggesstion-box").hide();

            populateTags($('#abilities'), occupation.abilities);
            populateTags($('#knowledge'), occupation.knowledge);
            populateTags($('#interests'), occupation.interests);
            populateTags($('#workValues'), occupation.workValues);
            populateTags($('#workActivities'), occupation.workActivities);
            populateTags($('#workStyles'), occupation.workStyles);
            populateTags($('#skills'), occupation.skills);
            populateTags($('#custom'), occupation.customQualities);
        }
    });
}


function selectCustomQuality(val) {

    $.ajax({
        type: "GET",
        url: "../api/occupations/addcustom",
        data: 'keyword=' + occupation.onetsoc_code + '&qualityId=' + val,
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            $("#search-box-quality").val(data.title);
            $("#suggesstion-box-quality").hide();
            populateTags($('#custom'), data);
        }
    });
}

function addOwnQuality() {

    $.ajax({
        type: "GET",
        url: "../api/occupations/qualities/addown",
        data: 'keyword=' + occupation.onetsoc_code + '&name=' + $('#qualityName').val() + '&description=' + $('#qualityDescription').val(),
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            $("#search-box-quality").val(data.title);
            $("#suggesstion-box-quality").hide();
            populateTags($('#custom'), data);
        }
    });
}

function addNewQuestion() {

    $.ajax({
        type: "POST",
        url: "../api/questions/addown",
        data: 'profileId=' + dto.profileId + '&qualityId=' + $('#qualityForQuestion').val() + '&question=' + $('#newQuestion').val(),
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        beforeSend: function () {
            console.log($("#leading-questions > tbody:last-child"));
            $("#leading-questions > tbody:last-child").append('<tr>\
        <td>' + $('#qualityForQuestion').val() + '</td>\
        <td>' + $('#qualityForQuestion option:selected').text() + '</td>\
        <td>' + $('#newQuestion').val() + '</td>\
        </tr>');
        }
    });
}


// AJAX call for autocomplete
$(document).ready(function () {
    $("#search-box-quality").keyup(function () {
        if ($(this).val().length == 0) {
            $("#suggesstion-box-quality ul").html("");
            return false;
        }
        $.ajax({
            type: "POST",
            url: "../api/occupations/custom-qualities/search",
            data: 'keyword=' + $(this).val(),
            headers: {
                "Authorization": localStorage.getItem('Authorization'),
                "Accept": "application/json, text/javascript, */*; q=0.01"
            },
            beforeSend: function () {
                $("#search-box-quality").css("background", "#FFF url(../images/LoaderIcon.gif) no-repeat 165px");
            },
            success: function (data) {
                $("#suggesstion-box-quality").show();
                $("#suggesstion-box-quality ul").html("");
                $.each(data, function (i, item) {
                    $("#suggesstion-box-quality ul").append("<li onclick='selectCustomQuality(\"" + item.elementId + "\")'>" + item.elementName + "</li>");
                    // alert(item.PageName);
                    if (i > 10) {
                        return false;
                    }
                });

                $("#search-box-quality").css("background", "#FFF");
            }
        });
    });
});


function sendDataToServer(survey) {
    alert("The results are:" + JSON.stringify(survey.data));
}

function finishSurvey(survey) {

    $.ajax({
        type: "POST",
        url: "../api/survey/finish/" + profileId + "/" + candidateId,
        data: JSON.stringify(survey.data),
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            console.log(data);
        }
    });

    alert("The results are:" + JSON.stringify(survey.data));
}


var updateWeight = function (profileId, qualityId, weight) {
    $.ajax({
        type: "POST",
        url: "../api/survey/update/weight",
        data: 'profileId=' + profileId + '&qualityId=' + qualityId + '&weight=' + weight,
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            console.log(data);
        }
    });
};

var setScoreAlgorithm = function (element) {
    var select = $(element);
    jobProfile.measuringType = select.val()
};

var finishSurveyConfig = function () {
    if (dto.measuringType === 'PRIORITY') {

        var value = $('.priority-selector').filter(function () {
            return this.value === 'NO_PRIORITY';
        });
        if (value.length == 0) {
        } else if (value.length > 0) {
            alert('Please set priority for all qualities');
            return;
        }
    }

    $.ajax({
        type: "POST",
        url: "../api/survey/config/finish",
        data: 'profileId=' + dto.profileId,
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function () {
            alert("Survey created. Thanks for using!");
            location.reload();
        }
    });
};


$(document).ready(function () {

    $.ajax({
        type: "GET",
        url: "../api/department/all",
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            var selectOptions = "<option value='0'>Select Department</option>";

            $.each(data, function (i, item) {
                selectOptions += '<option value="' + item.id + '">' + item.name + '</option>'
            });

            $("#departments select").html(selectOptions);

        }
    });

});

var populateTags = function (input, dataArray) {

    var cities = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: 'assets/cities.json'
    });
    cities.initialize();

    var elt = input;
    elt.tagsinput({
        itemValue: 'value',
        itemText: 'text',
        typeaheadjs: {
            name: 'cities',
            displayKey: 'text',
            source: cities.ttAdapter()
        }
    });

    $.each(dataArray, function (i, item) {
        elt.tagsinput('add', {
            "value": item.onetItem.elementId,
            "text": item.onetItem.elementName + "(" + item.weight + ")",
            "title": item.onetItem.description
        });
    });
};


var findElementForRemove = function (array, n) {

    for (item in array) {

        if (array[item].onetItem.elementId == n.value) {
            return item;
        }
    }
};

var qualities = new Array();
var printQualityRows = function (array) {
    var tableContent = "";
    for (item in array) {
        qualities.push(array[item].onetItem);
        tableContent += '\
                <tr>\
                    <td>' + array[item].onetItem.elementId + '</td>\
                    <td>' + array[item].onetItem.elementName + '</td>\
                    <td><select class="priority-selector" onchange="setPriority(this, \'' + array[item].onetItem.elementId + '\')">\
                    <option value="NO_PRIORITY">Set priority</option>\
                    <option value="MUST_HAVE">Must Have Quality</option>\
                    <option value="NICE_TO_HAVE">Nice to have Quality</option>\
                    <option value="LOW_PRIORITY">Low priority Quality</option>\
                    <option value="DEAL_BREAKER">Deal breaker</option>\
                    </select></td>\
                    <td>' + array[item].weight + '</td>\
                    <td><input onchange="setRequired(\'' + array[item].onetItem.elementId + '\')" type="checkbox" checked></td>\
                    </tr>'
    }
    $('#example-edit > tbody:last-child').append(tableContent);
};


var setPriority = function (element, id) {
    var select = $(element);
    $.ajax({
        type: "POST",
        url: "../api/survey/update/ranking",
        data: 'profileId=' + dto.profileId + '&qualityId=' + id + '&priorityRanking=' + select.val(),
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            console.log(data);
        }
    });
};


var setRequired = function (id) {
    $.ajax({
        type: "POST",
        url: "../api/survey/update/requirements",
        data: 'profileId=' + dto.profileId + '&qualityId=' + id,
        headers: {
            "Authorization": localStorage.getItem('Authorization'),
            "Accept": "application/json, text/javascript, */*; q=0.01"
        },
        success: function (data) {
            console.log(data);
        }
    });
};

var removeElementFromCustomItem = function (array, n) {
    for (item in array) {
        if (array[item].onetItem.elementId == n) {
            array.splice(item, 1);
        }
    }
};

var copyToCustomOccupation = function (target, array, n) {
    for (item in array) {
        if (array[item].onetItem.elementId == n) {
            target.push(array[item]);
        }
    }
};

var remove_array_element = function (array, n) {
    var index = findElementForRemove(array, n);
    if (index) {
        array.splice(index, 1);
    }
};

var selectTag = function (obj) {
    console.log($(obj).attr('title'));
    console.log($(obj).attr('id'));

    if ($(obj).hasClass('selected-tag')) {
        $(obj).removeClass('selected-tag');
        removeElementFromCustomItem(customOccupation.abilities, $(obj).attr('id'));
        removeElementFromCustomItem(customOccupation.skills, $(obj).attr('id'));
        removeElementFromCustomItem(customOccupation.interests, $(obj).attr('id'));
        removeElementFromCustomItem(customOccupation.knowledge, $(obj).attr('id'));
        removeElementFromCustomItem(customOccupation.workValues, $(obj).attr('id'));
        removeElementFromCustomItem(customOccupation.workActivities, $(obj).attr('id'));
        removeElementFromCustomItem(customOccupation.workStyles, $(obj).attr('id'));
        removeElementFromCustomItem(customOccupation.customQualities, $(obj).attr('id'));
    } else {
        $(obj).addClass('selected-tag');
        copyToCustomOccupation(customOccupation.abilities, occupation.abilities, $(obj).attr('id'));
        copyToCustomOccupation(customOccupation.skills, occupation.skills, $(obj).attr('id'));
        copyToCustomOccupation(customOccupation.interests, occupation.interests, $(obj).attr('id'));
        copyToCustomOccupation(customOccupation.knowledge, occupation.knowledge, $(obj).attr('id'));
        copyToCustomOccupation(customOccupation.workValues, occupation.workValues, $(obj).attr('id'));
        copyToCustomOccupation(customOccupation.workActivities, occupation.workActivities, $(obj).attr('id'));
        copyToCustomOccupation(customOccupation.workStyles, occupation.workStyles, $(obj).attr('id'));
        copyToCustomOccupation(customOccupation.customQualities, occupation.customQualities, $(obj).attr('id'));
    }
};