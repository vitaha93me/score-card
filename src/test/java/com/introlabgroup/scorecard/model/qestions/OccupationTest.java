package com.introlabgroup.scorecard.model.qestions;

import com.introlabgroup.scorecard.ScoreCardApplicationTests;
import com.introlabgroup.scorecard.model.onet.Occupation;
import com.introlabgroup.scorecard.repository.onet.OccupationRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author vitalii.
 */
public class OccupationTest extends ScoreCardApplicationTests {

    @Autowired
    private OccupationRepository occupationRepository;

    @Test
    @Transactional
    public void getOccupationWithAbilities() throws Exception {
        Occupation one = occupationRepository.findOne("17-2141.00");
        System.out.println(one);
    }
}
