package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.ScoreCardApplicationTests;
import com.introlabgroup.scorecard.model.payment.StripeSubscription;
import com.introlabgroup.scorecard.repository.onet.OnetItemRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * https://stripe.com/docs/testing
 */
public class StripeServiceTest extends ScoreCardApplicationTests {


    @Test
    public void test() {
//        new StripeService().testApiCall();
//        new StripeService().createServiceAsProduct();
//        new StripeService().createTarifPlan();
//        new StripeService().createCustomer();
//        new StripeService().subscribeCustomer();
//        SubscriptionDTO subscriptionDTO = new SubscriptionDTO();
//        subscriptionDTO.setScorecardCount(5);
//        subscriptionDTO.setPaymentPeriod(PaymentPeriod.MONTHLY);
//        new StripeService().calculatePrice(subscriptionDTO);
//        System.out.println(subscriptionDTO);
    }


    @Autowired
    private StripeService stripeService;

    @Autowired
    private CompanyService companyService;

    @Test
    public void isPeriodFinished() {

        StripeSubscription stripeSubscriptionByCompanyId = stripeService.getStripeDao().getStripeSubscriptionByCompanyId(1L);
        boolean periodFinished = stripeService.isPeriodFinished(stripeSubscriptionByCompanyId);
        System.out.println(periodFinished);
    }

    @Test
    public void isTrial() {
        boolean trial = companyService.isTrial((long) 15);
        System.out.println(trial);
    }


    @Autowired
    private OnetItemRepository repository;

    @Test
    public void testOnetRepo() {

//        OnetItem containingIgnoreCase = repository.findByElementNameContainingIgnoreCase("Problem SeNSitivit");
//        System.out.println(containingIgnoreCase);
    }
}


//created product
//{
//        "id": "prod_DVPlYRNXPddtfH",
//        "object": "product",
//        "active": true,
//        "attributes": [],
//        "caption": null,
//        "created": 1535530891,
//        "deactivate_on": [],
//        "description": null,
//        "images": [],
//        "livemode": false,
//        "metadata": {},
//        "name": "My SaaS Platform",
//        "package_dimensions": null,
//        "shippable": null,
//        "skus": null,
//        "statement_descriptor": null,
//        "type": "service",
//        "updated": 1535530891,
//        "unit_label": null,
//        "url": null,
//        "deleted": null
//        }

//created plan
//{
//        "id": "plan_DVPqwiLdMWoKuY",
//        "object": "plan",
//        "active": true,
//        "amount": 10000,
//        "billing_scheme": "per_unit",
//        "created": 1535531238,
//        "currency": "usd",
//        "interval": "day",
//        "interval_count": 1,
//        "livemode": false,
//        "metadata": {},
//        "nickname": "my test plan for 100 USD",
//        "product": "prod_DVPlYRNXPddtfH",
//        "tiers": null,
//        "tiers_mode": null,
//        "transform_usage": null,
//        "usage_type": "licensed",
//        "deleted": null,
//        "name": null,
//        "statement_description": null,
//        "statement_descriptor": null,
//        "trial_period_days": null
//        }

//create customer
//{
//        "id": "cus_DVQNWShrBnkv4D",
//        "object": "customer",
//        "account_balance": 0,
//        "business_vat_id": null,
//        "created": 1535533180,
//        "currency": null,
//        "default_source": "card_1D4PWaDy3SfPxoTpWor0skpK",
//        "deleted": null,
//        "delinquent": false,
//        "description": null,
//        "discount": null,
//        "email": "vitaha.me@gmail.com",
//        "livemode": false,
//        "metadata": {},
//        "shipping": null,
//        "sources": {
//        "object": "list",
//        "data": [
//        {
//        "address_city": null,
//        "address_country": null,
//        "address_line1": null,
//        "address_line1_check": null,
//        "address_line2": null,
//        "address_state": null,
//        "address_zip": null,
//        "address_zip_check": null,
//        "available_payout_methods": null,
//        "brand": "MasterCard",
//        "country": "US",
//        "currency": null,
//        "cvc_check": null,
//        "default_for_currency": null,
//        "dynamic_last4": null,
//        "exp_month": 8,
//        "exp_year": 2019,
//        "fingerprint": "DySO803QbDJNSj07",
//        "funding": "credit",
//        "last4": "4444",
//        "name": null,
//        "recipient": null,
//        "status": null,
//        "three_d_secure": null,
//        "tokenization_method": null,
//        "deleted": null,
//        "description": null,
//        "iin": null,
//        "issuer": null,
//        "type": null,
//        "id": "card_1D4PWaDy3SfPxoTpWor0skpK",
//        "object": "card",
//        "account": null,
//        "customer": "cus_DVQNWShrBnkv4D",
//        "metadata": {}
//        }
//        ],
//        "has_more": false,
//        "total_count": 1,
//        "url": "/v1/customers/cus_DVQNWShrBnkv4D/sources",
//        "count": null,
//        "request_options": null,
//        "request_params": null
//        },
//        "subscriptions": {
//        "object": "list",
//        "data": [],
//        "has_more": false,
//        "total_count": 0,
//        "url": "/v1/customers/cus_DVQNWShrBnkv4D/subscriptions",
//        "count": null,
//        "request_options": null,
//        "request_params": null
//        },
//        "cards": null,
//        "default_card": null,
//        "next_recurring_charge": null,
//        "subscription": null,
//        "trial_end": null
//        }

//subscription
//<com.stripe.model.SubscriptionDTO@521081105 id=sub_DVQSuFTXLKhmJ9> JSON: {
//        "id": "sub_DVQSuFTXLKhmJ9",
//        "object": "subscription",
//        "application_fee_percent": null,
//        "billing": "charge_automatically",
//        "billing_cycle_anchor": 1535533464,
//        "cancel_at_period_end": false,
//        "canceled_at": null,
//        "created": 1535533464,
//        "current_period_end": 1535619864,
//        "current_period_start": 1535533464,
//        "customer": "cus_DVQNWShrBnkv4D",
//        "days_until_due": null,
//        "discount": null,
//        "ended_at": null,
//        "items": {
//        "object": "list",
//        "data": [
//        {
//        "id": "si_DVQSRRtmxGqYez",
//        "object": "subscription_item",
//        "created": 1535533464,
//        "plan": {
//        "id": "plan_DVPqwiLdMWoKuY",
//        "object": "plan",
//        "active": true,
//        "amount": 10000,
//        "billing_scheme": "per_unit",
//        "created": 1535531238,
//        "currency": "usd",
//        "interval": "day",
//        "interval_count": 1,
//        "livemode": false,
//        "metadata": {},
//        "nickname": "my test plan for 100 USD",
//        "product": "prod_DVPlYRNXPddtfH",
//        "tiers": null,
//        "tiers_mode": null,
//        "transform_usage": null,
//        "usage_type": "licensed",
//        "deleted": null,
//        "name": null,
//        "statement_description": null,
//        "statement_descriptor": null,
//        "trial_period_days": null
//        },
//        "quantity": 1,
//        "deleted": null
//        }
//        ],
//        "has_more": false,
//        "total_count": 1,
//        "url": "/v1/subscription_items?subscription\u003dsub_DVQSuFTXLKhmJ9",
//        "count": null,
//        "request_options": null,
//        "request_params": null
//        },
//        "metadata": {},
//        "plan": {
//        "id": "plan_DVPqwiLdMWoKuY",
//        "object": "plan",
//        "active": true,
//        "amount": 10000,
//        "billing_scheme": "per_unit",
//        "created": 1535531238,
//        "currency": "usd",
//        "interval": "day",
//        "interval_count": 1,
//        "livemode": false,
//        "metadata": {},
//        "nickname": "my test plan for 100 USD",
//        "product": "prod_DVPlYRNXPddtfH",
//        "tiers": null,
//        "tiers_mode": null,
//        "transform_usage": null,
//        "usage_type": "licensed",
//        "deleted": null,
//        "name": null,
//        "statement_description": null,
//        "statement_descriptor": null,
//        "trial_period_days": null
//        },
//        "quantity": 1,
//        "start": 1535533464,
//        "status": "active",
//        "tax_percent": null,
//        "trial_end": null,
//        "trial_start": null
//        }
