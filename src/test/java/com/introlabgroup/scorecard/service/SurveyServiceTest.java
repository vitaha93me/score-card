package com.introlabgroup.scorecard.service;

import com.introlabgroup.scorecard.ScoreCardApplicationTests;
import com.introlabgroup.scorecard.model.onet.Occupation;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class SurveyServiceTest extends ScoreCardApplicationTests {

    @Autowired
    private SurveyService surveyService;

    @Test
    public void findSimilar() {

        Set<Occupation> similar = surveyService.findSimilar("Finance & Insurance Manager");
        System.out.println(similar);
    }
}